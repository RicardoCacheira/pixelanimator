(function (editor) {
    function func1(args) {
        //code
    }

    function func2(args) {
        //code
    }

    editor.addTool({
        name: "Function List",
        type: ToolTypes.FUNCTION_LIST,
        list: [{
            func: func1,
            args: [],
            id: 'func1'
        }, {
            func: func2,
            args: [],
            id: 'func2'
        }]

    })
})(editor);