(function (editor) {
    function run(pixel, raw) {

    }

    editor.addTool({
        name: "Click Tool",
        icon: "icon url",
        type: ToolTypes.CLICK,
        run: run,
        dialog: false,
        deactivate: false,
        raw: true
    })
})(editor);