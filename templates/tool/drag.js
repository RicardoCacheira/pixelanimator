(function (editor) {

    function start(pixel, raw) {

    }

    function move(pixel, raw) {

    }

    function end(pixel, raw) {

    }

    editor.addTool({
        name: "Drag Tool",
        icon: "icon url",
        type: ToolTypes.DRAG,
        start: start,
        move: move,
        end: end,
        dialog: false,
        deactivate: false,
        preview: false,
        emulateMissingEvents: false,
        raw: false,
        showStartPos: true,
        showPosDifference:false
    })
})(editor);