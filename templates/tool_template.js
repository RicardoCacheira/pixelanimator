/*
drag has start,move and end

click button and function has run 

*/

/*
    FUNCTION
    BUTTON
    CLICK
    DRAG
}*/


//FUNCTION
(function (editor) {
    function run(args) {
        //code
    }

    editor.addTool({
        name: "Function Tool",
        args: [],
        type: ToolTypes.FUNCTION,
        run: run
    })
})(editor);

//BUTTON
(function (editor) {
    function run() {

    }

    editor.addTool({
        name: "Button Tool",
        icon: "icon url",
        type: ToolTypes.BUTTON,
        run: run
    })
})(editor);

//CLICK
(function (editor) {
    function run(pixel, raw) {

    }

    editor.addTool({
        name: "Click Tool",
        icon: "icon url",
        type: ToolTypes.CLICK,
        run: run,
        dialog: false,
        raw: true
    })
})(editor);

//DRAG
(function (editor) {

    function run(args) {

    }

    function start(pixel, raw) {

    }

    function move(pixel, raw) {

    }

    function end(pixel, raw) {

    }

    editor.addTool({
        name: "Drag Tool",
        icon: "icon url",
        type: ToolTypes.DRAG,
        args: [], //Optional, works as a function
        run: run, //Optional, works as a function
        start: start,
        move: move,
        end: end,
        dialog: false,
        preview: false,
        raw: true
    })
})(editor);