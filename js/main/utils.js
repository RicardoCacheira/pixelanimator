function normalizeRGBValue(value) {
    value = Number.parseInt(value);
    if (value < 0) {
        value = 0;
    } else if (value > 255) {
        value = 255;
    }

    return value | 0;
}

function normalizeAlpha(alpha) {
    alpha = Number.parseFloat(alpha);
    if (alpha) {
        if (alpha <= 0) {
            alpha = 0;
        } else if (alpha >= 1) {
            alpha = 1;
        } else {
            alpha = Math.round(alpha * 100) / 100
        }
    }
    return alpha;
}

function resizeArray(array, length) {
    array.splice(length, array.length - length);
}

//NOTE: this does not support css transformations
function addTouchEventOffset(evt) {
    if (evt instanceof TouchEvent) {
        evt.offsetX = evt.touches[0].pageX - evt.touches[0].target.offsetLeft;
        evt.offsetY = evt.touches[0].pageY - evt.touches[0].target.offsetTop;
    }
}

function calculateLinePoints(startX, startY, endX, endY) {
    var result = [];
    var reversed = false;
    var aux
    if (startX == endX) {
        if (startY > endY) {
            aux = startY;
            startY = endY;
            endY = aux;
            reversed = true;
        }
        for (var i = startY; i <= endY; i++) {
            result.push({
                x: startX,
                y: i
            });
        }
    } else if (startY == endY) {
        if (startX > endX) {
            aux = startX;
            startX = endX;
            endX = aux;
            reversed = true;
        }
        for (var i = startX; i <= endX; i++) {
            result.push({
                x: i,
                y: startY
            });
        }
    } else {
        var switched = false;
        aux = Math.abs(endX - startX);
        //Switches x with y if the y has more values
        if (Math.abs(endY - startY) > aux) {
            aux = startX;
            startX = startY;
            startY = aux;
            aux = endX;
            endX = endY;
            endY = aux;
            switched = true;
        }

        var m = (endY - startY) / (endX - startX);
        var b = startY - m * startX;

        if (startX > endX) {
            aux = startX;
            startX = endX;
            endX = aux;
            reversed = true;
        }

        for (var i = startX; i <= endX; i++) {
            if (switched) {
                result.push({
                    x: Math.round(m * i + b),
                    y: i
                })
            } else {
                result.push({
                    x: i,
                    y: Math.round(m * i + b),
                })
            }
        }
    }

    if (reversed) {
        result.reverse();
    }
    return result;
}

function toValidPos(value, maxValue = Number.POSITIVE_INFINITY) {
    value = Number.parseInt(value);
    if (!Number.isNaN(value) && value >= 0 && value < maxValue) {
        return value;
    }
    return -1;
}

function fullscreen() {
    var elem = document.documentElement;
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
    } else if (elem.mozRequestFullScreen) {
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
        elem.webkitRequestFullscreen();
    }
}

function exportImage(filename, uri) {
    var elem = a(uri, $$.attr, 'download', filename);
    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        elem.dispatchEvent(event);
    } else {
        elem.click();
    }
}

function getSelectionLimits(selection) {
    var startX = Number.POSITIVE_INFINITY;
    var startY = Number.POSITIVE_INFINITY;
    var endX = Number.NEGATIVE_INFINITY;
    var endY = Number.NEGATIVE_INFINITY;
    for (var x in selection) {
        x = Number.parseInt(x);
        if (x < startX) {
            startX = x
        }
        if (x > endX) {
            endX = x
        }
        for (var y of selection[x]) {
            if (y < startY) {
                startY = y
            }
            if (y > endY) {
                endY = y
            }
        }
    }
    return {
        start: {
            x: startX,
            y: startY
        },
        end: {
            x: endX,
            y: endY
        }
    };
}

function rotateOdd(x, y, cx, cy, ang) {
    x -= cx;
    y -= cy;
    switch (ang) {
        case 90:
            return {
                x: -y + cx,
                y: +x + cy
            }
        case 180:
            return {
                x: -x + cx,
                y: -y + cy
            }
        case 270:
            return {
                x: y + cx,
                y: -x + cy
            }
    }

}

function rotateEven(x, y, cx, cy, ang) {
    x -= cx;
    y -= cy;
    switch (ang) {
        case 90:
            return {
                x: -y + cx - 1,
                y: +x + cy
            }
        case 180:
            return {
                x: -x + cx - 1,
                y: -y + cy - 1
            }
        case 270:
            return {
                x: y + cx,
                y: -x + cy - 1
            }
    }

}

function centerOdd(startX, startY, endX, endY) {
    return {
        x: (endX + startX) / 2,
        y: (endY + startY) / 2
    }
}

function centerEven(startX, startY, endX, endY) {
    return {
        x: (endX + startX + 1) / 2,
        y: (endY + startY + 1) / 2
    }
}

function processAngle(ang) {
    ang = (ang / 90 | 0) % 4 * 90;
    if (ang < 0) {
        ang += 360
    }
    return ang;
}