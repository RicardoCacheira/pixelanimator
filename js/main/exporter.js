class Exporter {
    constructor(editor) {
        this.canvas = canvas();
        this.ctx = this.canvas.getContext("2d");
        this.editor = editor;
    }

    export (pixelSize = 10, frame) {
        var d = this.editor.getDimensions();
        this.canvas.attr('width', pixelSize * d.width, 'height', pixelSize * d.height);
        for (var x = 0; x < d.width; x++) {
            for (var y = 0; y < d.height; y++) {
                this.ctx.fillStyle = this.editor.getFinalPixel(x, y, frame);
                this.ctx.fillRect(x * pixelSize, y * pixelSize, pixelSize, pixelSize);
            }
        }
        this.ctx.stroke();
        return this.canvas.toDataURL();
    }

    export2(pixelSize = 10, frame) {
        var d = this.editor.getDimensions();
        this.canvas.attr('width', pixelSize * d.width, 'height', pixelSize * d.height);
        var pixelSizeDiv2 = pixelSize / 2;
        var primaryBackground = Colors.WHITE;
        var secondaryBackground = new Color(192, 192, 192);
        for (var x = 0; x < d.width; x++) {
            for (var y = 0; y < d.height; y++) {
                var color = this.editor.getFinalPixel(x, y, frame);
                var px = x * pixelSize;
                var py = y * pixelSize;
                this.ctx.fillStyle = color.over(primaryBackground);
                this.ctx.fillRect(px, py, pixelSizeDiv2, pixelSizeDiv2);
                this.ctx.fillRect(px + pixelSizeDiv2, py + pixelSizeDiv2, pixelSizeDiv2, pixelSizeDiv2);
                this.ctx.fillStyle = color.over(secondaryBackground);
                this.ctx.fillRect(px, py + pixelSizeDiv2, pixelSizeDiv2, pixelSizeDiv2);
                this.ctx.fillRect(px + pixelSizeDiv2, py, pixelSizeDiv2, pixelSizeDiv2);
            }
        }
        this.ctx.stroke();
        return this.canvas.toDataURL();
    }

    exportLayer(pixelSize = 10, frame, layer) {
        var d = this.editor.getDimensions();
        this.canvas.attr('width', pixelSize * d.width, 'height', pixelSize * d.height);
        for (var x = 0; x < d.width; x++) {
            for (var y = 0; y < d.height; y++) {
                this.ctx.fillStyle = this.editor.getLayerPixel(x, y, frame, layer);
                this.ctx.fillRect(x * pixelSize, y * pixelSize, pixelSize, pixelSize);
            }
        }
        this.ctx.stroke();
        return this.canvas.toDataURL();
    }

    exportLayer2(pixelSize = 10, frame, layer) {
        var d = this.editor.getDimensions();
        this.canvas.attr('width', pixelSize * d.width, 'height', pixelSize * d.height);
        var pixelSizeDiv2 = pixelSize / 2;
        var primaryBackground = Colors.WHITE;
        var secondaryBackground = new Color(192, 192, 192);
        for (var x = 0; x < d.width; x++) {
            for (var y = 0; y < d.height; y++) {
                var color = this.editor.getLayerPixel(x, y, frame, layer);
                var px = x * pixelSize;
                var py = y * pixelSize;
                this.ctx.fillStyle = color.over(primaryBackground);
                this.ctx.fillRect(px, py, pixelSizeDiv2, pixelSizeDiv2);
                this.ctx.fillRect(px + pixelSizeDiv2, py + pixelSizeDiv2, pixelSizeDiv2, pixelSizeDiv2);
                this.ctx.fillStyle = color.over(secondaryBackground);
                this.ctx.fillRect(px, py + pixelSizeDiv2, pixelSizeDiv2, pixelSizeDiv2);
                this.ctx.fillRect(px + pixelSizeDiv2, py, pixelSizeDiv2, pixelSizeDiv2);
            }
        }
        this.ctx.stroke();
        return this.canvas.toDataURL();
    }
}