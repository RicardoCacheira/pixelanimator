class ToolManager {
    constructor(editor) {
        this.editor = editor;
        this.actionToolList = [];
        this.functionToolList = [];
    }

    static onButtonToolClicked() {
        var run = this.tool.run();
        var editor = this.editor
        if (run) {
            run.then(function () {
                editor.render();
            })
        } else {
            editor.render();
        }
    }

    static onClickToolClicked() {
        var tool = this.tool;
        var editor = this.editor
        if (editor.activeTool === this.tool) {
            editor.deactivateActiveTool();
        } else {
            editor.deactivateActiveTool();
            editor.activeToolType = ToolTypes.CLICK;
            editor.activeTool = tool;
            editor.rawPos = tool.raw;
            editor.deactivateToolOnEnd = tool.deactivate;
            editor.setEmulateMissingEvents(tool.emulateMissingEvents);
            this.addClass('active-tool');
        }
    }

    static activateDefaultTool() {
        var editor = this.editor
        editor.activeToolType = ToolTypes.DRAG;
        editor.activeTool = this;
        editor.rawPos = this.raw;
        editor.deactivateToolOnEnd = this.deactivate;
        editor.setEmulateMissingEvents(this.emulateMissingEvents);
        editor.previewMode = this.preview;
    }

    static onDragToolClicked() {
        var tool = this.tool;
        var editor = this.editor
        if (editor.activeTool === tool) {
            editor.deactivateActiveTool();
        } else {
            editor.deactivateActiveTool();
            editor.activeToolType = ToolTypes.DRAG;
            editor.activeTool = tool;
            editor.rawPos = tool.raw;
            editor.deactivateToolOnEnd = tool.deactivate;
            editor.setEmulateMissingEvents(tool.emulateMissingEvents);
            editor.previewMode = tool.preview;
            this.addClass('active-tool');
        }
    }

    static onToolListClicked() {
        this.func();
    }

    static onToolListMenu(evt) {
        evt.preventDefault()
        if (this.openMenu) {
            this.child(1).css("display", "none");
            this.openMenu = false;
        } else {
            this.child(1).css("display", "block");
            this.openMenu = true;
        }
    }

    static setToolLiOnCreate(pos) {
        this.tool = this.list[pos];
        this.func = this.tool.func;
        this.child(0).attr("src", this.tool.icon);
    }

    static setToolLi(pos) {
        this.tool = this.list[pos];
        this.func = this.tool.func;
        this.child(0).attr("src", this.tool.icon);
        this.func();
    }

    static setToolImg(evt) {
        evt.stopPropagation()
        this.parent(0).css("display", "none");
        this.parent(1).openMenu = false;
        this.parent(1).setTool(this.pos);
    }

    addTool(tool) {
        if (tool.type == ToolTypes.TOOL_LIST) {
            var list = []
            var func
            for (var t of tool.list) {
                switch (t.type) {
                    case ToolTypes.BUTTON:
                        func = ToolManager.onButtonToolClicked;
                        break;
                    case ToolTypes.CLICK:
                        func = ToolManager.onClickToolClicked;
                        break;
                    case ToolTypes.DRAG:
                        if (t.showStartPos === undefined) {
                            t.showStartPos = true;
                        }
                        func = ToolManager.onDragToolClicked;
                        break;
                    default:
                        return;
                }
                t.func = func;
                list.push(t);
            }
            var liElem = li(img().attr('width', 32, 'height', 32), div().addClass("tool-list")).addClass("tool-list-tool");
            for (var i = 0; i < list.length; i++) {
                var image = img(list[i].icon).attr('width', 32, 'height', 32).event("click", ToolManager.setToolImg);
                image.pos = i;
                liElem.child(1).append(image);
            }
            liElem.editor = this.editor;
            liElem.list = list;
            liElem.setTool = ToolManager.setToolLiOnCreate;
            tool.liElem = liElem;
            liElem.setTool(0);
            liElem.setTool = ToolManager.setToolLi;
            this.actionToolList.push({
                tool: tool,
                //isList: true,
                isActive: true
            });
            liElem.event('click', ToolManager.onToolListClicked);
            liElem.event('contextmenu', ToolManager.onToolListMenu);
        } else if (tool.type != ToolTypes.FUNCTION && tool.type != ToolTypes.FUNCTION_LIST) {
            var func
            switch (tool.type) {
                case ToolTypes.BUTTON:
                    func = ToolManager.onButtonToolClicked;
                    break;
                case ToolTypes.CLICK:
                    func = ToolManager.onClickToolClicked;
                    break;
                case ToolTypes.DRAG:
                    if (tool.showStartPos === undefined) {
                        tool.showStartPos = true;
                    }
                    tool.activate = ToolManager.activateDefaultTool;
                    tool.editor = this.editor;
                    func = ToolManager.onDragToolClicked;
                    break;
                default:
                    return;
            }
            if (tool.defaultTool) {
                this.editor.setDefaultTool(tool);
            } else {
                var liElem = li(img(tool.icon).attr('width', 32, 'height', 32));
                liElem.tool = tool;
                liElem.editor = this.editor;
                liElem.event('click', func);
                tool.liElem = liElem;
                this.actionToolList.push({
                    tool: tool,
                    isActive: true
                });
            }
        } else {
            if (tool.type == ToolTypes.FUNCTION) {
                tool.list = [{
                    func: tool.func,
                    id: tool.id,
                    args: tool.args
                }]
            }
            this.functionToolList.push({
                tool: tool,
                isActive: true
            });
        }
        console.log("Loaded tool: " + tool.name);
    }

    setActionToolStatus(pos, status = true) {
        var tool = this.actionToolList[pos];
        if (tool) {
            tool.isActive = status;
        }
    }

    setFunctionToolStatus(pos, status = true) {
        var fucn = this.functionToolList[pos];
        if (fucn) {
            fucn.isActive = status;
        }
    }

    getActionToolsList() {
        var list = [];
        for (var t of this.actionToolList) {
            list.push({
                name: t.tool.name,
                isActive: t.isActive
            });
        }
        return list;
    }

    getFunctionToolsList() {
        var list = [];
        for (var t of this.functionToolList) {
            list.push({
                name: t.tool.name,
                isActive: t.isActive
            });
        }
        return list;
    }

    getActiveActionToolsList() {
        var list = [];
        for (var t of this.actionToolList) {
            if (t.isActive) {
                list.push(t.tool);
            }
        }
        return list;
    }

    getActionTool(name) {
        for (var tool of this.actionToolList) {
            if (tool.tool.name == name) {
                return tool.tool;
            }
        }
    }

    getActiveFunctionToolsObject() {
        console.log("Generating Funcion Tools Object:")
        var functions = {};
        for (var t of this.functionToolList) {
            if (t.isActive) {
                for (var f of t.tool.list) {
                    if (!functions[f.id]) {
                        console.log("Function '" + f.id + "' added.");
                        functions[f.id] = f.func;
                    } else {
                        console.error("Function '" + f.id + "' already exist.");
                    }
                }
            }
        }
        return functions;
    }
}