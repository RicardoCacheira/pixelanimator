const borderAmount = 2;

//Reimplement the select border
// decrease the calculation of repetly used values
//review the canvas variables

class Renderer {
    constructor(canvas, editor) {
        this.canvas = canvas;
        this.canvas.width = canvas.parentElement.clientWidth;
        this.canvas.height = canvas.parentElement.clientHeight;
        this.ctx = canvas.getContext("2d");
        this.editor = editor;
        this.showLines = true;
        this.realOffsetX = 0;
        this.realOffsetY = 0;
        this.virtualOffsetX = 0;
        this.virtualOffsetY = 0;
        this.zoom = 1;
        this.pixelSize = 0;
        this.gridSize = 1;
        this.mainGridColor = new Color(64, 64, 64, 0.7);
        this.secondaryGridColor = new Color(192, 192, 192, 0.7);
        this.backgroundColor = Colors.WHITE;
        this.backgroundColor2 = new Color(192, 192, 192);
        this.selectColor = new Color(255, 255, 255, 0.3);
        this.canvasWidth = this.canvas.clientWidth;
        this.canvasHeight = this.canvas.clientHeight;
        this.finalPixelSize = 0;
        this.imageData = undefined
        var renderer = this;
        this.autorun = false;
        this.animationStep = 0;
        this.totalAnimationSteps = 4;
        window.onresize = function () {
            var oldWidth = canvas.width;
            var oldHeight = canvas.height;
            canvas.width = canvas.parentElement.clientWidth;
            canvas.height = canvas.parentElement.clientHeight;
            renderer.addOffset((canvas.width - oldWidth) / 2, (canvas.height - oldHeight) / 2)
            renderer.updateCanvasVariables();
            renderer.render();
        }
    }

    createImageData() {
        if (this.finalPixelSize > 0) {
            var start = this.rawToPixel(0, 0);
            var end = this.rawToPixel(this.canvasWidth - 1, this.canvasHeight - 1);
            end.x++;
            end.y++;

            var width = end.x - start.x + 1;
            var height = end.y - start.y + 1;

            this.imageData = this.ctx.createImageData(this.finalPixelSize * width, this.finalPixelSize * height);
            //Setting all of the alpha values to 255 so it doesnt need to be done on every render
            for (var i = 3; i < this.imageData.data.length; i = i + 4) {
                this.imageData.data[i] = 255;
            }
        }
    }

    updateCanvasVariables() {
        this.canvasWidth = this.canvas.clientWidth;
        this.canvasHeight = this.canvas.clientHeight;
        var d = this.editor.getDimensions();
        var auxW = this.canvasWidth / (d.width + borderAmount * 2);
        var auxH = this.canvasHeight / (d.height + borderAmount * 2);
        if (auxH < auxW) {
            auxW = auxH;
        }
        this.pixelSize = auxW | 0;

        this.finalPixelSize = Math.round(this.pixelSize * this.zoom);
        this.createImageData();
    }

    reset() {
        this.zoom = 1;
        var d = this.editor.getDimensions();
        this.realOffsetX = ((this.canvasWidth - d.width * this.finalPixelSize) / 2)
        this.realOffsetY = ((this.canvasHeight - d.height * this.finalPixelSize) / 2)
        this.virtualOffsetX = this.realOffsetX | 0
        this.virtualOffsetY = this.realOffsetY | 0
        this.createImageData()
        this.render();
    }

    setup() {
        this.updateCanvasVariables();
        this.reset();
    }

    getZoom() {
        return this.zoom;
    }

    setZoom(zoom) {
        if (zoom > 0) {
            var centerX = this.canvasWidth / 2;
            var centerY = this.canvasHeight / 2;
            var zoomRatio = zoom / this.zoom;
            this.realOffsetX = centerX - (centerX - this.realOffsetX) * zoomRatio;
            this.realOffsetY = centerY - (centerY - this.realOffsetY) * zoomRatio;
            this.virtualOffsetX = this.realOffsetX | 0
            this.virtualOffsetY = this.realOffsetY | 0
            this.zoom = zoom;
            this.finalPixelSize = Math.round(this.pixelSize * zoom);
            this.createImageData();
            this.render();
        }
    }

    render() {
        var frame = this.editor.getActiveFrameIndex();

        var d = this.editor.getDimensions();
        var start = this.rawToPixel(0, 0);
        var end = this.rawToPixel(this.canvasWidth - 1, this.canvasHeight - 1);
        end.x++;
        end.y++;

        var width = end.x - start.x + 1;

        var line = this.finalPixelSize * width * 4
        var aux = width * this.finalPixelSize
        var aux2 = this.finalPixelSize * 4
        var halfSize = (this.finalPixelSize / 2 - this.gridSize) | 0;
        var isSecondaryGrid;
        var color;
        var pixelColor;
        var pixelColor2;
        var borderColor;
        var yx;
        var x;
        var p;
        var h;
        var hx;
        var w;
        var pf;
        var animationStep = this.animationStep;
        var invertedColor;
        var isSelected;
        var auxColor;
        var hasTopSelectBorder;
        var hasBottomSelectBorder;
        var hasLeftSelectBorder;
        var hasRightSelectBorder;
        for (var y = start.y; y < end.y; y++) {
            yx = (y - start.y) * aux
            for (x = start.x; x < end.x; x++) {
                p = (x - start.x + yx) * aux2

                color = this.editor.getFinalPixel(x, y, frame);
                pixelColor = color.over(this.backgroundColor);
                pixelColor2 = color.over(this.backgroundColor2);
                invertedColor = pixelColor.inverse();
                isSelected = this.editor.isSelectedPixel(x, y);
                hasTopSelectBorder = isSelected != this.editor.isSelectedPixel(x, y - 1);
                hasBottomSelectBorder = isSelected != this.editor.isSelectedPixel(x, y + 1);
                hasLeftSelectBorder = isSelected != this.editor.isSelectedPixel(x - 1, y);
                hasRightSelectBorder = isSelected != this.editor.isSelectedPixel(x + 1, y);
                if (isSelected) {
                    pixelColor = this.selectColor.over(pixelColor);
                    pixelColor2 = this.selectColor.over(pixelColor2);
                }
                if ((x >= 0 && x < d.width) && (y >= 0 && y < d.height)) {
                    borderColor = this.mainGridColor.over(pixelColor);
                    isSecondaryGrid = false;
                } else {
                    borderColor = this.secondaryGridColor.over(pixelColor);
                    isSecondaryGrid = true;
                }
                //Top Border
                for (h = 0; h < this.gridSize; h++) {
                    hx = p + h * line;
                    if (hasTopSelectBorder) {
                        for (w = 0; w < this.finalPixelSize; w++) {
                            pf = hx + w * 4;
                            if (w % this.totalAnimationSteps == animationStep) {
                                this.imageData.data[pf] = pixelColor.red;
                                this.imageData.data[pf + 1] = pixelColor.green;
                                this.imageData.data[pf + 2] = pixelColor.blue;
                            } else {
                                this.imageData.data[pf] = invertedColor.red;
                                this.imageData.data[pf + 1] = invertedColor.green;
                                this.imageData.data[pf + 2] = invertedColor.blue;
                            }
                        }
                    } else {
                        for (w = 0; w < this.finalPixelSize; w++) {
                            pf = hx + w * 4;
                            this.imageData.data[pf] = borderColor.red;
                            this.imageData.data[pf + 1] = borderColor.green;
                            this.imageData.data[pf + 2] = borderColor.blue;
                        }
                    }
                }
                //Left-Right Border/Center
                for (; h < this.finalPixelSize - this.gridSize; h++) {
                    hx = p + h * line;
                    //Left Border
                    if (hasLeftSelectBorder) {
                        if (h % this.totalAnimationSteps == animationStep) {
                            auxColor = pixelColor;
                        } else {
                            auxColor = invertedColor;
                        }
                    } else {
                        auxColor = borderColor;
                    }

                    for (w = 0; w < this.gridSize; w++) {
                        pf = hx + w * 4;
                        this.imageData.data[pf] = auxColor.red;
                        this.imageData.data[pf + 1] = auxColor.green;
                        this.imageData.data[pf + 2] = auxColor.blue;
                    }
                    //Center
                    for (; w < this.finalPixelSize - this.gridSize; w++) {
                        pf = hx + w * 4;
                        if (isSecondaryGrid || (h <= halfSize && w <= halfSize || h > halfSize && w > halfSize)) {
                            this.imageData.data[pf] = pixelColor.red;
                            this.imageData.data[pf + 1] = pixelColor.green;
                            this.imageData.data[pf + 2] = pixelColor.blue;
                        } else {
                            this.imageData.data[pf] = pixelColor2.red;
                            this.imageData.data[pf + 1] = pixelColor2.green;
                            this.imageData.data[pf + 2] = pixelColor2.blue;
                        }
                    }
                    //Right Border
                    if (hasRightSelectBorder) {
                        if (h % this.totalAnimationSteps == animationStep) {
                            auxColor = pixelColor;
                        } else {
                            auxColor = invertedColor;
                        }
                    } else {
                        auxColor = borderColor;
                    }
                    for (; w < this.finalPixelSize; w++) {
                        pf = hx + w * 4;
                        this.imageData.data[pf] = auxColor.red;
                        this.imageData.data[pf + 1] = auxColor.green;
                        this.imageData.data[pf + 2] = auxColor.blue;
                    }
                }
                //Bottom Border
                for (; h < this.finalPixelSize; h++) {
                    hx = p + h * line;
                    if (hasBottomSelectBorder) {
                        for (w = 0; w < this.finalPixelSize; w++) {
                            pf = hx + w * 4;
                            if (w % this.totalAnimationSteps == animationStep) {
                                this.imageData.data[pf] = pixelColor.red;
                                this.imageData.data[pf + 1] = pixelColor.green;
                                this.imageData.data[pf + 2] = pixelColor.blue;
                            } else {
                                this.imageData.data[pf] = invertedColor.red;
                                this.imageData.data[pf + 1] = invertedColor.green;
                                this.imageData.data[pf + 2] = invertedColor.blue;
                            }
                        }
                    } else {
                        for (w = 0; w < this.finalPixelSize; w++) {
                            pf = hx + w * 4
                            this.imageData.data[pf] = borderColor.red;
                            this.imageData.data[pf + 1] = borderColor.green;
                            this.imageData.data[pf + 2] = borderColor.blue;
                        }
                    }
                }
            }
        }
        if (this.imageData) {
            var rawStart = this.pixelToRaw(start.x, start.y)
            this.ctx.putImageData(this.imageData, rawStart.x, rawStart.y);
        }

    }

    rawToPixel(x, y) {
        x = ((x - this.virtualOffsetX) / this.finalPixelSize);
        y = ((y - this.virtualOffsetY) / this.finalPixelSize);
        if (x < 0) {
            x -= 1;
        }
        if (y < 0) {
            y -= 1;
        }
        x = x | 0;
        y = y | 0;
        return {
            x: x,
            y: y
        };
    }

    pixelToRaw(x, y) {

        x = x * this.finalPixelSize + this.virtualOffsetX;
        y = y * this.finalPixelSize + this.virtualOffsetY;

        return {
            x: x,
            y: y
        };
    }

    getViewWindowDimensions() {
        return {
            start: this.rawToPixel(0, 0),
            end: this.rawToPixel(this.canvasWidth - 1, this.canvasHeight - 1)
        };
    }

    addOffset(x, y) {
        this.realOffsetX += x;
        this.realOffsetY += y;
        this.virtualOffsetX = this.realOffsetX | 0
        this.virtualOffsetY = this.realOffsetY | 0
        this.createImageData()
    }

    startAutorun() {
        if (!this.autorun) {
            this.autorun = true;
            this.run()
        }
    }

    stopAutorun() {
        this.autorun = false;
    }

    run() {
        this.render();
        this.animationStep++;
        if (this.animationStep == this.totalAnimationSteps) {
            this.animationStep = 0;
        }
        if (this.autorun) {
            var r = this
            setTimeout(function () {
                r.run();
            }, 200);
        }
    }
}