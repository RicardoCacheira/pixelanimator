class Color {
    static _colorOver(a, b, alphaRatio) {
        return alphaRatio * a + (1 - alphaRatio) * b;
    }
    static _numHex(num) {
        var result = num.toString(16);
        if (result.length == 1) {
            result = "0" + result;
        }
        return result;
    }

    static hsv(hue, saturation, value, alpha) {
        hue = hue % 360;
        var c = value * saturation;
        var x = c * (1 - Math.abs(((hue / 60) % 2) - 1))
        var m = value - c;
        var r, g, b;
        if (hue < 60) {
            r = c;
            g = x;
            b = 0;
        } else if (hue < 120) {
            r = x;
            g = c;
            b = 0;
        } else if (hue < 180) {
            r = 0;
            g = c;
            b = x;
        } else if (hue < 240) {
            r = 0;
            g = x;
            b = c;
        } else if (hue < 300) {
            r = x;
            g = 0;
            b = c;
        } else if (hue < 360) {
            r = c;
            g = 0;
            b = x;
        }
        return new Color(Math.round((r + m) * 255), Math.round((g + m) * 255), Math.round((b + m) * 255), alpha);
    }

    static lerp(v0, v1, t) {
        return (1 - t) * v0 + t * v1;
    }

    static _normalizeSignatureValue(value, name) {
        value = Number.parseInt(value, 16);
        if (Number.isNaN(value)) {
            throw name + " not a number";
        }
        if (value < 0) {
            throw "negative " + name + " value";
        }
        return value;
    }

    static fromSignature(signature) {
        if (typeof signature != "string") {
            throw "Non-string signature";
        }

        var alpha = Number.parseFloat(signature.substring(6));
        if (Number.isNaN(alpha)) {
            throw "Alpha not a number";
        }
        if (alpha < 0) {
            throw "Negative Alpha value";
        }

        if (alpha > 1) {
            throw "Alpha over 1";
        }
        return new Color(Color._normalizeSignatureValue(signature.substring(0, 2), "Red"), Color._normalizeSignatureValue(signature.substring(2, 4), "Green"), Color._normalizeSignatureValue(signature.substring(4, 6), "Blue"), alpha);
    }

    static average(list) {
        var red = 0;
        var green = 0;
        var blue = 0;
        var alpha = 0;
        for (var c of list) {
            red += c.red;
            green += c.green;
            blue += c.blue;
            alpha += c.alpha;
        }
        return new Color(red / list.length, green / list.length, blue / list.length, alpha / list.length);
    }

    constructor(red = 0, green = 0, blue = 0, alpha = 1) {
        this.alpha = normalizeAlpha(alpha);
        if (this.alpha == 0) {
            this.red = 0;
            this.green = 0;
            this.blue = 0;
        } else {
            this.red = normalizeRGBValue(red);
            this.green = normalizeRGBValue(green);
            this.blue = normalizeRGBValue(blue);
        }
    }

    over(b) {
        if (this.equals(Colors.CLEAR)) {
            return b;
        }
        if (b.equals(Colors.CLEAR)) {
            return this;
        }
        var alpha = this.alpha + b.alpha * (1 - this.alpha);
        if (alpha) {
            var alphaRatio = this.alpha / alpha;
            return (new Color(Color._colorOver(this.red, b.red, alphaRatio), Color._colorOver(this.green, b.green, alphaRatio), Color._colorOver(this.blue, b.blue, alphaRatio), alpha))
        }
        return Colors.CLEAR;
    }

    inverse() {
        return new Color(255 - this.red, 255 - this.green, 255 - this.blue, this.alpha);
    }

    equals(color) {
        return (this.red == color.red) && (this.green == color.green) && (this.blue == color.blue) && (this.alpha == color.alpha);
    }

    toString() {
        return 'rgba(' + this.red + ',' + this.green + ',' + this.blue + ',' + this.alpha + ')';
    }

    toHSV() {
        var r = this.red / 255;
        var g = this.green / 255;
        var b = this.blue / 255;
        var cmin = Math.min(r, g, b);
        var cmax = Math.max(r, g, b);
        var delta = cmax - cmin;
        var hue;
        var saturation;
        if (delta == 0) {
            hue = 0;
        } else if (r == cmax) {
            hue = 60 * (((g - b) / delta) % 6);
        } else if (g == cmax) {
            hue = 60 * (((b - r) / delta) + 2);
        } else {
            hue = 60 * (((r - g) / delta) + 4);
        }
        if (cmax) {
            saturation = delta / cmax;
        } else {
            saturation = 0;
        }
        hue = Math.round(hue);
        if (hue < 0) {
            hue += 360;
        }
        return {
            hue: hue,
            saturation: saturation,
            value: cmax
        };
    }

    toHex() {
        return "#" + Color._numHex(this.red) + Color._numHex(this.green) + Color._numHex(this.blue);
    }

    toSignature() {
        return Color._numHex(this.red) + Color._numHex(this.green) + Color._numHex(this.blue) + this.alpha;
    }

    gradient(finalColor, values) {
        var list = [this];
        values = values = Number.parseInt(values) - 1;
        if (!Number.isNaN(values) && values > 1) {
            for (var i = 1; i <= values; i++) {
                var t = i / values;
                list.push(new Color(
                    Color.lerp(this.red, finalColor.red, t),
                    Color.lerp(this.green, finalColor.green, t),
                    Color.lerp(this.blue, finalColor.blue, t),
                    Color.lerp(this.alpha, finalColor.alpha, t)
                ));
            }
        }
        return list;
    }

}

class ColorPalette {

    static compare(a, b) {
        return b.num - a.num;
    }

    constructor(colors = []) {
        this.palette = {}
        for (var color of colors) {
            this.addColor(color);
        }
    }

    addColor(color) {
        if (color instanceof Color) {
            var id = color.toSignature();
            if (!this.palette[id]) {
                this.palette[id] = {
                    color: color,
                    num: 1
                };
            } else {
                this.palette[id].num++;
            }
            return this.palette[id].color;
        }
        return Colors.CLEAR;
    }

    merge(palette) {
        if (palette instanceof ColorPalette) {
            for (var color in palette.palette) {
                if (!this.palette[color]) {
                    this.palette[color] = palette.palette[color]
                }
            }
        }
    }

    toList() {
        var list = [];
        for (var c in this.palette) {
            list.push(this.palette[c]);
        }
        list.sort(ColorPalette.compare);
        for (var i = 0; i < list.length; i++) {
            list[i] = list[i].color;
        }
        return list;
    }
}

var Colors = {
    CLEAR: new Color(0, 0, 0, 0),
    BLACK: new Color(0, 0, 0, 1),
    WHITE: new Color(255, 255, 255, 1),
    GREY: new Color(128, 128, 128, 1),
    RED: new Color(255, 0, 0, 1),
    GREEN: new Color(0, 255, 0, 1),
    BLUE: new Color(0, 0, 255, 1),
    CYAN: new Color(0, 255, 255, 1),
    MAGENTA: new Color(255, 0, 255, 1),
    YELLOW: new Color(255, 255, 0, 1)
}