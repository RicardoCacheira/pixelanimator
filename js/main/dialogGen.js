var OptionTypes = {
    TEXT: 0,
    INPUT: 1, //TODO
    NUMBER: 2, //TODO
    CHECK: 3, //TODO
    LIST: 4, //TODO
    COLOR: 5,
    FILE: 6, //TODO
    POS: 7, //TODO
    FRAME: 8, //TODO
    LAYER: 9, //TODO
    SPRITE: 10 //TODO
}

/*
type - the type of option.
var - the name of the resulting var from this option (required).
name - the name that apears in the dialog window. If none them it defaults to the var.

Types

TEXT - this is a special type that doesnt require the var option, it simply shows text.
    text - the text to be displayed

INPUT - this requests a text input.
    notEmpty - forces that the input is not empty, by default is false.
    regex - checks if the input is acepted by the given regex.

NUMBER - this requests a valid number.
    isFloat - allows that the number is a float, by default is false.
    min - minimum value the the number can be, optional.
    max - maximum value the the number can be, optional.

CHECK - this shows a checkbox
    isCheked - defines if the checkbox is checked by default

LIST - this requests that it selects a value from a list
    list - the list containing all possible values.
    start - the index of the starting value, if no value is set then no starting value is set

COLOR - this requests a color value
    color - the starting color, by default is white (#FFFFFFFF)

FILE - this requests a file

POS - this requests a position on the editor
    inside - this forces that the chosen position must be inside the image

FRAME - this requests a frame
    multiple - this makes the choice be of multiple frames

LAYER - this requests a layer

SPRITE - this requests a sprite

*/

class DialogGen {

    constructor(editor) {
        this.editor = editor;
        this.dialogCount = 0;
    }

    static generateRangeTr(name, min, max, step, value, type) {
        var atributes = {
            min: min,
            max: max,
            step: step,
            value: value
        };
        var range = input('range').attr(atributes);

        var number = input('number').attr(atributes).css('width', '40px');

        var elem = tr(td(name + ': '), td(range), td(number))

        range.oninput = function () {
            number.value = this.value;
            elem.update();
        }

        number.onchange = function () {
            if (isFloat) {
                var value = Number.parseFloat(this.value);
            } else {
                var value = Number.parseInt(this.value);
            }
            if (Number.isNaN(value)) {
                value = 0;
            }
            if (value > max) {
                value = max;
            } else if (value < min) {
                value = min;
            }
            this.value = value;
            range.value = value;
            elem.update();
        }

        return elem;
    }

    static updateRangeBackgound(red, green, blue, hue, saturation, value, alpha) {
        var v_red = red.child(1, 0).value;
        var v_green = green.child(1, 0).value;
        var v_blue = blue.child(1, 0).value;
        var v_hue = hue.child(1, 0).value;
        var v_saturation = saturation.child(1, 0).value / 100;
        var v_value = value.child(1, 0).value / 100;
        var v_alpha = alpha.child(1, 0).value;
        red.child(1).css('background', 'linear-gradient(to right, ' + new Color(0, v_green, v_blue) + ' , ' + new Color(255, v_green, v_blue) + ')');
        green.child(1).css('background', 'linear-gradient(to right, ' + new Color(v_red, 0, v_blue) + ' , ' + new Color(v_red, 255, v_blue) + ')');
        blue.child(1).css('background', 'linear-gradient(to right, ' + new Color(v_red, v_green, 0) + ' , ' + new Color(v_red, v_green, 255) + ')');
        hue.child(1).css('background', 'linear-gradient(to right, ' + Color.hsv(0, v_saturation, v_value) + ' , ' + Color.hsv(60, v_saturation, v_value) + ' , ' + Color.hsv(120, v_saturation, v_value) + ' , ' + Color.hsv(180, v_saturation, v_value) + ' , ' + Color.hsv(240, v_saturation, v_value) + ' , ' + Color.hsv(300, v_saturation, v_value) + ' , ' + Color.hsv(0, v_saturation, v_value) + ')');
        saturation.child(1).css('background', 'linear-gradient(to right, ' + Color.hsv(v_hue, 0, v_value) + ' , ' + Color.hsv(v_hue, 1, v_value) + ')');
        value.child(1).css('background', 'linear-gradient(to right, ' + Color.hsv(v_hue, v_saturation, 0) + ' , ' + Color.hsv(v_hue, v_saturation, 1) + ')');
        alpha.child(1).css('background', 'linear-gradient(to right, ' + new Color(v_red, v_green, v_blue, 0) + ' , ' + new Color(v_red, v_green, v_blue, 255) + ')');
    }


    generateInfoDialog(info = "") {
        this.addDialog();

        var gen = this;
        var ok = input('button').attr('value', 'OK').addClass('dialog-button');
        ok.onclick = function () {
            var dialog = this.parent(1);
            if (dialog.style.zIndex == gen.dialogCount) {
                dialog.remove();
                gen.removeDialog();
            }
        }
        document.body.appendChild(div(div(info), div(ok).css('text-align', 'center')).addClass('dialog').css('zIndex', this.dialogCount));
    }

    generateCustomDialog(element, callback) {
        this.addDialog();
        var gen = this;
        var cancel = input('button').attr('value', 'Cancel').addClass('dialog-button');
        cancel.onclick = function () {
            var dialog = this.parent(1);
            if (dialog.style.zIndex == gen.dialogCount) {
                dialog.remove();
                gen.removeDialog();
                if (callback) {
                    callback(false);
                }
            }
        }

        var ok = input('button').attr('value', 'OK').addClass('dialog-button');
        ok.onclick = function () {
            var dialog = this.parent(1);
            if (dialog.style.zIndex == gen.dialogCount) {
                dialog.remove();
                gen.removeDialog();
                if (callback) {
                    callback(true);
                }
            }
        }
        document.body.appendChild(div(element, div(cancel, ok).css('text-align', 'center')).addClass('dialog').css('zIndex', this.dialogCount));
    }

    generateConfirmDialog(info = "", callback) {
        this.addDialog();
        var gen = this;
        var cancel = input('button').attr('value', 'No').addClass('dialog-button');
        cancel.onclick = function () {
            var dialog = this.parent(1);
            if (dialog.style.zIndex == gen.dialogCount) {
                dialog.remove();
                gen.removeDialog();
                if (callback) {
                    callback(false);
                }
            }
        }

        var ok = input('button').attr('value', 'Yes').addClass('dialog-button');
        ok.onclick = function () {
            var dialog = this.parent(1);
            if (dialog.style.zIndex == gen.dialogCount) {
                var dialog = this.parent(1);
                dialog.remove();
                gen.removeDialog();
                if (callback) {
                    callback(true);
                }
            }
        }
        document.body.appendChild(div(div(info), div(cancel, ok).css('text-align', 'center')).addClass('dialog').css('zIndex', this.dialogCount));
    }

    generateColorDialog(color, callback, title = "Color Picker") {
        this.addDialog();
        if ((!color) || (!(color instanceof Color))) {
            color = Colors.WHITE;
        }
        var hsv = color.toHSV();

        var titleTh = th(title).attr('colspan', 2);
        var colorPreview = div().css('width', '80px', 'height', '80px', 'background-color', color);
        var colorPreviewBackgound = div(colorPreview).css('border', '1px solid black', 'width', '80px', 'height', '80px').addClass('checkerboard');
        var red = DialogGen.generateRangeTr('Red', 0, 255, 1, color.red);
        var green = DialogGen.generateRangeTr('Green', 0, 255, 1, color.green);
        var blue = DialogGen.generateRangeTr('Blue', 0, 255, 1, color.blue);
        var hue = DialogGen.generateRangeTr('Hue', 0, 360, 1, hsv.hue);
        var saturation = DialogGen.generateRangeTr('Saturation', 0, 100, 1, Math.round(hsv.saturation * 100));
        var value = DialogGen.generateRangeTr('Value', 0, 100, 1, Math.round(hsv.value * 100));
        var alpha = DialogGen.generateRangeTr('Alpha', 0, 1, 0.01, color.alpha, true);
        DialogGen.updateRangeBackgound(red, green, blue, hue, saturation, value, alpha);

        var updatePreviewRGB = function () {
            var color = new Color(red.child(1, 0).value, green.child(1, 0).value, blue.child(1, 0).value, alpha.child(1, 0).value);
            colorPreview.css('background-color', color);
            var hsv = color.toHSV();
            hsv.saturation = Math.round(hsv.saturation * 100);
            hsv.value = Math.round(hsv.value * 100);
            hue.child(1, 0).value = hsv.hue;
            hue.child(2, 0).value = hsv.hue;
            saturation.child(1, 0).value = hsv.saturation;
            saturation.child(2, 0).value = hsv.saturation;
            value.child(1, 0).value = hsv.value;
            value.child(2, 0).value = hsv.value;
            DialogGen.updateRangeBackgound(red, green, blue, hue, saturation, value, alpha);
        }

        var updatePreviewHSV = function () {
            var color = Color.hsv(hue.child(1, 0).value, saturation.child(1, 0).value / 100, value.child(1, 0).value / 100, alpha.child(1, 0).value);
            colorPreview.css('background-color', color);
            red.child(1, 0).value = color.red;
            red.child(2, 0).value = color.red;
            green.child(1, 0).value = color.green;
            green.child(2, 0).value = color.green;
            blue.child(1, 0).value = color.blue;
            blue.child(2, 0).value = color.blue;
            DialogGen.updateRangeBackgound(red, green, blue, hue, saturation, value, alpha);
        }

        var updatePreviewAlpha = function () {
            colorPreview.css('background-color', new Color(red.child(1, 0).value, green.child(1, 0).value, blue.child(1, 0).value, alpha.child(1, 0).value));
            DialogGen.updateRangeBackgound(red, green, blue, hue, saturation, value, alpha);
        }

        red.update = updatePreviewRGB;
        green.update = updatePreviewRGB;
        blue.update = updatePreviewRGB;
        hue.update = updatePreviewHSV;
        saturation.update = updatePreviewHSV;
        value.update = updatePreviewHSV;
        alpha.update = updatePreviewAlpha;

        var gen = this;
        var cancel = input('button').attr('value', 'Cancel').addClass('dialog-button');
        cancel.onclick = function () {
            var dialog = this.parent(1);
            if (dialog.style.zIndex == gen.dialogCount) {
                dialog.remove();
                gen.removeDialog();
                if (callback) {
                    callback();
                }
            }
        }

        var ok = input('button').attr('value', 'Ok').addClass('dialog-button');
        ok.onclick = function () {
            var dialog = this.parent(1);
            if (dialog.style.zIndex == gen.dialogCount) {
                dialog.remove();
                gen.removeDialog();
                if (callback) {
                    callback(new Color(red.child(1, 0).value, green.child(1, 0).value, blue.child(1, 0).value, alpha.child(1, 0).value));
                }
            }
        }
        document.body.appendChild(div(table(tr(titleTh), tr(th(colorPreviewBackgound), th(table(red, green, blue, tr(td()), hue, saturation, value, tr(td()), alpha)))), div(cancel, ok).css('text-align', 'center')).addClass('dialog').css('zIndex', this.dialogCount));
    }

    generateDialog(opcs, callback, title) {
        this.addDialog();
        var generatedOptions = [];
        for (var i = 0; i < opcs.length; i++) {
            var opc = opcs[i];
            if (!this.checkOptions(opc)) {
                console.log("Error in opc " + i);
                return;
            }
            switch (opc.type) {
                case OptionTypes.TEXT:
                    var textTd = td(opc.text).attr('colspan', 2);
                    generatedOptions.push(tr(textTd));
                    break;
                case OptionTypes.INPUT:
                    break;
                case OptionTypes.NUMBER:
                    break;
                case OptionTypes.CHECK:
                    break;
                case OptionTypes.LIST:
                    break;
                case OptionTypes.COLOR:
                    generatedOptions.push(createColorOption(opc, this));
                    break;
                case OptionTypes.FILE:
                    break;
                case OptionTypes.POS:
                    break;
                case OptionTypes.FRAME:
                    break;
                case OptionTypes.SPRITE:
                    break;
            }
            generatedOptions.push(tr(td().css('display', 'none').addClass('error')));
        }

        var gen = this;
        var cancel = input('button').attr('value', 'Cancel').addClass('dialog-button');
        cancel.onclick = function () {
            var dialog = this.parent(1);
            if (dialog.style.zIndex == gen.dialogCount) {
                dialog.remove();
                gen.removeDialog();
                callback();
            }
        }
        var ok = input('button').attr('value', 'Ok').addClass('dialog-button');
        ok.onclick = function () {
            var dialog = this.parent(1);
            if (dialog.style.zIndex == gen.dialogCount) {
                if (gen.checkOptionsState(opcs, dialog)) {
                    dialog.remove();
                    gen.removeDialog();
                    if (callback) {
                        callback(gen.createOptionsResult(opcs, dialog));
                    }
                }
            }
        }

        document.body.appendChild(div(table(tr(th(title).attr('colspan', 2).css('display', (title ? 'table-cell' : 'none'))), generatedOptions), div(cancel, ok, style, 'text-align', 'center')).addClass('dialog').css('zIndex', this.dialogCount));
    }

    checkOptions(opc) {
        if (opc.type != undefined) {
            if (opc.type != OptionTypes.TEXT) {
                if (opc.var) {
                    if (opc.name == undefined) {
                        opc.name = opc.var;
                    }
                    switch (opc.type) {
                        case OptionTypes.INPUT:
                            break;
                        case OptionTypes.NUMBER:
                            break;
                        case OptionTypes.CHECK:
                            break;
                        case OptionTypes.LIST:
                            break;
                        case OptionTypes.COLOR:
                            return checkColorOption(opc);
                        case OptionTypes.FILE:
                            break;
                        case OptionTypes.POS:
                            break;
                        case OptionTypes.FRAME:
                            break;
                        case OptionTypes.SPRITE:
                            break;
                    }
                }

            } else {
                return true;
            }
        }
        return false;

    }

    checkOptionsState(opcs, dialog) {
        var passed = true;
        for (var i = 0; i < opcs.length; i++) {
            var opc = opcs[i];
            var opcElem = dialog.child(0, i * 2 + 1);
            var fail;
            switch (opc.type) {
                case OptionTypes.INPUT:
                    break;
                case OptionTypes.NUMBER:
                    break;
                case OptionTypes.LIST:
                    break;
                    /* 
                    is always valid
                    case OptionTypes.COLOR:
                         fail = checkColorOptionState(opc, opcElem);
                         break;*/
                case OptionTypes.FILE:
                    break;
                case OptionTypes.POS:
                    break;
                case OptionTypes.FRAME:
                    break;
                case OptionTypes.SPRITE:
                    break;
            }
            if (fail) {
                this.showError(opcElem, fail);
                passed = false;
            } else {
                this.showOk(opcElem);
            }
        }
        return passed;

    }

    showError(elem, error) {
        elem.nextSibling.style.display = 'block';
        elem.nextSibling.clear().append(span(error).addClass('error'));
    }

    showOk(elem) {
        elem.nextSibling.css('display', 'none').clear();
    }

    createOptionsResult(opcs, dialog) {
        var result = {};
        for (var i = 0; i < opcs.length; i++) {
            var opc = opcs[i];
            var opcElem = dialog.child(0, i * 2 + 1);
            switch (opc.type) {
                case OptionTypes.INPUT:
                    break;
                case OptionTypes.NUMBER:
                    break;
                case OptionTypes.LIST:
                    break;
                case OptionTypes.COLOR:
                    result[opc.var] = getColorOptionResult(opc, opcElem);
                    break;
                case OptionTypes.FILE:
                    break;
                case OptionTypes.POS:
                    break;
                case OptionTypes.FRAME:
                    break;
                case OptionTypes.SPRITE:
                    break;
            }
        }
        return result;
    }

    addDialog() {
        this.dialogCount++;
        this.editor.disableCanvas();
    }

    removeDialog() {
        this.dialogCount--;
        if (this.dialogCount == 0) {
            this.editor.enableCanvas();
        }
    }
}

// CREATE,CHECK,CHECK STATE, GET RESULT

// ================ Color Option Start ================
function createColorOption(opc, gen) {
    var button = input('button').css('width', '40px', 'background-color', opc.color);
    button.color = opc.color;
    button.onclick = function () {
        gen.generateColorDialog(opc.color, function (c) {
            if (c) {
                button.color = c;
                button.css('background-color', c);
            }
        });
    }
    return tr(td(opc.name + ": "), td(button))
}

function checkColorOption(opc) {
    if ((!opc.color) || (!(opc.color instanceof Color))) {
        opc.color = Colors.WHITE;
    }
    return true;
}

function getColorOptionResult(opc, opcElem) {
    return opcElem.child(1, 0).color;
}
// ================ Color Option End ================