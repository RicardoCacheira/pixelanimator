const defaultWidth = 16
const defaultHeight = 16

const ToolTypes = {
    FUNCTION: 0,
    FUNCTION_LIST: 1,
    BUTTON: 2,
    CLICK: 3,
    DRAG: 4,
    TOOL_LIST: 5
}

debugMode = true;

class Editor {
    constructor(canvas, width = defaultWidth, height = defaultHeight) {
        this.supportsTouch = true;
        try {
            TouchEvent
        } catch (e) {
            this.supportsTouch = false;
        }
        this.canvas = canvas;
        this.exporter = new Exporter(this);
        this.renderer = new Renderer(canvas, this);
        this.dialogGen = new DialogGen(this);
        this.toolManager = new ToolManager(this);
        this.layers = undefined;
        this.activeLayer = undefined;
        this.activeLayerIndex = 0;
        this.activeFrameIndex = 0;
        this.sprites = [];
        this.frameCount = 0;
        this.width = width;
        this.height = height;
        this.activeTool = undefined;
        this.activeToolType = undefined;
        this.moving = false;
        this.lastMove = undefined;
        this.lastPixelMove = {
            x: NaN,
            y: NaN
        };
        this.rawPos = false;
        this.emulateMissingEvents = false;
        this.previewMode = false;
        this.deactivateToolOnEnd = false;
        this.editedFrames = [];
        this.allFramesEdited = false;
        this.editedLayers = [];
        this.allLayersEdited = false;
        this.listeners = [];
        this.functions = {};
        this.defaultTool = undefined;
        this.onPreview = false;
        this.setPrimaryColor(Colors.BLACK);
        this.setSecondaryColor(Colors.WHITE);
        this.unselectAll();
        var editor = this;
        this.canvasOnClick = function (evt) {
            if (editor.activeTool && editor.activeToolType == ToolTypes.CLICK) {
                if (editor.rawPos) {
                    editor.activeTool.run(editor.renderer.rawToPixel(evt.offsetX, evt.offsetY), {
                        x: evt.offsetX,
                        y: evt.offsetY
                    });
                } else {
                    editor.activeTool.run(editor.renderer.rawToPixel(evt.offsetX, evt.offsetY));
                }
                editor.render();
                if (editor.deactivateToolOnEnd) {
                    editor.deactivateActiveTool();
                }
            }
        }

        this.canvasOnMouseDown = function (evt) {
            if (!editor.activeTool && editor.defaultTool) {
                editor.defaultTool.activate();
            }
            if (editor.activeTool && editor.activeToolType == ToolTypes.DRAG) {
                if (editor.supportsTouch) {
                    addTouchEventOffset(evt);
                }
                editor.moving = true;
                if (editor.previewMode) {
                    editor.startPreview();
                }
                var pos = editor.renderer.rawToPixel(evt.offsetX, evt.offsetY);
                if (editor.rawPos) {
                    editor.lastMove = evt;
                    editor.activeTool.start(pos, {
                        x: evt.offsetX,
                        y: evt.offsetY
                    });
                } else {
                    editor.lastMove = pos;
                    editor.activeTool.start(pos);
                }
                editor._triggerEvent("mouseDown", pos, editor.activeTool.showStartPos, editor.activeTool.showPosDifference);
                editor.render();
            }
        }
        canvas.onclick = this.canvasOnClick;
        canvas.onmousedown = this.canvasOnMouseDown;

        canvas.onmousemove = function (evt) {
            evt.preventDefault();
            var pos = editor.renderer.rawToPixel(evt.offsetX, evt.offsetY);
            if (editor.moving && editor.activeTool && editor.activeToolType == ToolTypes.DRAG) {
                if (editor.supportsTouch) {
                    addTouchEventOffset(evt);
                }
                if (editor.rawPos) {
                    if (editor.emulateMissingEvents) {
                        var missingEvents = calculateLinePoints(editor.lastMove.offsetX, editor.lastMove.offsetY, evt.offsetX, evt.offsetY);
                        missingEvents.shift();
                        for (var missingEvent of missingEvents) {
                            pos = editor.renderer.rawToPixel(missingEvent.x, missingEvent.y);
                            if (editor.previewMode) {
                                editor.startPreview();
                            }
                            editor.activeTool.move(pos, missingEvent);
                        }
                    } else {
                        if (editor.previewMode) {
                            editor.startPreview();
                        }
                        editor.activeTool.move(pos, {
                            x: evt.offsetX,
                            y: evt.offsetY
                        });
                    }
                    editor.lastMove = evt;
                    editor.render();
                } else {
                    if (pos.x != editor.lastMove.x || pos.y != editor.lastMove.y) {
                        if (editor.emulateMissingEvents) {
                            var missingEvents = calculateLinePoints(editor.lastMove.x, editor.lastMove.y, pos.x, pos.y);
                            missingEvents.shift();
                            for (var missingEvent of missingEvents) {
                                if (editor.previewMode) {
                                    editor.startPreview();
                                }
                                editor.activeTool.move(missingEvent);
                            }
                        } else {
                            if (editor.previewMode) {
                                editor.startPreview();
                            }
                            editor.activeTool.move(pos);
                        }
                        editor.lastMove = pos;
                        editor.render();
                    }
                }
            }
            if (pos.x != editor.lastPixelMove.x || pos.y != editor.lastPixelMove.y) {
                editor._triggerEvent("mouseMove", pos);
                editor.lastPixelMove = pos;
            }
        }

        window.onmouseup = function () {
            if (editor.moving && editor.activeTool && editor.activeToolType == ToolTypes.DRAG) {
                editor.moving = false;

                if (editor.previewMode) {
                    editor.endPreview();
                }
                if (editor.rawPos) {
                    editor.activeTool.end(editor.renderer.rawToPixel(editor.lastMove.offsetX, editor.lastMove.offsetY), {
                        x: editor.lastMove.offsetX,
                        y: editor.lastMove.offsetY
                    });
                } else {
                    editor.activeTool.end(editor.lastMove);
                }
                editor._triggerEvent("mouseUp", editor.lastPixelMove);
                editor.render();
                editor.lastMove = undefined;
                if (editor.deactivateToolOnEnd) {
                    editor.deactivateActiveTool();
                }
            }
        }

        canvas.onmousewheel = function (evt) {
            evt.preventDefault();
            editor.setZoom(editor.getZoom() + evt.deltaY / 100);
        }

        canvas.ontouchstart = canvas.onmousedown;
        canvas.ontouchmove = canvas.onmousemove;
        window.ontouchend = window.onmouseup;
        window.onbeforeunload = function () {
            editor.saveLocal()
        };
    }

    /**
     * Editor Functions
     *================================================================*/

    _triggerEvent(event) {
        event = event.trim();
        if (debugMode) {
            console.log("Trigger: " + [...arguments].toString(", "));
        }
        if (this.listeners[event]) {
            var args = [...arguments];
            args.shift();
            for (var callback of this.listeners[event]) {
                callback(...args);
            }
        }
    }

    addEventListener(event, callback) {
        event = event.trim();
        if (!this.listeners[event]) {
            this.listeners[event] = [];
        }
        this.listeners[event].push(callback);
    }

    getDimensions() {
        return {
            width: this.width,
            height: this.height
        }
    }

    //returns the start and end points of the area that should be considered
    getActionAreaDimensions() {
        var rendererDimensions = this.renderer.getViewWindowDimensions();
        var actionAreaDimensions = {
            start: {
                x: 0,
                y: 0
            },
            end: {
                x: this.width - 1,
                y: this.height - 1
            }
        }
        if (rendererDimensions.start.x < 0) {
            actionAreaDimensions.start.x = rendererDimensions.start.x;
        }
        if (rendererDimensions.start.y < 0) {
            actionAreaDimensions.start.y = rendererDimensions.start.y;
        }
        if (rendererDimensions.end.x >= this.width) {
            actionAreaDimensions.end.x = rendererDimensions.end.x;
        }
        if (rendererDimensions.end.y >= this.height) {
            actionAreaDimensions.end.y = rendererDimensions.end.y;
        }
        return actionAreaDimensions;
    }

    getPrimaryColor() {
        return this.primaryColor;
    }

    getSecondaryColor() {
        return this.secondaryColor;
    }

    setDimensions(width, height) {
        this.width = width;
        this.height = height;
    }

    setPrimaryColor(color) {
        if (color && (color instanceof Color)) {
            this.primaryColor = color;
            this._triggerEvent("colorChange", {
                type: 'primary',
                color: color
            });
        }
    }

    setSecondaryColor(color) {
        if (color && (color instanceof Color)) {
            this.secondaryColor = color;
            this._triggerEvent("colorChange", {
                type: 'secondary',
                color: color
            });
        }
    }

    setPrimaryColorRGBA(r, g, b, a) {
        this.setPrimaryColor(new Color(r, g, b, a));
    }

    setSecondaryColorRGBA(r, g, b, a) {
        this.setSecondaryColor(new Color(r, g, b, a));
    }

    switchColors() {
        var aux = this.primaryColor;
        this.setPrimaryColor(this.secondaryColor);
        this.setSecondaryColor(aux);
    }

    normalizeColorPalette() {
        this.mergeSelectionCanvas();
        var palette = new ColorPalette();
        for (var layer of this.layers) {
            layer.normalizeColorPalette(palette);
        }
        return palette;
    }

    startPreview() {
        if (this.onPreview) {
            this.endPreview();
        }
        this.onPreview = true;
        this.activeLayer.startPreview(this.activeFrameIndex);
        this.previewSelection = this.selectedPixels;
        if (this.selectionCanvas) {
            this.previewSelectionCanvas = this.selectionCanvas.clone();
        }
        this._unselectAll();
        for (var ix in this.previewSelection) {
            for (var iy of this.previewSelection[ix]) {
                this._selectPixel(ix, iy);
            }
        }
    }

    endPreview() {
        if (this.onPreview) {
            this.onPreview = false;
            this.activeLayer.endPreview(this.activeFrameIndex);
            this.selectedPixels = this.previewSelection;
            if (this.previewSelectionCanvas) {
                this.selectionCanvas = this.previewSelectionCanvas;
                this.previewSelectionCanvas = undefined;
            } else {
                this.selectionCanvas = undefined;
            }
            this.privewSelectionCanvas
        }
    }

    enableCanvas() {
        this.canvas.onclick = this.canvasOnClick;
        this.canvas.onmousedown = this.canvasOnMouseDown;
    }

    disableCanvas() {
        this.canvas.onclick = undefined;
        this.canvas.onmousedown = undefined;
    }

    start() {
        try {
            editor.loadLocal()
        } catch (err) {
            console.error(err);
            editor.loadNewProject();
        }
        this.renderer.setup();
        this.updateFunctionToolsList();
        this.updateActionToolsList()
    }

    /*================================================================*
     * Editor Functions
     */

    /**
     * Renderer Functions
     *================================================================*/

    render() {
        if (!this.render.autorun) {
            this.renderer.render();
        }
        this.triggerFrameEditEvents();
        this.triggerLayerEditEvents();
    }

    moveCanvas(x, y) {
        this.renderer.addOffset(x, y);
    }

    getZoom() {
        return this.renderer.getZoom();
    }

    setZoom(zoom) {
        if (zoom > 0.1 && zoom <= 5) {
            this.renderer.setZoom(zoom);
            this._triggerEvent("zoom", zoom);
        }
    }

    resetWindow() {
        this.renderer.reset();
    }

    /*================================================================*
     * Renderer Functions
     */

    /**
     * Pixel Functions
     *================================================================*/

    _setPixel(x, y, color) {
        x = x | 0
        y = y | 0
        if (this.selectionCanvas && this.isSelectedPixel(x, y)) {
            this.selectionCanvas.setPixel(x, y, color);
            this.addEditedLayer();
        } else if (this.hasNoSelection() || this.isSelectedPixel(x, y)) {
            this.activeLayer.setPixel(x, y, color, this.activeFrameIndex);
            this.addEditedLayer();
        }
    }

    getPixel(x, y) {
        x = x | 0
        y = y | 0
        if (this.selectionCanvas && this.isSelectedPixel(x, y)) {
            this.selectionCanvas.getPixel(x, y);
        }
        return this.activeLayer.getPixel(x, y, this.activeFrameIndex);
    }

    setPixel(x, y) {
        this._setPixel(x, y, this.primaryColor);
    }

    erasePixel(x, y) {
        this._setPixel(x, y, Colors.CLEAR);
    }

    getLayerPixel(x, y, frame = this.activeFrameIndex, layer = this.activeLayerIndex) {
        x = x | 0
        y = y | 0
        if (this.selectionCanvas && frame == this.getActiveFrameIndex() && layer == this.getActiveLayerIndex() && this.isSelectedPixel(x, y)) {
            this.selectionCanvas.getPixel(x, y, );
        }
        return this.layers[layer].getPixel(x, y, frame);
    }

    getFinalPixel(x, y, frame = this.activeFrameIndex) {
        var final = Colors.CLEAR;
        if (this.selectionCanvas && frame == this.getActiveFrameIndex()) {
            for (var i = this.layers.length - 1; i > -1; i--) {
                if (this.layers[i].isActive) {
                    if (i == this.getActiveLayerIndex() && this.isSelectedPixel(x, y)) {
                        final = final.over(this.selectionCanvas.getPixel(x, y));
                    } else {
                        final = final.over(this.layers[i].getPixel(x, y, frame));
                    }
                }
            }
        } else {
            for (var i = this.layers.length - 1; i > -1; i--) {
                if (this.layers[i].isActive) {
                    final = final.over(this.layers[i].getPixel(x, y, frame));
                }
            }
        }
        return final;
    }

    /*================================================================*
     * Pixel Functions
     */

    /**
     * Frame Functions
     *================================================================*/

    getActiveFrameIndex() {
        return this.activeFrameIndex;
    }

    getFrameCount() {
        return this.frameCount;
    }

    setFrame(frame) {
        this.mergeSelectionCanvas();
        frame = toValidPos(frame, this.frameCount);
        if (frame != -1) {
            this.endPreview();
            this.activeFrameIndex = frame;
            this._triggerEvent('frameSwitch', frame);
            this.setAllLayersEdited();
        }
    }

    previousFrame() {
        this.setFrame(this.activeFrameIndex - 1);
    }

    nextFrame() {
        this.setFrame(this.activeFrameIndex + 1);
    }

    addFrame(pos = this.activeFrameIndex + 1) {
        this.mergeSelectionCanvas();
        pos = toValidPos(pos, this.frameCount + 1);
        if (pos != -1) {
            this.frameCount++;
            for (var layer of this.layers) {
                layer.addFrame(pos);
            }
            this._triggerEvent("frameAdd", pos);
            this.setFrame(pos);
        }
    }

    addFrameCopy(frame = this.activeFrameIndex, pos = this.activeFrameIndex + 1) {
        this.mergeSelectionCanvas();
        frame = toValidPos(frame, this.frameCount);
        pos = toValidPos(pos, this.frameCount + 1);
        if (frame != -1 && pos != -1) {
            this.addFrame(pos)
            if (pos <= frame) {
                frame++;
            }
            for (var layer of this.layers) {
                if (layer instanceof PixelCanvasLayerMF) {
                    layer.setCanvas(layer.getFrameCopy(frame), pos);
                }
            }
            this.addEditedFrame(pos);
        }
    }

    removeFrame(pos = this.activeFrameIndex) {
        this.mergeSelectionCanvas();
        if (this.frameCount > 1) {
            pos = toValidPos(pos, this.frameCount);
            if (pos != -1) {
                this.frameCount--;
                for (var layer of this.layers) {
                    layer.removeFrame(pos);
                }
                this._triggerEvent("frameRemove", pos);
            }
            if (pos == this.frameCount) {
                pos--;
            }
            this.setFrame(pos);
        }
    }

    moveFrame(currentPos, newPos) {
        this.mergeSelectionCanvas();
        currentPos = toValidPos(currentPos, this.frameCount);
        newPos = toValidPos(newPos, this.frameCount);
        if ((currentPos != -1) && (currentPos != -1) && currentPos != newPos) {
            for (var layer of this.layers) {
                layer.moveFrame(currentPos, newPos);
            }
            this._triggerEvent("frameMove", currentPos, newPos);
            if (this.activeFrameIndex == currentPos) {
                this.setFrame(newPos);
            } else if ((currentPos < newPos) && (this.activeFrameIndex > currentPos) && (this.activeFrameIndex <= newPos)) {
                this.previousFrame();
            } else if ((currentPos > newPos) && (this.activeFrameIndex < currentPos) && (this.activeFrameIndex >= newPos)) {
                this.nextFrame();
            }
        }
    }

    /*================================================================*
     * Frame Functions
     */

    /**
     * Layer Funcions
     *================================================================*/

    getActiveLayerIndex() {
        return this.activeLayerIndex;
    }

    getLayerCount() {
        return this.layers.length;
    }

    setLayer(layer) {
        this.mergeSelectionCanvas();
        layer = toValidPos(layer, this.layers.length);
        if (layer != -1) {
            this.endPreview();
            this.activeLayerIndex = layer;
            this.activeLayer = this.layers[layer];
            this._triggerEvent('layerSwitch', layer);
        }
    }

    previousLayer() {
        this.setLayer(this.activeLayerIndex - 1);
    }

    nextLayer() {
        this.setLayer(this.activeLayerIndex + 1);
    }

    moveLayerDown() {
        this.moveLayer(this.activeLayerIndex, this.activeLayerIndex - 1);
    }

    moveLayerUp() {
        this.moveLayer(this.activeLayerIndex, this.activeLayerIndex + 1);
    }

    _addLayer(pos = this.activeLayerIndex + 1, layer) {
        this.mergeSelectionCanvas();
        pos = toValidPos(pos, this.getLayerCount() + 1);
        if (pos != -1) {
            layer.name = "Layer " + (pos + 1);
            this.layers.splice(pos, 0, layer);
            this._triggerEvent("layerAdd", pos);
            this.setLayer(pos);
        }
    }

    addLayerMF(pos) {
        this._addLayer(pos, new PixelCanvasLayerMF(this.frameCount));
    }

    addLayerSF(pos) {
        this._addLayer(pos, new PixelCanvasLayerSF());
    }

    removeLayer(pos = this.activeLayerIndex) {
        this.mergeSelectionCanvas();
        if (this.getLayerCount() > 1) {
            pos = toValidPos(pos, this.getLayerCount());
            if (pos != -1) {
                this.layers.splice(pos, 1);
                this._triggerEvent("layerRemove", pos);
            }
            if (pos == this.getLayerCount()) {
                pos--;
            }
            this.setAllFramesEdited()
            this.setLayer(pos);
        }
    }

    moveLayer(currentPos, newPos = this.activeLayerIndex) {
        this.mergeSelectionCanvas();
        currentPos = toValidPos(currentPos, this.getLayerCount());
        newPos = toValidPos(newPos, this.getLayerCount());
        if ((currentPos != -1) && (newPos != -1) && currentPos != newPos) {
            this.layers.splice(newPos, 0, this.layers.splice(currentPos, 1)[0]);
            this._triggerEvent("layerMove", currentPos, newPos);
            this.setAllFramesEdited()
            if (this.getActiveLayerIndex() == currentPos) {
                this.setLayer(newPos);
            } else if ((currentPos < newPos) && (this.getActiveLayerIndex() > currentPos) && (this.getActiveLayerIndex() <= newPos)) {
                this.previousLayer();
            } else if ((currentPos > newPos) && (this.getActiveLayerIndex() < currentPos) && (this.getActiveLayerIndex() >= newPos)) {
                this.nextLayer();
            }
        }
    }

    setLayerStatus(status = true, pos = this.activeLayerIndex) {
        pos = toValidPos(pos, this.getLayerCount() + 1);
        if (pos != -1) {
            this.layers[pos].setIsActive(status);
            this.setAllFramesEdited();
        }
    }

    getLayerStatus(pos = this.activeLayerIndex) {
        pos = toValidPos(pos, this.getLayerCount() + 1);
        if (pos != -1) {
            return this.layers[pos].isActive;
        }
    }

    getLayerName(pos) {
        pos = toValidPos(pos, this.getLayerCount());
        if (pos != -1) {
            return this.layers[pos].name;
        }
    }

    setLayerName(pos, name) {
        pos = toValidPos(pos, this.getLayerCount());
        if (pos != -1) {
            return this.layers[pos].name = name;
        }
    }

    mergeLayer(layer = this.getActiveLayerIndex()) {
        this.mergeSelectionCanvas();
        if (layer > 0 && layer < this.layers.length) {
            this.layers[layer - 1].merge(this.layers.splice(layer, 1)[0], this.getActiveFrameIndex());
        }
        this._triggerEvent("layerRemove", layer);
        this.setAllFramesEdited();
        this.addEditedLayer(layer - 1);
        this.setLayer(layer - 1);
    }

    switchLayerFrameType(layer = this.getActiveLayerIndex()) {
        this.mergeSelectionCanvas();
        layer = toValidPos(layer, this.getLayerCount());
        if (layer != -1) {
            if (this.layers[layer] instanceof PixelCanvasLayerMF) {
                this.layers[layer] = this.layers[layer].toSF(this.getActiveFrameIndex());
            } else if (this.layers[layer] instanceof PixelCanvasLayerSF) {
                this.layers[layer] = this.layers[layer].toMF(this.getActiveFrameIndex(), this.getFrameCount());
            }
            if (layer == this.getActiveLayerIndex()) {
                this.activeLayer = this.layers[layer];
            }
            this.setAllFramesEdited();
        }
    }

    getLayerFrameType(layer = this.getActiveLayerIndex()) {
        this.mergeSelectionCanvas();
        layer = toValidPos(layer, this.getLayerCount());
        if (layer != -1) {
            if (this.layers[layer] instanceof PixelCanvasLayerMF) {
                return "mf"
            } else if (this.layers[layer] instanceof PixelCanvasLayerSF) {
                return "sf"
            }
        }
    }

    clearLayer() {
        this.mergeSelectionCanvas();
        this.layers[this.getActiveLayerIndex()].clear(this.getActiveFrameIndex());
        this.addEditedLayer(this.getActiveLayerIndex());
    }

    /*================================================================*
     * Layer Funcions
     */

    /**
     * Tools Functions
     *================================================================*/

    addTool(tool) {
        this.toolManager.addTool(tool);
    }

    getActionToolsList() {
        this.toolManager.getActionToolsList();
    }

    getFunctionToolsList() {
        this.toolManager.getFunctionToolsList();
    }

    updateActionToolsList() {
        this._triggerEvent("actionToolsListUpdate", this.toolManager.getActiveActionToolsList());
        this.deactivateActiveTool();
    }

    updateFunctionToolsList() {
        this.functions = this.toolManager.getActiveFunctionToolsObject();
    }

    deactivateActiveTool() {
        if (this.activeTool) {
            this.activeTool = undefined;
            this._triggerEvent("deactivateActiveTool");
        }
    }

    setDefaultTool(tool) {
        this.defaultTool = tool;
    }

    setEmulateMissingEvents(value) {
        this.emulateMissingEvents = value;
    }

    /*================================================================*
     * Tools Functions
     */

    /**
     * Dialog Functions
     *================================================================*/

    showInfoDialog() {
        this.dialogGen.generateInfoDialog(...arguments);
    }

    showCustomDialog() {
        this.dialogGen.generateCustomDialog(...arguments);
    }

    showConfirmDialog() {
        this.dialogGen.generateConfirmDialog(...arguments);
    }

    showColorDialog() {
        this.dialogGen.generateColorDialog(...arguments);
    }

    showDialog() {
        this.dialogGen.generateDialog(...arguments);
    }

    /*================================================================*
     * Dialog Functions
     */

    /**
     * Select Functions
     *================================================================*/

    _selectPixel(x, y) {
        x = x | 0;
        y = y | 0;
        this.renderer.startAutorun();
        if (!this.selectedPixels[x]) {
            this.selectedPixels[x] = [y];
            this.selectedPixels.length++;
        } else {
            if (this.selectedPixels[x].indexOf(y) == -1) {
                this.selectedPixels[x].push(y);
            }
        }
    }

    _unselectPixel(x, y) {
        x = this.selectedPixels[x];
        if (x) {
            var aux = x.indexOf(y);
            if (aux != -1) {
                x.splice(aux, 1);
                if (x.length == 0) {
                    delete this.selectedPixels[x];
                    this.selectedPixels.length--;
                    if (this.hasNoSelection()) {
                        this.renderer.stopAutorun();
                    }
                }
            }
        }
    }

    _unselectAll() {
        this.selectedPixels = {};
        Object.defineProperty(this.selectedPixels, 'length', {
            value: 0,
            enumerable: false,
            writable: true,
            configurable: true
        });
        this.renderer.stopAutorun();
    }

    selectPixel(x, y) {
        this.mergeSelectionCanvas();
        this._selectPixel(x, y);
    }

    unselectPixel(x, y) {
        this.mergeSelectionCanvas();
        this._unselectPixel(x, y)
    }

    _isValid(x, y, d, color, isInsideSelection, list) {
        if ((!list[x] || list[x].indexOf(y) == -1) && this.isSelectedPixel(x, y) == isInsideSelection) {
            return (d.start.x <= x) && (d.end.x >= x) && (d.start.y <= y) && (d.end.y >= y) && color.equals(editor.getPixel(x, y));
        }
        return false;

    }

    _getSelectionArea(x, y) {
        var pixel = {
            x: x,
            y: y
        };
        var list = {};
        var dimensions = this.getActionAreaDimensions()
        var targetColor = this.getPixel(x, y);
        var queue = [pixel];
        var isInsideSelection = this.isSelectedPixel(x, y);
        while (queue.length) {
            var n = queue.shift();
            if (this._isValid(n.x, n.y, dimensions, targetColor, isInsideSelection, list)) {
                var w = n.x;
                var e = n.x;
                var valid = true;
                while (valid) {
                    if (this._isValid(w - 1, n.y, dimensions, targetColor, isInsideSelection, list)) {
                        w--;
                    } else {
                        valid = false;
                    }
                }
                valid = true;
                while (valid) {
                    if (this._isValid(e + 1, n.y, dimensions, targetColor, isInsideSelection, list)) {
                        e++;
                    } else {
                        valid = false;
                    }
                }
                for (var i = w; i <= e; i++) {
                    if (list[i]) {
                        list[i].push(n.y);
                    } else {
                        list[i] = [n.y];
                    }
                    if (this._isValid(i, n.y + 1, dimensions, targetColor, isInsideSelection, list)) {
                        queue.push({
                            x: i,
                            y: n.y + 1
                        });
                    }
                    if (this._isValid(i, n.y - 1, dimensions, targetColor, isInsideSelection, list)) {
                        queue.push({
                            x: i,
                            y: n.y - 1
                        });
                    }
                }
            }
        }
        return list;
    }

    selectArea(x, y) {
        this.mergeSelectionCanvas();
        if (!this.isSelectedPixel(x, y)) {
            var list = this._getSelectionArea(x, y);
            for (var x in list) {
                var line = list[x];
                for (var y of line) {
                    this._selectPixel(x, y);
                }
            }
        }
    }

    unselectArea(x, y) {
        this.mergeSelectionCanvas();
        if (this.isSelectedPixel(x, y)) {
            var list = this._getSelectionArea(x, y);
            for (var x in list) {
                var line = list[x];
                for (var y of line) {
                    this._unselectPixel(x, y);
                }
            }
        }
    }

    unselectAll() {
        this.mergeSelectionCanvas();
        this._unselectAll();
    }

    isSelectedPixel(x, y) {
        if (this.selectedPixels[x]) {
            return this.selectedPixels[x].indexOf(y) != -1;
        }
        return false;
    }

    hasSelection() {
        return this.selectedPixels.length > 0;
    }

    hasNoSelection() {
        return this.selectedPixels.length == 0;
    }

    _forceCreateSelectionCanvas() {
        console.log("Create: " + this.selectionCanvas);
        this.selectionCanvas = this.activeLayer.createSelectionCanvas(this.selectedPixels, this.getActiveFrameIndex());
    }

    createSelectionCanvas() {
        console.log("Create: " + this.selectionCanvas);
        if (!this.selectionCanvas && this.hasSelection()) {
            this.selectionCanvas = this.activeLayer.createSelectionCanvas(this.selectedPixels, this.getActiveFrameIndex());
        }
    }

    mergeSelectionCanvas() {
        console.log("Merge: " + this.selectionCanvas);
        if (this.selectionCanvas) {
            this.activeLayer.mergeSelectionCanvas(this.selectionCanvas, this.getActiveFrameIndex());
            this.selectionCanvas = undefined;
            this.addEditedLayer(this.getActiveLayerIndex());
        }
    }

    /*================================================================*
     * Select Functions
     */

    /**
     * Translate Functions
     *================================================================*/

    _translateSelection(x, y) {
        if (this.hasSelection() && (x != 0 || y != 0)) {
            var oldSelection = this.selectedPixels;
            this._unselectAll();
            for (var ix in oldSelection) {
                ix = Number.parseInt(ix)
                for (var iy of oldSelection[ix]) {
                    this._selectPixel(ix + x, Number.parseInt(iy) + y);
                }
            }
        }
    }


    translateLayer(x, y) {
        this.mergeSelectionCanvas();
        if (x != 0 || y != 0) {
            this.activeLayer.translate(x, y, this.getActiveFrameIndex());
            this._translateSelection(x, y);
            this.addEditedLayer();
        }
    }

    translateSelection(x, y) {
        this.mergeSelectionCanvas();
        this._translateSelection(x, y);
    }

    translateSelectionContent(x, y) {
        if (this.hasSelection() && (x != 0 || y != 0)) {
            this.createSelectionCanvas();
            this.selectionCanvas.translateSelectionContent(x, y, this.selectedPixels);
            this._translateSelection(x, y)
            this.addEditedLayer();
        }
    }

    /*================================================================*
     * Translate Functions
     */

    /**
     * Invert Functions
     *================================================================*/

    invertPixel(x, y) {
        this.setPixel(this.getPixel(x, y).inverse());
        this.addEditedLayer();
    }

    invertLayer() {
        this.mergeSelectionCanvas();
        this.activeLayer.invert(this.getActionAreaDimensions(), this.getActiveFrameIndex());
        this.addEditedLayer();
    }

    invertSelectionContent() {
        if (this.hasSelection()) {
            if (this.selectionCanvas) {
                this.selectionCanvas.invertSelectionContent(this.selectedPixels);
            } else {
                this.activeLayer.invertSelectionContent(this.selectedPixels, this.getActiveFrameIndex());
            }
            this.addEditedLayer();
        }
    }


    /*================================================================*
     * Invert Functions
     */

    /**
     * Mirror Functions
     *================================================================*/

    _mirrorSelection(isVertical) {
        if (this.hasSelection()) {
            var d = getSelectionLimits(this.selectedPixels);
            var oldSelection = this.selectedPixels;
            this._unselectAll();
            if (isVertical) {
                var middle = (d.end.y + d.start.y) / 2;
                for (var x in oldSelection) {
                    for (var y of oldSelection[x]) {
                        this._selectPixel(x, middle + (middle - y));
                    }
                }
            } else {
                var middle = (d.end.x + d.start.x) / 2;
                for (var x in oldSelection) {
                    var nx = middle + (middle - x);
                    for (var y of oldSelection[x]) {
                        this._selectPixel(nx, y);
                    }
                }
            }
        }
    }

    mirrorLayer(isVertical) {
        this.mergeSelectionCanvas();
        this.activeLayer.mirror(this.getDimensions(), isVertical, this.getActiveFrameIndex());
        this._mirrorSelection();
        this.addEditedLayer();
    }

    mirrorSelection(isVertical) {
        this.mergeSelectionCanvas();
        this._mirrorSelection(isVertical);
    }

    mirrorSelectionContent(isVertical) {
        if (this.hasSelection()) {
            this.createSelectionCanvas();
            this.selectionCanvas.mirrorSelectionContent(this.selectedPixels, isVertical, this.getActiveFrameIndex());
            this._mirrorSelection()
            this.addEditedLayer();
        }
    }

    /*================================================================*
     * Mirror Functions
     */

    /**
     * Rotate Functions
     *================================================================*/
    _rotateSelection(dimensions, angle) {
        if (this.hasSelection() && angle != 0) {
            var oldSelection = this.selectedPixels;
            this._unselectAll();
            dimensions = {
                start: dimensions.start,
                end: {
                    x: dimensions.end.x,
                    y: dimensions.end.y
                }
            }
            var xSize = (dimensions.end.x - dimensions.start.x + 1);
            var ySize = (dimensions.end.y - dimensions.start.y + 1);
            var xEven = xSize % 2 == 0;
            var yEven = ySize % 2 == 0;
            var offsetX = 0;
            var offsetY = 0;
            var center;
            var aux;
            if (xEven != yEven) { //width and height are even and odd
                if (xSize > ySize) {
                    switch (angle) {
                        case 180:
                            offsetX = 1;
                            break;
                        case 270:
                            offsetY = 1;
                            break;
                    }
                    dimensions.end.x--;
                    xEven = !xEven;
                } else {
                    switch (angle) {
                        case 90:
                            offsetX = 1;
                            break;
                        case 180:
                            offsetY = 1;
                            break;
                    }
                    dimensions.end.y--;
                    yEven = !yEven;
                }
            }
            if (xEven) { //width and height are even
                center = centerEven(dimensions.start.x, dimensions.start.y, dimensions.end.x, dimensions.end.y);
                for (var x in oldSelection) {
                    x = Number.parseInt(x);
                    for (var y of oldSelection[x]) {
                        aux = rotateEven(x, y, center.x, center.y, angle);
                        this._selectPixel(aux.x + offsetX, aux.y + offsetY);
                    }
                }
            } else { //width and height are odd
                center = centerOdd(dimensions.start.x, dimensions.start.y, dimensions.end.x, dimensions.end.y);
                for (var x in oldSelection) {
                    x = Number.parseInt(x);
                    for (var y of oldSelection[x]) {
                        aux = rotateOdd(x, y, center.x, center.y, angle);
                        this._selectPixel(aux.x + offsetX, aux.y + offsetY);
                    }
                }
            }
        }
    }

    rotateLayer(angle) {
        angle = processAngle(angle);
        var d = this.getDimensions();
        d = {
            start: {
                x: 0,
                y: 0
            },
            end: {
                x: d.width - 1,
                y: d.height - 1
            }
        };
        this.mergeSelectionCanvas();
        this.activeLayer.rotate(d, angle, this.getActiveFrameIndex());
        this._rotateSelection(d, angle);
        this.addEditedLayer();
    }

    rotateSelection(angle) {
        if (this.hasSelection()) {
            angle = processAngle(angle);
            this.mergeSelectionCanvas();
            this._rotateSelection(getSelectionLimits(this.selectedPixels), angle);
        }
    }

    rotateSelectionContent(angle) {
        if (this.hasSelection()) {
            this.createSelectionCanvas();
            angle = processAngle(angle);
            var d = getSelectionLimits(this.selectedPixels);
            this.selectionCanvas.rotateSelectionContent(this.selectedPixels, d, angle);
            this._rotateSelection(d, angle)
            this.addEditedLayer();
        }
    }
    /*================================================================*
     * Rotate Functions
     */

    /**
     * Scale Functions
     *================================================================*/
    _selectScaledUpPixel(x, y, ratio, cx, cy) {
        var endy = y + ratio;
        for (var endx = x + ratio; x < endx; x++) {
            for (var iy = y; iy < endy; iy++) {
                this._selectPixel(x + cx, iy + cy);
            }
        }
    }
    _scaleUpSelection(ratio, cx, cy) {
        ratio = toValidPos(ratio);
        if (ratio > 1 && this.hasSelection()) {
            var oldSelection = this.selectedPixels;
            this._unselectAll();
            for (var x in oldSelection) {
                x = Number.parseInt(x);
                for (var y of oldSelection[x]) {
                    this._selectScaledUpPixel((x - cx) * 2, (y - cy) * 2, ratio, cx, cy);
                }
            }
        }
    }

    scaleUpLayer(ratio) {
        ratio = toValidPos(ratio);
        if (ratio > 1) {
            this.mergeSelectionCanvas();
            this.activeLayer.scaleUp(ratio, this.getActiveFrameIndex());
            this._scaleUpSelection(ratio, 0, 0);
            this.addEditedLayer();
        }
    }

    scaleUpSelectionContent(ratio) {
        ratio = toValidPos(ratio);
        if (ratio > 1 && this.hasSelection()) {
            this.createSelectionCanvas();
            var center = getSelectionLimits(this.selectedPixels).start;
            this.selectionCanvas.scaleUpSelectionContent(this.selectedPixels, ratio, center.x, center.y);
            this._scaleUpSelection(ratio, center.x, center.y);
            this.addEditedLayer();
        }
    }

    _scaleDownSelection(ratio, cx, cy) {
        ratio = toValidPos(ratio);
        if (ratio > 1 && this.hasSelection()) {
            var oldSelection = this.selectedPixels;
            this._unselectAll();
            for (var x in oldSelection) {
                for (var y of oldSelection[x]) {
                    this._selectPixel((x - cx) / ratio + cx, (y - cy) / ratio + cy);
                }
            }
        }
    }

    scaleDownLayer(ratio) {
        ratio = toValidPos(ratio);
        if (ratio > 1) {
            this.mergeSelectionCanvas();
            this.activeLayer.scaleDown(ratio, this.getActiveFrameIndex());
            this._scaleDownSelection(ratio, 0, 0);
            this.addEditedLayer();
        }
    }

    scaleDownSelectionContent(ratio) {
        ratio = toValidPos(ratio);
        if (ratio > 1 && this.hasSelection()) {
            this.createSelectionCanvas();
            var center = getSelectionLimits(this.selectedPixels).start;
            this.selectionCanvas.scaleDownSelectionContent(this.selectedPixels, ratio, center.x, center.y);
            this._scaleDownSelection(ratio, center.x, center.y);
            this.addEditedLayer();
        }
    }

    /*================================================================*
     * Scale Functions
     */


    /**
     * Copy/Paste Functions
     *================================================================*/

    copySelection() {
        if (this.hasSelection()) {
            this.mergeSelectionCanvas();
            this.clipboard = {};
            for (var x in this.selectedPixels) {
                var line = this.selectedPixels[x];
                this.clipboard[x] = {}
                for (var y of line) {
                    this.clipboard[x][y] = this.getPixel(x, y);
                }
            }
        }
    }

    cutSelection() {
        if (this.hasSelection()) {
            this.mergeSelectionCanvas();
            this.clipboard = {};
            for (var x in this.selectedPixels) {
                var line = this.selectedPixels[x];
                this.clipboard[x] = {}
                for (var y of line) {
                    this.clipboard[x][y] = this.getPixel(x, y);
                    this.erasePixel(x, y);
                }
            }
        }
    }

    pasteSelection() {
        if (this.clipboard) {
            this.unselectAll();
            this._forceCreateSelectionCanvas();
            for (var x in this.clipboard) {
                var line = this.clipboard[x];
                for (var y in line) {
                    this._selectPixel(x, Number.parseInt(y));
                    this._setPixel(x, y, line[y]);
                }
            }
        }
    }

    /*================================================================*
     * Copy/Paste Functions
     */

    /**
     * Edit Event Functions
     *================================================================*/

    addEditedFrame(frame) {
        if (!this.allFramesEdited && this.editedFrames.indexOf(frame) == -1) {
            this.editedFrames.push(frame);
        }
    }

    setAllFramesEdited() {
        this.allFramesEdited = true;
    }

    getEditedFrames() {
        if (this.allFramesEdited) {
            var result = [];
            for (var i = 0; i < this.frameCount; i++) {
                result.push(i);
            }
            return result;
        }
        return this.editedFrames;
    }

    clearEditedFrameList() {
        this.editedFrames = [];
        this.allFramesEdited = false;
    }

    triggerFrameEditEvents() {
        var list = this.getEditedFrames();
        this.clearEditedFrameList();
        for (var frame of list) {
            this._triggerEvent('editedFrame', frame);
        }
    }

    addEditedLayer(layer = this.getActiveLayerIndex()) {
        if (!this.allLayersEdited && this.editedLayers.indexOf(layer) == -1) {
            if (this.layers[layer] instanceof PixelCanvasLayerMF) {
                this.addEditedFrame(this.getActiveFrameIndex());
            } else if (this.layers[layer] instanceof PixelCanvasLayerSF) {
                this.setAllFramesEdited();
            }
            this.editedLayers.push(layer);
        }
    }

    setAllLayersEdited() {
        this.allLayersEdited = true;
    }

    getEditedLayers() {
        if (this.allLayersEdited) {
            var result = [];
            for (var i = 0; i < this.getLayerCount(); i++) {
                result.push(i);
            }
            return result;
        }
        return this.editedLayers;
    }

    clearEditedLayerList() {
        this.editedLayers = [];
        this.allLayersEdited = false;
    }

    triggerLayerEditEvents() {
        var list = this.getEditedLayers();
        this.clearEditedLayerList();
        for (var Layer of list) {
            this._triggerEvent('editedLayer', Layer);
        }
    }

    /*================================================================*
     * Edit Event Functions
     */

    /**
     * Export Functions
     *================================================================*/

    export (pixelSize, frame = this.activeFrameIndex) {
        return this.exporter.export(pixelSize, frame);
    }

    export2(pixelSize, frame = this.activeFrameIndex) {
        return this.exporter.export2(pixelSize, frame);
    }

    exportLayer(pixelSize, frame = this.activeFrameIndex, layer = this.activeLayerIndex) {
        return this.exporter.exportLayer(pixelSize, frame, layer);
    }

    exportLayer2(pixelSize, frame = this.activeFrameIndex, layer = this.activeLayerIndex) {
        return this.exporter.exportLayer2(pixelSize, frame, layer);
    }

    /*================================================================*
     * Export Functions
     */

    /**
     * Load/Save Functions
     *================================================================*/

    toJSON() {
        var colorList = this.normalizeColorPalette().toList();
        var layerList = [];
        for (var layer of this.layers) {
            layerList.push(layer.toJSON(colorList));
        }
        for (var i = 0; i < colorList.length; i++) {
            colorList[i] = colorList[i].toSignature();
        }
        return {
            activeFrame: this.getActiveFrameIndex(),
            activeLayer: this.getActiveLayerIndex(),
            frameCount: this.getFrameCount(),
            colorList: colorList,
            layers: layerList
        };
    }

    saveLocal() {
        var json = this.toJSON();
        localStorage.setItem("activeFrame", json.activeFrame);
        localStorage.setItem("activeLayer", json.activeLayer);
        localStorage.setItem("frameCount", json.frameCount);
        localStorage.setItem("colorList", JSON.stringify(json.colorList));
        localStorage.setItem("layers", JSON.stringify(json.layers));
    }

    loadNewProject() {
        this.layers = [new PixelCanvasLayerMF(1, "Layer 1")]
        this.activeFrameIndex = 0;
        this.activeLayerIndex = 0;
        this.frameCount = 1
        this.activeLayer = this.layers[0];
        this.unselectAll();
        this.setZoom(1);
        this._triggerEvent("loadProject");
    }

    loadJSON(json) {
        if (typeof json.activeFrame != "number" || Number.isNaN(json.activeFrame) || json.activeFrame < 0) {
            throw "Active frame is not a valid number";
        }
        if (typeof json.activeLayer != "number" || Number.isNaN(json.activeLayer) || json.activeLayer < 0) {
            throw "Active layer is not a valid number";
        }
        if (typeof json.frameCount != "number" || Number.isNaN(json.frameCount) || json.frameCount < 1) {
            throw "Frame count is not a valid number";
        }
        if (!(json.colorList instanceof Array)) {
            throw "Color list is not an Array";
        }
        if (!(json.layers instanceof Array)) {
            throw "Layer List is not an Array";
        }
        var layers = []
        var colorList = json.colorList
        for (var i = 0; i < colorList.length; i++) {
            try {
                colorList[i] = Color.fromSignature(colorList[i]);
            } catch (err) {
                throw "Color " + i + ": " + err;
            }
        }
        for (var layer of json.layers) {
            if (layer.type == "mf") {
                layers.push(PixelCanvasLayerMF.fromJSON(layer, colorList, json.frameCount));
            } else if (layer.type == "sf") {
                layers.push(PixelCanvasLayerSF.fromJSON(layer, colorList));
            }
        }
        this.layers = layers
        this.activeFrameIndex = json.activeFrame;
        this.activeLayerIndex = json.activeLayer;
        this.frameCount = json.frameCount;
        this.activeLayer = layers[this.activeLayerIndex];
        this.setZoom(1);
        this._triggerEvent("loadProject");
    }

    loadLocal() {
        this.loadJSON({
            activeFrame: Number.parseInt(localStorage.getItem("activeFrame")),
            activeLayer: Number.parseInt(localStorage.getItem("activeLayer")),
            frameCount: Number.parseInt(localStorage.getItem("frameCount")),
            colorList: JSON.parse(localStorage.getItem("colorList")),
            layers: JSON.parse(localStorage.getItem("layers"))
        });
        this.render()
    }

    /*================================================================*
     * Load/Save Functions
     */
}