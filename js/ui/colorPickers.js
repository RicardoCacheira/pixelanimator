function setupColorPickers(editor) {

    //"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABq0lEQVR4Xu3aYY7CIBRFYdxjl9k9zmRSZ5KJD3PxUNvG428e4OcFWuKt+UECN1RtcRMQhkBAAaEALDeBAkIBWG4CBYQCsNwECggFYLkJFBAKwHITKCAUgOVXSOBX8R1PM+/TTORJEASEq0RAAaEALDeBOwBWXR6ynx8y6CBolUABBxAFHMCqmgooIBRoraUp4iP972H3PX73Ae7fR0AYDQEFrAVcwjAZPcDZS478UOlcHsZYliWtjRjXdX0YQ8CIbmsk4ABW1VRAAZ8KuAd2eF6+D/QQ2UQFbA39lVhAAeHpJeCbAKtNv3r2SqfzcYeIgP1oRO/CAgr4JzD1FiM9REzgQALTDd5DZEN9+T1VQAHTp659bkpM4IcmcErsfjtJD4ypg3au4GePET3g0kEFhIICCtgVcAnDcLwFsJpjuqzJYwy0icoFjJgG3lFhf3G5CYyp6oYCCljfkkAXVD77PhBNJiw+7BBJT2ZP4fCX/GlmAgewTCDEEnAHwCt2eapDRMArCsA5m0ABoQAsN4ECQgFYbgIFhAKw3AQKCAVguQkUEArAchMoIBSA5d92yeFRDAswJwAAAABJRU5ErkJggg==",

    $$('.switch-button').event('click', function () {
        editor.switchColors();
    })

    var colorPreviews = $$('.color-preview');

    function setPrimaryColor(color) {
        editor.setPrimaryColor(color);
    }

    function setSecondaryColor(color) {
        editor.setSecondaryColor(color);
    }

    var primary = editor.getPrimaryColor();
    var secondary = editor.getSecondaryColor();

    for (var preview of colorPreviews) {
        if (preview.attributes.color.value == 'primary') {
            preview.css('background-color', primary);
        } else if (preview.attributes.color.value == 'secondary') {
            preview.css('background-color', secondary);
        }
        preview.addEventListener('click', function () {

            if (this.attributes.color.value == 'primary') {
                editor.showColorDialog(editor.getPrimaryColor(), setPrimaryColor, 'Primary Color');

            } else if (this.attributes.color.value == 'secondary') {
                editor.showColorDialog(editor.getSecondaryColor(), setSecondaryColor, 'Secondary Color')

            }
        });
    }

    /*
        TODO check why it doesnt work
    
        event(colorPickers, 'click', function () {
            if (picker.attributes.color.value == 'primary') {
                editor.showColorDialog(editor.getPrimaryColor(), setPrimaryColor, 'Primary Color');

            } else if (picker.attributes.color.value == 'secondary') {
                editor.showColorDialog(editor.getSecondaryColor(), setSecondaryColor, 'Secondary Color')

            }

        });*/

    var updated = false

    var colorPicker = $$('.color-picker');

    var color = primary;

    var hsv = color.toHSV();

    var red = DialogGen.generateRangeTr('R', 0, 255, 1, color.red);
    var green = DialogGen.generateRangeTr('G', 0, 255, 1, color.green);
    var blue = DialogGen.generateRangeTr('B', 0, 255, 1, color.blue);
    var hue = DialogGen.generateRangeTr('H', 0, 360, 1, hsv.hue);
    var saturation = DialogGen.generateRangeTr('S', 0, 100, 1, Math.round(hsv.saturation * 100));
    var value = DialogGen.generateRangeTr('V', 0, 100, 1, Math.round(hsv.value * 100));
    var alpha = DialogGen.generateRangeTr('A', 0, 1, 0.01, color.alpha, true);
    DialogGen.updateRangeBackgound(red, green, blue, hue, saturation, value, alpha);
    var updatePreviewRGB = function () {
        var color = new Color(red.child(1, 0).value, green.child(1, 0).value, blue.child(1, 0).value, alpha.child(1, 0).value);
        var hsv = color.toHSV();
        hsv.saturation = Math.round(hsv.saturation * 100);
        hsv.value = Math.round(hsv.value * 100);
        hue.child(1, 0).value = hsv.hue;
        hue.child(2, 0).value = hsv.hue;
        saturation.child(1, 0).value = hsv.saturation;
        saturation.child(2, 0).value = hsv.saturation;
        value.child(1, 0).value = hsv.value;
        value.child(2, 0).value = hsv.value;
        DialogGen.updateRangeBackgound(red, green, blue, hue, saturation, value, alpha);
        updated = true;
        editor.setPrimaryColor(color);
    }

    var updatePreviewHSV = function () {
        var color = Color.hsv(hue.child(1, 0).value, saturation.child(1, 0).value / 100, value.child(1, 0).value / 100, alpha.child(1, 0).value);
        red.child(1, 0).value = color.red;
        red.child(2, 0).value = color.red;
        green.child(1, 0).value = color.green;
        green.child(2, 0).value = color.green;
        blue.child(1, 0).value = color.blue;
        blue.child(2, 0).value = color.blue;
        DialogGen.updateRangeBackgound(red, green, blue, hue, saturation, value, alpha);
        updated = true;
        editor.setPrimaryColor(color);
    }

    var updatePreviewAlpha = function () {
        var color = new Color(red.child(1, 0).value, green.child(1, 0).value, blue.child(1, 0).value, alpha.child(1, 0).value);
        DialogGen.updateRangeBackgound(red, green, blue, hue, saturation, value, alpha);
        updated = true;
        editor.setPrimaryColor(color);
    }

    red.update = updatePreviewRGB;
    green.update = updatePreviewRGB;
    blue.update = updatePreviewRGB;
    hue.update = updatePreviewHSV;
    saturation.update = updatePreviewHSV;
    value.update = updatePreviewHSV;
    alpha.update = updatePreviewAlpha;

    colorPicker.append(table(red, green, blue, tr(td()), hue, saturation, value, tr(td()), alpha));

    editor.addEventListener("colorChange", function (color) {
        for (var preview of colorPreviews) {
            if (preview.attributes.color.value == color.type) {
                preview.css('background-color', color.color);
            }
        }

        if (updated) {
            updated = false;
        } else {
            if (color.type == 'primary') {
                color = color.color;
                red.child(1, 0).value = color.red;
                red.child(2, 0).value = color.red;
                green.child(1, 0).value = color.green;
                green.child(2, 0).value = color.green;
                blue.child(1, 0).value = color.blue;
                blue.child(2, 0).value = color.blue;
                var hsv = color.toHSV();
                hsv.saturation = Math.round(hsv.saturation * 100);
                hsv.value = Math.round(hsv.value * 100);
                hue.child(1, 0).value = hsv.hue;
                hue.child(2, 0).value = hsv.hue;
                saturation.child(1, 0).value = hsv.saturation;
                saturation.child(2, 0).value = hsv.saturation;
                value.child(1, 0).value = hsv.value;
                value.child(2, 0).value = hsv.value;
                alpha.child(1, 0).value = color.alpha;
                alpha.child(2, 0).value = color.alpha;
                DialogGen.updateRangeBackgound(red, green, blue, hue, saturation, value, alpha);
            }
        }
    });

}