function setupLayerList(editor) {
    const BUTTON_SIZE = "30px";

    var active;
    var liList;
    var layerList = $$('.layer-list');

    function addLayerMF() {
        editor.addLayerMF();
        editor.render();
    }

    function addLayerSF() {
        editor.addLayerSF();
        editor.render();
    }


    function removeLayer() {
        editor.removeLayer();
        editor.render();
    }

    function moveLayerUp() {
        editor.moveLayerUp();
        editor.render()
    }

    function moveLayerDown() {
        editor.moveLayerDown();
        editor.render()
    }

    function mergeLayer() {
        editor.mergeLayer();
        editor.render()
    }

    function allowDrop(evt) {
        evt.preventDefault();
    }

    function onPageDrag(evt) {
        evt.dataTransfer.setData("text", evt.target.parentElement.position);
    }

    function onPageDrop(evt) {
        evt.preventDefault();
        var data = evt.dataTransfer.getData("text");
        editor.moveLayer(data, this.position);
        editor.render();
    }

    function onLayerClick() {
        editor.setLayer(this.parentElement.position);
        editor.render();
    }

    function onTypeButtonClick() {

    }

    function onVisibleButtonClick() {
        var liElem = this.parent().parent();
        editor.setLayerStatus(!this.isVisible, liElem.position);
        editor.render();
        liElem.updateButtons();
    }

    function onFramesButtonClick() {
        var liElem = this.parent().parent();
        editor.switchLayerFrameType(liElem.position);
        editor.render();
        liElem.updateButtons();
    }

    function updateButtons() {
        //isVisible
        var visibleButton = this.child(1, 2);
        visibleButton.isVisible = editor.getLayerStatus(this.position);
        if (visibleButton.isVisible) {
            visibleButton.src = "./images/visible.png";
        } else {
            visibleButton.src = "./images/invisible.png"
        }
        //framesType
        var framesButton = this.child(1, 3);
        if (editor.getLayerFrameType(this.position) == "mf") {
            framesButton.src = "./images/multi.png";
        } else {
            framesButton.src = "./images/single.png"
        }
    }

    function updateLayerName() {
        editor.setLayerName(this.parent(1).position, this.value);
    }

    function createLayerLi(pos) {
        var imgElem = img(editor.exportLayer2(10, editor.getActiveFrameIndex(), pos)).attr('draggable', true).css("max-height", "80px", "max-width", "80px").addClass('layer-thumb');
        imgElem.event('dragstart', onPageDrag);
        imgElem.event('click', onLayerClick);
        var nameDiv = input("text").attr("value", editor.getLayerName(pos)).addClass('layer-name').event("change", updateLayerName);
        var typeButton = img("./images/layer.png").css("height", BUTTON_SIZE, "width", BUTTON_SIZE);
        var visibleButton = img().css("height", BUTTON_SIZE, "width", BUTTON_SIZE);
        var framesButton = img().css("height", BUTTON_SIZE, "width", BUTTON_SIZE);
        visibleButton.event("click", onVisibleButtonClick);
        framesButton.event("click", onFramesButtonClick);
        var liElem = li(imgElem, div(nameDiv, typeButton, visibleButton, framesButton).css("display", "inline-block"));
        liElem.event('dragover', allowDrop);
        liElem.event('drop', onPageDrop);
        liElem.position = pos;
        liElem.updateButtons = updateButtons;
        liElem.updateButtons();
        return liElem;
    }

    function onLayerAdd(pos) {
        var newLi = createLayerLi(pos);
        if (pos) {
            liList.splice(pos, 0, newLi);
            layerList.insertBefore(newLi, liList[pos - 1]);
        } else {
            layerList.appendChild(newLi, liList[0]);
            liList.unshift(newLi);
        }
        for (var i = pos + 1; i < liList.length; i++) {
            var element = liList[i];
            element.position = i;
        }
    }

    function onLayerRemove(pos) {
        liList.splice(pos, 1)[0].remove();
        for (var i = pos; i < liList.length; i++) {
            var element = liList[i];
            element.position = i;
        }
    }

    function onLayerMove(currentPos, newPos) {
        liList.splice(newPos, 0, liList.splice(currentPos, 1)[0]);
        if (newPos < currentPos) {
            var aux = newPos;
            newPos = currentPos;
            currentPos = aux;
        }
        for (var i = currentPos; i <= newPos; i++) {
            liList[i].position = i;
        }
        layerList.clear();
        layerList.append(liList.reverse());
        liList.reverse();
    }

    function onEditedLayer(pos) {
        liList[pos].child(0).src = editor.exportLayer2(10, editor.getActiveFrameIndex(), pos);
    }

    function onLayerSwitch(layer) {
        active.removeClass('active-layer');
        active = liList[layer];
        active.addClass('active-layer');
    }

    function loadList() {
        layerList.clear();
        liList = [];
        for (var i = 0; i < editor.getLayerCount(); i++) {
            liList.push(createLayerLi(i));
        }
        layerList.append(liList.reverse());
        liList.reverse();
        active = liList[editor.getActiveLayerIndex()];
        active.addClass('active-layer');
    }

    editor.addEventListener("layerAdd", onLayerAdd);
    editor.addEventListener("layerRemove", onLayerRemove);
    editor.addEventListener("layerMove", onLayerMove);
    editor.addEventListener("editedLayer", onEditedLayer);
    editor.addEventListener("layerSwitch", onLayerSwitch);
    editor.addEventListener("loadProject", loadList);

    $$('.add-layer-mf').event('click', addLayerMF);
    $$('.add-layer-sf').event('click', addLayerSF);
    $$('.remove-layer').event('click', removeLayer);
    $$('.layer-up').event('click', moveLayerUp);
    $$('.layer-down').event('click', moveLayerDown);
    $$('.merge-layer').event('click', mergeLayer);
}