function setupInfobar(editor) {
    var infobar = $$(".infobar");

    var simpleMode = true;

    var showDiff = false

    var startPos = undefined;

    function onMouseMove(pos) {
        if (simpleMode) {
            infobar.clear().append("Pos: " + pos.x + ", " + pos.y);
        } else {
            var str = "Start: " + startPos.x + ", " + startPos.y + ", End: " + pos.x + ", " + pos.y;
            if (showDiff) {
                str += ", Move: " + (pos.x - startPos.x) + ", " + (pos.y - startPos.y);
            } else {
                str += ", Lenght: " + (Math.abs(pos.x - startPos.x) + 1) + ", " + (Math.abs(pos.y - startPos.y) + 1);
            }
            infobar.clear().append(str);
        }
    }

    editor.addEventListener("mouseDown", function (pos, showStartPos, showPosDiff) {
        simpleMode = !showStartPos;
        startPos = pos;
        showDiff = showPosDiff;
        onMouseMove(pos);
    });

    editor.addEventListener("mouseUp", function (pos, showStartPos) {
        simpleMode = true;
        infobar.clear().append("Pos: " + pos.x + ", " + pos.y);
        onMouseMove(pos);
    });

    editor.addEventListener("mouseMove", onMouseMove);
}