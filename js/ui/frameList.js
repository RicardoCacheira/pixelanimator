function setupFrameList(editor) {
    var frameList = $$('.frame-list');
    var active;
    var liList;

    function addFrame() {
        editor.addFrame();
        editor.render()
    }

    function addFrameCopy() {
        editor.addFrameCopy();
        editor.render()
    }

    function removeFrame() {
        editor.removeFrame();
        editor.render()
    }

    function previousFrame() {
        editor.previousFrame();
        editor.render()
    }

    function nextFrame() {
        editor.nextFrame();
        editor.render()
    }

    function allowDrop(evt) {
        evt.preventDefault();
    }

    function onPageDrag(evt) {
        evt.dataTransfer.setData("text", evt.target.parentElement.position);
    }

    function onPageDrop(evt) {
        evt.preventDefault();
        var data = evt.dataTransfer.getData("text");
        editor.moveFrame(data, this.position);
    }

    function onFrameClick() {
        editor.setFrame(this.parentElement.position);
        editor.render();
    }

    function createFrameLi(pos) {
        var imgElem = img(editor.export2(10, pos)).attr('height', 80, 'draggable', true).addClass('frame-thumb');
        imgElem.event('dragstart', onPageDrag);
        imgElem.event('click', onFrameClick);

        var indexDiv = div(pos + 1).addClass('frame-num');

        var liElem = li(imgElem, indexDiv);
        liElem.event('dragover', allowDrop);
        liElem.event('drop', onPageDrop);
        liElem.position = pos;

        return liElem;
    }

    function onFrameAdd(pos) {
        var newLi = createFrameLi(pos);
        if (pos) {
            if (pos == liList) {
                frameList.appendChild(newLi);
            } else {
                frameList.insertBefore(newLi, liList[pos]);
            }
            liList.splice(pos, 0, newLi);

        } else {
            frameList.insertBefore(newLi, liList[0]);
            liList.unshift(newLi);
        }
        for (var i = pos + 1; i < liList.length; i++) {
            var element = liList[i];
            element.position = i;
            element.child(1).clear().append(i + 1);
        }
    }

    function onFrameRemove(pos) {
        liList.splice(pos, 1)[0].remove();
        for (var i = pos; i < liList.length; i++) {
            var element = liList[i];
            element.position = i;
            element.child(1).clear().append(i + 1);
        }
    }

    function onFrameMove(currentPos, newPos) {
        var newPosElem = liList[newPos];
        var current = liList.splice(currentPos, 1)[0];
        current.remove();
        if (newPos < currentPos) {
            frameList.insertBefore(current, newPosElem);

        } else {
            frameList.insertBefore(current, liList[newPos]);
        }
        liList.splice(newPos, 0, current);
        if (newPos < currentPos) {
            var aux = newPos;
            newPos = currentPos;
            currentPos = aux;
        }
        for (var i = currentPos; i <= newPos; i++) {
            var element = liList[i];
            element.position = i;
            element.child(1).clear().append(i + 1);
        }
    }

    function onEditedFrame(pos) {
        liList[pos].child(0).src = editor.export2(10, pos);
    }

    function onFrameSwitch(frame) {
        active.removeClass('active-frame');
        active = liList[frame];
        active.addClass('active-frame');
    }

    function loadList() {
        frameList.clear();
        liList = [];
        for (var i = 0; i < editor.frameCount; i++) {
            liList.push(createFrameLi(i));
        }
        frameList.append(liList);
        active = liList[editor.getActiveFrameIndex()];
        active.addClass('active-frame');
    }

    editor.addEventListener("frameAdd", onFrameAdd);
    editor.addEventListener("frameRemove", onFrameRemove);
    editor.addEventListener("frameMove", onFrameMove);
    editor.addEventListener("editedFrame", onEditedFrame);
    editor.addEventListener("frameSwitch", onFrameSwitch);
    editor.addEventListener("loadProject", loadList);

    $$('.add-frame').event('click', addFrame);
    $$('.add-frame-copy').event('click', addFrameCopy);
    $$('.remove-frame').event('click', removeFrame);
    $$('.prev-frame').event('click', previousFrame);
    $$('.next-frame').event('click', nextFrame);
}