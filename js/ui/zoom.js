var minZoom = 20;
var maxZoom = 500;

function setupZoom(editor) {
    var zoomV = $$('.zoom-value');
    var zoomR = $$('.zoom-range');

    zoomV.attr('value', '100%');

    zoomR.attr('min', minZoom, 'max', maxZoom, 'value', 100).event('input', function () {
        editor.setZoom(this.value / 100);
    })

    editor.addEventListener("zoom", function (zoom) {
        zoomV.attr('value', ((zoom * 100) | 0) + '%');
        zoomR.value = ((zoom * 100) | 0);
    });
}