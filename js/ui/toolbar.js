function setupToolbar(editor) {
    var toolbar = $$('.toolbar').child(0);
    editor.addEventListener("actionToolsListUpdate", function (tools) {
        for (var tool of tools) {
            toolbar.append(tool.liElem)
        }
    });
    editor.addEventListener("deactivateActiveTool", function () {
        toolbar.select(".active-tool").removeClass('active-tool')
    });

}