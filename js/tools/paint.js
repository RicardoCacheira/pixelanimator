//Paint Tool
(function (editor) {

    function start(pixel) {
        editor.setPixel(pixel.x, pixel.y);
    }

    function move(pixel) {
        editor.setPixel(pixel.x, pixel.y);
    }

    function end() {}

    editor.addTool({
        name: "Paint",
        icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABwElEQVR4Xu2aS47DIBAFyf0PPVlEymKCpdeU8QfXrPuBKQr3GOXV/EMEXihtuAkQSiBAAUICML66gX8dPruuedfB4GbOiAsQUhWgACEBGNfAAsAUVloXTb1SE0nBpHUCbK37pSXADTVSMGmdBmrgtgO7WtRaGx7vrk1keMHw+P/EBfhBMrwhAhTg91QRGZY2cHhx0f8hsFGRXYPPF8cFGKPqFwpQgJAAjGtgAeClYfXWcbUmIsCCbb1SAQoQEoBxDSwAvB2sqzURARZsW6JhaCC8uhLgjQEu8b4700AB2jD6BI76FtZADTzOwGVtO6qJCNDjmhOY0UQ0MOffrRRgAeCjYM1oIgIs2LbslRRhQJuIBhL65Hd1N53357ErBj7eNtpEBNghqIHwXSJAAUICML5lYPq+S+vSx9x7vHTe4ToBDqP7BAUoQEgAxjXwZIDkguF2DaPyJUIWl2bTOujI3Dg9whq4sT/EjjSb1s1VCI6ugQcCrLxD/9cuYVsFQG/BlbwAQ7PT2xwN3AAqwEIX9gh3CFS6cHiqu2WpqWSOU7IChNgFKEBIAMY1cBJAOOxz4st2x6O2UICQtAAFCAnA+Bviu4xRvaM39gAAAABJRU5ErkJggg==",
        type: ToolTypes.DRAG,
        start: start,
        move: move,
        end: end,
        emulateMissingEvents: true,
        raw: false,
        showStartPos: false
    })
})(editor);

//Erase Tool
(function (editor) {

    function start(pixel) {
        editor.erasePixel(pixel.x, pixel.y);
    }

    function move(pixel) {
        editor.erasePixel(pixel.x, pixel.y);
    }

    function end() {}

    editor.addTool({
        name: "Erase",
        icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABy0lEQVR4Xu3a0W6DMAxGYfr+D91dTKrUKbQ2x07CdHYdg/n4Y7Zoj8MfJPBA1RYfAsIQCCggFIDlJlBAKADLTaCAUACWm0ABoQAsN4ECQgFYbgIFhAKwfLcEPgfPs1uPby3u1pyAcEcIKCAUgOUmMAEYxYquS9y6bunKj0gUJrquTiVxJQETWKOlAgoIBWD5rARWz7Hq611mFPAy3W+hgAK+BGaFYclhQvXMqr7e5Rx2vLVVD7fkvgJezl7fR2RJEo7jWHJfE2gCXwK3TOCSphOhae+PbuH2BhNYo6Xt/QkI35CAAn4U2GoLj5rZ6ZC2HYs+rIADwcwMFFBA+MUQcB4gGcikNvqEM+4R6uVsBpIGSW2o6VUnL5mvMEEgtQJOSseMlxR6mW7hENP5og7A6lORbdI2awYK2DDHSIpILdyg38vdwt+NPq4QsAmweo5Fr5eZ0/DRa8rpaUym/m/Hu5/uhIQzANXDXED473ECCsj/wZJsa1Ibmk8zFmVmYPRLGr2mgPAvFgEFdAbiMRmdV5kb/YtfT6IPLGBU6mSdgAJCAVhuAjcEhC3dq7wjgfcSgN0KKCAUgOUmUEAoAMtNoIBQAJabQAGhACz/AYXablGyyYLdAAAAAElFTkSuQmCC",
        type: ToolTypes.DRAG,
        start: start,
        move: move,
        end: end,
        emulateMissingEvents: true,
        raw: false,
        showStartPos: false
    })
})(editor);

//Clear Tool
(function (editor) {

    function run() {
        return new Promise(function (resolve) {
            editor.showConfirmDialog('Do you really wish to clear the contents of the current layer?', function (clear) {
                if (clear) {
                    editor.clearLayer();
                }
                resolve();
            });
        });

    }

    editor.addTool({
        name: "Clear",
        icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABxUlEQVR4Xu2ayw7DIAwEyf9/dHvIqUqQ1hqLoHR6ZsGM17zSY/hDBA6kVjwECE0gQAFCAlCuAwUICUC5DhQgJADlOlCAkACU60ABQgJQrgMFCAlA+cyBH9jvW+UXXgKspVqANV6X1gIUICQA5UscuPvRiGyQAhxjCBCWoQAFCAlA+WMOTDeHuwBTLWRzkZNYIm3lJpJCiAbuJjXpj8QSaQU4z6QAJ0eW1krSgRs68C6kdOcj7rgbN+0vjRndRLqDIRPuTogAJ5XYnXQdWDg+PbYLp+VgCReymTZdsbaRWB4rYRI0cWo6blo1AiwQ3WoNTOO2hFNShcu/JVyAqgMLsNKFWwcWoOrAAiwdCGEJsACwuzS7+0uT+dhBunvC3f0J0Oesk0D6fqcDoWNeDTC6hBc2INKUxBJp/So3T48A/S5MivfURi4qvAwtOQeSoDmy3x5ILJG2sgamk0uPIml/3e3SHTx6BRJgLT2ohNOhdGD3H7FT8ovaWcIQ9PYA4fy2li9ZA7cmAIMToAAhASjXgasAwnH+R777oXf7TAgQpkiAAoQEoFwHChASgHIdKEBIAMp1oAAhASjXgQKEBKBcBwoQEoDyLwONjFGrgDkFAAAAAElFTkSuQmCC",
        type: ToolTypes.BUTTON,
        run: run
    })
})(editor);