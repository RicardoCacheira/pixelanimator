(function (editor) {

    function isValid(x, y, d, color) {
        if (!editor.hasSelection() || editor.isSelectedPixel(x, y)) {
            return (d.start.x <= x) && (d.end.x >= x) && (d.start.y <= y) && (d.end.y >= y) && color.equals(editor.getPixel(x, y));
        }
        return false;

    }

    //For now it only works with pixel canvas layers, not sprites.
    function run(pixel) {
        var dimensions = editor.getActionAreaDimensions()
        var targetColor = editor.getPixel(pixel.x, pixel.y);
        var finalColor = editor.getPrimaryColor();
        if (!targetColor.equals(finalColor)) {
            var queue = [pixel];
            while (queue.length) {
                var n = queue.shift();
                if (isValid(n.x, n.y, dimensions, targetColor)) {
                    var w = n.x;
                    var e = n.x;
                    var valid = true;
                    while (valid) {
                        if (isValid(w - 1, n.y, dimensions, targetColor)) {
                            w--;
                        } else {
                            valid = false;
                        }
                    }
                    valid = true;
                    while (valid) {
                        if (isValid(e + 1, n.y, dimensions, targetColor)) {
                            e++;
                        } else {
                            valid = false;
                        }
                    }
                    for (var i = w; i <= e; i++) {
                        editor.setPixel(i, n.y);
                        if (isValid(i, n.y + 1, dimensions, targetColor)) {
                            queue.push({
                                x: i,
                                y: n.y + 1
                            });
                        }
                        if (isValid(i, n.y - 1, dimensions, targetColor)) {
                            queue.push({
                                x: i,
                                y: n.y - 1
                            });
                        }
                    }
                }
            }
        }
    }

    editor.addTool({
        name: "Fill",
        icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAB6ElEQVR4Xu2aUXLDIAwFyf0P3c40nc40gVjyyoY4m289A6snhHFuzR8icENqxU2A0AQCFCAkAOU6UICQAJTrQAFCAlCuAy8G8KuznqWTvNrkBAgrQoAChASgXAcKEBKAch0oQEgAynWgACEBKI86MBoHp7Mtf9c3EQEOchsFE43bthCM0IEC/CMwxQxTBn2R9F5p9sJ7855S1gL8gBLWgYkkW8IJWL1QAbbWVoJweGM5ookI8EJlqANbQ3/BE+DVAUb3O7IrVO/Tpa6kkxMgsUbiyEKGoUl+HFsHkmwMkr47SbuFv4uwhBPZrIZFk5eY+mbo7rLOLEKAnTwI8A5FB24W6esAAV4dYGargCzK5CFXZhZGmkhmnDIC8EECFCAkAOU68F0BLvPROwEw5Lbo99XRuNEmIsABQQGe9CqnA3XgD4HQ2TUUlLz7iz5z98adaA690NJxo4sd3ViQxlS6kATU0nEFeCef4fAvVxkh6cKHl5IOhE74RIBnuLJ0vyMb/hFNRICDssnsq4+PqHZM9fOelpxZbHUT0YE6MHf+OcOBxJWHl+s7NBEBklN94hJjmRuf1ZqIDtSBiXeiiaFTGgZtIhN5PQ0tQJgNAQoQEoByHQgBLiPPnAOXmfRKExEgzIYABQgJQPk3lQOMUY32iZUAAAAASUVORK5CYII=",
        type: ToolTypes.CLICK,
        run: run,
        dialog: false,
        preview: false
    })
})(editor);