(function (editor) {
    function drawLine(x1, y1, x2, y2) {
        var points = calculateLinePoints(x1, y1, x2, y2);
        for (var p of points) {
            editor.setPixel(p.x, p.y);
        }
    }

    function drawRectangle(x1, y1, x2, y2) {
        if (x1 > x2) {
            var aux = x1;
            x1 = x2;
            x2 = aux;
        }
        if (y1 > y2) {
            var aux = y1;
            y1 = y2;
            y2 = aux;
        }
        drawLine(x1, y1, x1, y2);
        drawLine(x2, y1, x2, y2);
        if (x1 != x2) {
            drawLine(x1 + 1, y1, x2 - 1, y1);
            drawLine(x1 + 1, y2, x2 - 1, y2);
        }
    }

    function drawFilledRectangle(x1, y1, x2, y2) {
        if (x1 > x2) {
            var aux = x1;
            x1 = x2;
            x2 = aux;
        }
        for (var i = x1; i <= x2; i++) {
            drawLine(i, y1, i, y2);
        }
    }

    function drawFilledRectangleGradientHorizontal(x1, y1, x2, y2) {
        if (x1 > x2) {
            var aux = x1;
            x1 = x2;
            x2 = aux;
        }
        var primary = editor.getPrimaryColor();
        var secondary = editor.getSecondaryColor();
        var size = x2 - x1 + 1;
        var list = primary.gradient(secondary, size);
        for (var i = x1; i <= x2; i++) {
            editor.setPrimaryColor(list[i - x1]);
            drawLine(i, y1, i, y2);
        }
        editor.setPrimaryColor(primary);
    }

    function drawFilledRectangleGradientVertical(x1, y1, x2, y2) {
        if (y1 > y2) {
            var aux = y1;
            y1 = y2;
            y2 = aux;
        }
        var primary = editor.getPrimaryColor();
        var secondary = editor.getSecondaryColor();
        var size = y2 - y1 + 1;
        var list = primary.gradient(secondary, size);
        for (var i = y1; i <= y2; i++) {
            editor.setPrimaryColor(list[i - y1]);
            drawLine(x1, i, x2, i);
        }
        editor.setPrimaryColor(primary);
    }

    function drawCircle(x0, y0, x1, y1) {
        var radius = Math.round(Math.sqrt(Math.pow((x1 - x0), 2) + Math.pow((y1 - y0), 2)));
        var x = radius - 1;
        var y = 0;
        var dx = 1;
        var dy = 1;
        var decisionOver2 = dx - (radius << 1);

        while (x >= y) {
            editor.setPixel(x + x0, y + y0);
            editor.setPixel(y + x0, x + y0);
            editor.setPixel(-x + x0, y + y0);
            editor.setPixel(-y + x0, x + y0);
            editor.setPixel(-x + x0, -y + y0);
            editor.setPixel(-y + x0, -x + y0);
            editor.setPixel(x + x0, -y + y0);
            editor.setPixel(y + x0, -x + y0);
            if (decisionOver2 <= 0) {
                y++;
                decisionOver2 += dy;
                dy += 2;
            }
            if (decisionOver2 > 0) {
                x--;
                dx += 2;
                decisionOver2 += (-radius << 1) + dx;
            }
        }
    }

    editor.addTool({
        name: "Drawing Functions",
        type: ToolTypes.FUNCTION_LIST,
        list: [{
            func: drawLine,
            args: [],
            id: 'drawLine'
        }, {
            func: drawRectangle,
            args: [],
            id: 'drawRectangle'
        }, {
            func: drawFilledRectangle,
            args: [],
            id: 'drawFilledRectangle'
        }, {
            func: drawFilledRectangleGradientHorizontal,
            args: [],
            id: 'drawFilledRectangleGradientHorizontal'
        }, {
            func: drawFilledRectangleGradientVertical,
            args: [],
            id: 'drawFilledRectangleGradientVertical'
        }, {
            func: drawCircle,
            args: [],
            id: 'drawCircle'
        }]
    })
})(editor);

(function (editor) {

    var startPoint;

    function start(pixel) {
        startPoint = pixel;
        editor.functions.drawLine(startPoint.x, startPoint.y, pixel.x, pixel.y);
    }

    function move(pixel) {
        editor.functions.drawLine(startPoint.x, startPoint.y, pixel.x, pixel.y);
    }

    editor.addTool({
        name: "Line",
        icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABm0lEQVR4Xu3byxLCIBBE0fj/H62LLCVV014wQK5rJo9DwySWvg4/SOCFqi0+BIQhEFBAKADLTeAJ+G44lmxKg+AkrVAuIJwlAQWEArDcBAaALaxq+VfPeGITEbAal4txAgoIBWC5CQwAu2K1zrt7ExEwSFtrqIACQgFYbgIDwOFYuzcRAYO03dIwTOD1DP38OPdzIUzLiHKXcKB6C9ZOS1jAIG3TNAwTeAp03fe7HgymKil3CQda02CtuoQFDNI2dcMwgXAmBXwA4NT73QoJFBCuEgEFhAKw3AQGgMthzdZEBAzSttwbRvXe7vw2xgRWZ+linIAB4BZYdzYRAYO0bdswTCBMgYCLAG673/0rgQLCpAsoYF1gxKucCaz7N//pXS0fMXnVc3cbR2/iUWkb0YUFhFkWUEAmkOyBj08b3QMFbAiaQLaCo5+7mkATCOMWAJq2ovXVHiiggEUBOMwECggFYDl9Dkzq4aXOWZ4AtBpLUj+nALyqBEDA4DmwNS8CCgjXKwTsf/YNjpjsgRvcbv9bEBCaCiggFIDlH2oyRlFROjjYAAAAAElFTkSuQmCC",
        type: ToolTypes.DRAG,
        start: start,
        move: move,
        end: move,
        preview: true,
        emulateMissingEvents: false,
        raw: false
    })
})(editor);

(function (editor) {

    var startPointRectangle;
    var startPointFilledRectangle;
    var startPointCircle;

    function startRectangle(pixel) {
        startPointRectangle = pixel;
        editor.functions.drawRectangle(startPointRectangle.x, startPointRectangle.y, pixel.x, pixel.y);
    }

    function moveRectangle(pixel) {
        editor.functions.drawRectangle(startPointRectangle.x, startPointRectangle.y, pixel.x, pixel.y);
    }

    function startFilledRectangle(pixel) {
        startPointFilledRectangle = pixel;
        editor.functions.drawFilledRectangle(startPointFilledRectangle.x, startPointFilledRectangle.y, pixel.x, pixel.y);
    }

    function moveFilledRectangle(pixel) {
        editor.functions.drawFilledRectangle(startPointFilledRectangle.x, startPointFilledRectangle.y, pixel.x, pixel.y);
    }

    function startCircle(pixel) {
        startPointCircle = pixel;
        editor.functions.drawCircle(startPointCircle.x, startPointCircle.y, pixel.x, pixel.y);
    }

    function moveCircle(pixel) {
        editor.functions.drawCircle(startPointCircle.x, startPointCircle.y, pixel.x, pixel.y);
    }

    editor.addTool({
        name: "Shape Tools",
        type: ToolTypes.TOOL_LIST,
        list: [{
                name: "Rectangle",
                icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABIklEQVR4Xu3cQQ5EQABFQe5/aA6AReeJSHfN2jOm/GSs7JtPEthTLd4AxhEABBgFYv60wCOed9b84gVw7FYDHPO6HA0QYBSIeVrgao88d3+kAAcWCHAA6+5QgACjQMwtEGAUiLkFAowCMbdAgFEg5hYIMArE3AIBRoGYWyDAKBBzCwQYBWJugQCjQMwtEGAUiLkFAowCMbdAgFEg5hYIMArE3AIBRoGYWyDAKBBzCwQYBWJugQCjQMwtEGAUiLkFAowCMbdAgFEg5hYIMArE3AIBRoGYWyDAKBBzCwQYBWJugQCjQMwtEGAUiLkFAowCMbdAgFEg5hb4N8B4PVPk6d1ZUwjEHwEQYBSIuQV+BRi/Z518tbdSvn5nAUZSgACjQMxPXzaCUS0Ixo8AAAAASUVORK5CYII=",
                type: ToolTypes.DRAG,
                start: startRectangle,
                move: moveRectangle,
                end: moveRectangle,
                preview: true,
            },
            {
                name: "Filled Rectangle",
                icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABEElEQVR4Xu3SwQ2AIAAEQei/aC2A+CBrfMj4djUMN4cnCcxUiwfAOAKAAKNAzJ8WeMXv/jVfvADuXTXAPa/lbYAAo0DMLRBgFIi5BQKMAjG3QIBRIOYWCDAKxNwCAUaBmFsgwCgQcwsEGAViboEAo0DMLRBgFIi5BQKMAjG3QIBRIOYWCDAKxNwCAUaBmFsgwCgQcwsEGAViboEAo0DMLRBgFIi5BQKMAjG3QIBRIOYWCDAKxNwCAUaBmFsgwCgQcwsEGAViboEAo0DMLRBgFIi5BQKMAjG3QIBRIOYWCDAKxNwCAUaBmFsgwCgQcwsEGAViboEAo0DMLfArwPifc/Jlkucc/Z2TAoyOAAFGgZjfCktGUS+p9mcAAAAASUVORK5CYII=",
                type: ToolTypes.DRAG,
                start: startFilledRectangle,
                move: moveFilledRectangle,
                end: moveFilledRectangle,
                preview: true,
            }, {
                name: "Circle",
                icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABrUlEQVR4Xu3cy7LCIBRE0eT/P1onTlJCOLhb4mPfMU1g0ZRadXXf/EMCO0ob3gSEJRBQQCgA46saeIPrTMaje45OdrJLAWEFBBSwLeAVhs2ggNWrSZ8Dt3mIR9dMNxZdTFIp8IJWsikNWrUYAfsC9KCSttFbQzcWXUxSadWtmQFsYc3kF/nEHlPa7wxAacLY8q+fqLRfAfsHJSAssYACQgEYt4ECQgEYt4ECQgEYRw0sheECvzH+5NJ7Iy1g+3gFhLUXUEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOM2UEAoAOPlBrae82//tVrar9+V61dSwPR13bbn3xqzgTYQ9kxAAd8mACf2RURAKADjNvDTAKufTlrjZt4uwX0P461mvbxmurHoYoZbzwyIrlnA/qGUbEqDTg4+epqZgg1nia5ZwIsbODzux4DqqVfnI+NoaQ7Pjk4WuOoEppqN7jk6mYDVM5wf5xWeNzskfhbwDpTPjC5SHo2AAAAAAElFTkSuQmCC",
                type: ToolTypes.DRAG,
                start: startCircle,
                move: moveCircle,
                end: moveCircle,
                preview: true,
            }
        ]
    });
})(editor);

(function (editor) {

    var startPointH;
    var startPointV;

    function startH(pixel) {
        startPointH = pixel;
        editor.functions.drawFilledRectangleGradientHorizontal(startPointH.x, startPointH.y, pixel.x, pixel.y);
    }

    function moveH(pixel) {
        editor.functions.drawFilledRectangleGradientHorizontal(startPointH.x, startPointH.y, pixel.x, pixel.y);
    }

    function startV(pixel) {
        startPointV = pixel;
        editor.functions.drawFilledRectangleGradientVertical(startPointV.x, startPointV.y, pixel.x, pixel.y);
    }

    function moveV(pixel) {
        editor.functions.drawFilledRectangleGradientVertical(startPointV.x, startPointV.y, pixel.x, pixel.y);
    }

    editor.addTool({
        name: "Gradient Tools",
        type: ToolTypes.TOOL_LIST,
        list: [{
            name: "Filled Rectangle Gradient Horizontal",
            icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACgCAYAAACLz2ctAAAEz0lEQVR4Xu3da2rjUAwF4GQNyf63OkPLgCG4loxVzmX89W/MVSwfn4dU2ufDjw4EO/AM1lZaBx4ACATRDgBgtP2KAyAMRDsAgNH2Kw6AMBDtAABG2684AMJAtAMAGG2/4mcA+KfTrtfr1bns8X6/W9dNn5eq+7/cR7d/z+ezha3WRf+QAoA7r0z3gQDgPt8A4A88PA2Y6fNWBz4G/AFYqQcHgBjwuwMAuA+Ebl+612FADPjdgRTzAiAAAuDRnKVL5ak3eLru9Hmr9w8DYkAMiAG3DmBAKVgKPmCErqR3ryPBJJgEk2ASXC38reKs4k5ZExJ8UVq7DZwODdPnpe6jW5cHvAjUacBMn9cFQqouAAKgECKECCFCyEcHUtKVksJUXRJMgkkwCSbBJJgEVxgwBzzqUMrDTNedPi/lZbt1eUAekAfkAXnASv/tgu2CeUAecOtA12N1r0t5Tx6QB+QBeUAekAc0B6wwwAPygDxg5y3x17F2urR6GBBCFgsDKcCkgJCqKwUvBvwUEFJ1ARAAjWGMYYxhqnBhFWcVZwxjDGMMUzHl1+fGMMYw7b8wK4QIIUKIECKEVNIqhAghQogQIoRUTCmELOYpUyvFbl0hZDHApFZiqboACIBSsBQsBVfeTgqWgqVgKVgKrphSCl7MU3bTqBDiwUXDAAACIAAe6KsQIoQIIUKIECKEfHRg2jtNnyeEkK5T0gWA+4DhAb1Ip16kLvPaBS+WvjEgBjz1pk8DZvq8LhOl6mJADBidPwIgAALg0WxndQmZlq7p81bvHwbEgBgQA24dwIBSsBR8wAhdSe9eR4JJMAkmwSS4+iUDqziruFPWhARflNZuA6dDw/R5qfvo1uUBLwJ1GjDT53WBkKoLgAAohAghQogQ8tGBlHSlpDBVlwSTYBJMgkkwCSbBFQbMAY86lPIw03Wnz0t52W5dHpAH5AF5QB6w0n+7YLtgHpAH3DrQ9Vjd61LekwfkAXlAHpAH5AHNASsM8IA8IA/YeUv8u9adLq0eBoSQxcJACjApIKTqSsGLAT8FhFRdAARAYxhjGGOYKlxYxVnFGcMYwxjDVEz59bkxjDHMoztFEEKEECFECBFCKmkVQoQQIUQIEUIqphRCFvOU3TBgE+LBRcMAAAIgAB7oqxAihAghQogQIoR8dGDaO02fJ4SQrlPSBYD7gOEBvUinXqQu89oFL5a+MSAGPPWmTwNm+rwuE6XqYkAMGJ0/AiAAAuDRbGd1CZmWrunzVu8fBsSAGBADbh3AgFKwFHzACF1J715HgkkwCSbBJLj6JQOrOKu4U9aEBF+U1m4Dp0PD9Hmp++jW5QEvAnUaMNPndYGQqguAACiECCFCiBDy0YGUdKWkMFWXBJNgEkyCSTAJJsEVBswBjzqU8jDTdafPS3nZbl0ekAfkAXlAHrDSf7tgu2AekAfcOtD1WN3rUt6TB+QBeUAekAfkAc0BKwzwgDwgD9h5S/yjmp0urR4GhJDFwkAKMCkgpOpKwYsBPwWEVF0ABEBjGGMYY5gqXFjFWcUZwxjDGMNUTOlzHfiVDpyR4F/5Ag69dwcA8N7PP373ABh/BPf+AgB47+cfv3sAjD+Ce38BALz384/fPQDGH8G9vwAA3vv5x+8eAOOP4N5f4C+BuDC/pGH21AAAAABJRU5ErkJggg==",
            type: ToolTypes.DRAG,
            start: startH,
            move: moveH,
            end: moveH,
            preview: true,
        }, {
            name: "Filled Rectangle Gradient Vertical",
            icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACgCAYAAACLz2ctAAAEwklEQVR4Xu3Z3UkkYRSE4Z4YZGIQUxBjGIxBTEGMQSYFMQYxBjEFMQYxhlkaNoDqma+pouvd6zN9furBi/12E/+4gPECO2NvWnOBCYAgsF4AgNbz0xyAGLBeAIDW89McgBiwXgCA1vPTHIAYsF4AgNbz03wJwBPn4gILLiDZkor+NwXggutTqj1yABApa11AsiUV8RdwrYw2/V3JllQEwE1DWWs5yZZUBMC1Mtr0dyVbUhEANw1lreUkW1IRANfKaNPflWxJRQDcNJS1lpNsSUUAXCujTX9XsiUVzWe6urriP6I37WXscn9/f5ItqQiAY8Np+BoAG1IO3hGAweE0jAbAhpSDdwRgcDgNowGwIeXgHQEYHE7DaABsSDl4RwAGh9Mw2nCA19fXvIQ0yBm048/Pj/TIIRXNMwFwUDIlnwFgSdCpawIwNZmSuQBYEnTqmgBMTaZkLgCWBJ26JgBTkymZC4AlQaeuCcDUZErmGg7w9vaWl5ASPCPW/Pr6kh45pKJ5IACOiKXnGwDsyTpyUwBGxtIzFAB7so7cFICRsfQMBcCerCM3BWBkLD1DAbAn68hNARgZS89QwwEeDgdeQnr8XLzpx8eH9MghFc3TAPDiTKo+AMCquPOWBWBeJlUTAbAq7rxlAZiXSdVEAKyKO29ZAOZlUjURAKvizlsWgHmZVE00HODDwwMvIVWELlv27e1NeuSQiuZRAHhZIG2/BmBb4mH7AjAskLZxANiWeNi+AAwLpG0cALYlHrYvAMMCaRsHgG2Jh+0LwLBA2sYZDvDp6YmXkDZFF+x7PB6lRw6paJ4DgBekUfhTABaGnrQyAJPSKJwFgIWhJ60MwKQ0CmcBYGHoSSsDMCmNwlkAWBh60soATEqjcJbhAF9eXngJKYR07srPz8/SI4dUNA8BwHOj6PwdADtzj9kagDFRdA4CwM7cY7YGYEwUnYMAsDP3mK0BGBNF5yAA7Mw9ZmsAxkTROchwgK+vr7yEdFo6a+vHx0fpkUMqmicA4Fk51P4IgLXRZywOwIwcaqcAYG30GYsDMCOH2ikAWBt9xuIAzMihdgoA1kafsTgAM3KonWI4wPf3d15CajktX/z+/l565JCK5vYAXB5C8y8A2Jx+wO4ADAiheQQANqcfsDsAA0JoHgGAzekH7A7AgBCaRwBgc/oBuwMwIITmEYYD/Pz85CWkWdTC3e/u7qRHDqlo7g3AhQmUlwOwHIB7fQC6EyjvD8ByAO71AehOoLw/AMsBuNcHoDuB8v4ALAfgXh+A7gTK+w8H+P39zUtIOaol69/c3EiPHFLR3BiAS85PLQAxYL0AAK3npzkAMWC9AACt56c5ADFgvQAAreenOQAxYL0AAK3np/lwgL+/v7yE4Eq+wH6/lx45pKK5KwDl21M4TRMAYWC9AACt56c5ADFgvQAAreenOQAxYL0AAK3npzkAMWC9AACt56f5cICn04mXEFzJF9jtdtIjh1Q0dwWgfHsKp2kCIAysFwCg9fw0ByAGrBcAoPX8NAcgBqwXAKD1/DQHIAasFwCg9fw0Hw6Qk3KBNS4gv4Ss0ZxvcgEAYsB6AQBaz09zAGLAegEAWs9PcwBiwHoBAFrPT3MAYsB6AQBaz0/zfwRVEr8YVZ4hAAAAAElFTkSuQmCC",
            type: ToolTypes.DRAG,
            start: startV,
            move: moveV,
            end: moveV,
            preview: true,
        }]
    })
})(editor);