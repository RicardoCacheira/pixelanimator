(function (editor) {

    var startPos

    function start(pixel) {
        startPos = pixel;
    }

    function move(pixel) {
        editor.functions.drawFilledRectangle(startPos.x, startPos.y, pixel.x, pixel.y);
    }

    function end(pixel) {
        var x1 = startPos.x
        var x2 = pixel.x
        var y1 = startPos.y
        var y2 = pixel.y

        if (x1 > x2) {
            var aux = x1;
            x1 = x2;
            x2 = aux;
        }

        if (y1 > y2) {
            var aux = y1;
            y1 = y2;
            y2 = aux;
        }
        var sizeX = x2 - x1 + 1;
        var sizeY = y2 - y1 + 1;

        /*
        0------1
        |      |
        |      |
        2------3
        */
        var p0 = editor.getPixel(x1, y1);
        var p1 = editor.getPixel(x2, y1);
        var p2 = editor.getPixel(x1, y2);
        var p3 = new Color(p1.red + p2.red, p1.green + p2.green, p1.blue + p2.blue, p1.over(p2).alpha)

        var g01 = p0.gradient(p1, sizeX);
        var g02 = p0.gradient(p2, sizeY);
        var g13 = p1.gradient(p3, sizeY);
        var g23 = p2.gradient(p3, sizeX);

        var primary = editor.getPrimaryColor();
        for (var i = x1; i <= x2; i++) {
            editor.setPrimaryColor(g01[i - x1]);
            editor.setPixel(i, y1);
            editor.setPrimaryColor(g23[i - x1]);
            editor.setPixel(i, y2);
        }

        for (var y = y1 + 1; y < y2; y++) {
            var g = g02[y].gradient(g13[y], sizeX)
            for (var x = x1; x <= x2; x++) {
                editor.setPrimaryColor(g[x - x1]);
                editor.setPixel(x, y);
            }
        }


        editor.setPrimaryColor(primary);
    }

    editor.addTool({
        name: "Colorgrid Tool",
        icon: "icon url",
        type: ToolTypes.DRAG,
        start: start,
        move: move,
        end: end,
        dialog: false,
        deactivate: false,
        preview: true,
        emulateMissingEvents: false,
        raw: false,
        showStartPos: true,
        showPosDifference: false
    })
})(editor);