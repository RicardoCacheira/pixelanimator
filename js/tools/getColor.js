(function (editor) {
    function run(pixel) {
        if (editor.hasNoSelection() || editor.isSelectedPixel(pixel.x, pixel.y)) {
            editor.setPrimaryColor(editor.getPixel(pixel.x, pixel.y));
        }
    }

    function runF(pixel) {
        if (editor.hasNoSelection() || editor.isSelectedPixel(pixel.x, pixel.y)) {
            editor.setPrimaryColor(editor.getFinalPixel(pixel.x, pixel.y));
        }
    }

    editor.addTool({
        name: "Get Color Tools",
        type: ToolTypes.TOOL_LIST,
        list: [{
                name: "Get Color",
                icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABm0lEQVR4Xu3a2w6CMBREUfj/j9YYn9BizunuBen2uUNlOW2DYd/8IIEdpQ1vAsISCCggFIBxGyggFIBxGyggFIBxG7go4KNw36UylMZFyULlCg2KzjhwnIAQW0ABoQCM28AEIDkIEtN8DQ2dD6FB5Fs0yAoIEQUUEArAuA0UEArAuA0UEArAuA0UEAok4tFHtNIlSVOrHyiqgwmUzFABM1qFsQIKCAVg3AYmAAlWdJruB8vMQ0TAaA1Oxgko4Db1/UAbmGjgCCzydFJ9FlQHE3ivoQImwT6HCyhgWcAl/HapdqgO/mjklZZrj/s73HqPCQS80X7XoyA2EBZEwCsBLrXfleDpHiEgrLOAAjIBlzDzSz3CLL9c6SEiYEEws4QFFBBueAKOA3S5Bq3P9kABBQwKwGE2UEAoAOMzGzhrn4Vkx7iAkFNAAaEAjNvAPwG8xYGR+Tur9Q23vh7sTbv4qCUsIHkBZ+L7ge2qdnIlGwiJM4Bkqsw/32Se4VkBIbmAAkIBGLeBnQDhZdeJ3/Z0HPUTCgilBRQQCsD4E+ZMc1F3H/LkAAAAAElFTkSuQmCC",
                type: ToolTypes.CLICK,
                run: run,
                dialog: false,
                deactivate: true,
                raw: false
            },
            {
                name: "Get Final Color",
                icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABwklEQVR4Xu3aQXKDMBBEUXz/QyeV8iYJkmtaH4QKvtczFjxakgG/Nj9I4IW6bd4EhCEQUEAoANtNoIBQALabQAGhAGw3gQ8F/GqcdysMrboqWSlcpaLqiBPrBITYAgoIBWC7CQwAyUYQDLMrLe0PpSJyFAf0CggRBRQQCsB2EyggFIDtJlBAKADbTaCAUCBor96itb6SJHX4hmK4MUBJSgVMtBq1AgoIBWC7CQwACVZ1mNM3lis3EQGrMejUCSjgdun/A01gkMAZWOTuZHgvGG4M8H5KBQzB/pcLKGBbwCn8dhl2GG78kMiVpusZ5/fn1M8YQMAbrXdnBMQEwoAIuBLgo9a7FjxdIwSEcRZQQCbgFGZ+0S3M46cr3UQEbAgmU1hAAeGCJ+A8wDtP10NftvfWQAGLD6AFzGb1zktAATOBzivW6pcslcBbrLNXTmEBq7nv1AkoYP+F8ox0zBijdY0PHXfWGnjoQZ+Q/OEnVQL2r0bpSZWAAsYTuvowwQQGP5+mrIHxpf7VULqaZICg99ANLVkDg2PclQpI9MgfGOG4S/0OJOdiAoneExMIvZZun7KJLC0AD05AAaEAbDeBKwF+A6qsjFHEKNyDAAAAAElFTkSuQmCC",
                type: ToolTypes.CLICK,
                run: runF,
                dialog: false,
                deactivate: true,
                raw: false
            }
        ]
    })
})(editor);