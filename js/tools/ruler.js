(function (editor) {

    var startPoint;

    function start(pixel) {
        startPoint = pixel;
        editor.functions.drawLine(startPoint.x, startPoint.y, pixel.x, pixel.y);
    }

    function move(pixel) {
        editor.functions.drawLine(startPoint.x, startPoint.y, pixel.x, pixel.y);
    }

    function end(pixel) {
        editor.showInfoDialog('Start:\nX: ' + startPoint.x + ',Y: ' + startPoint.y + '\nEnd:\nX: ' + pixel.x + ',Y: ' + pixel.y + '\nLength:\nX: ' + (Math.abs(pixel.x - startPoint.x) + 1) + ',Y: ' + (Math.abs(pixel.y - startPoint.y) + 1));
    }

    editor.addTool({
        name: "Ruler",
        icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABZUlEQVR4Xu3b246CMABF0fr/H+08mExMqElhM4Ywy2cOl8VpCxofwycJPFJaeACMJQAIMArEuAYCjAIxroEAo0CMayDAKBDjGggwCsS4BgKMAjGugQCjQIxrIMAoEOMa+EeAz7jfu8Y3hfvUQIDzCgCMQwMgwCgQ46mBs/lyNleevd3sms8+xuH97VlEDh9kjFGgAQJ8dUADJwaGcCwHQIC/a0xZqA5PTxqogRr4/py3+iXG4SEXH6lOfxOJb0abeAH8xrkA3KG8tCjVRWTH+SxtqoFLTJ83AgjwJbA0/iPW1eNLBlebA6+ECjDeDYAAo0CMayDAKBDjGggwCsS4BgKMAjGugQCjQIxrIMAoEOOnNzCezy3i6TeRWwjEiwAIMArEuAZ+CzAe5//E/VMp3muAAKNAjGsgwCgQ4xoIMArEuAYCjAIxroEAo0CMayDAKBDjGggwCsS4BgKMAjGugQCjQIz/AMNmfVEpaBodAAAAAElFTkSuQmCC",
        type: ToolTypes.DRAG,
        start: start,
        move: move,
        end: end,
        dialog: false,
        deactivate: false,
        preview: true,
        emulateMissingEvents: false,
        raw: false
    })
})(editor);