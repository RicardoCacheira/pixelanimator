(function (editor) {

    function runUp() {
        if (editor.hasSelection()) {
            editor.scaleUpSelectionContent(2);
        } else {
            editor.scaleUpLayer(2);
        }
    }

    function runDown() {
        if (editor.hasSelection()) {
            editor.scaleDownSelectionContent(2);
        } else {
            editor.scaleDownLayer(2);
        }
    }

    editor.addTool({
        name: "Scaling Tools",
        type: ToolTypes.TOOL_LIST,
        list: [{
            name: "Scale Up Tool",
            icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACgCAYAAACLz2ctAAAGJElEQVR4Xu2d224UWxBDk///aBARgiAmabsu7emedZ7r4rJX7zOCB97f+A8Hgg68B3ezGgfeABAIog4AYNR+lgMgDEQdAMCo/SwHQBiIOgCAUftZDoAwEHUAAKP2sxwAYSDqAABG7Wc5AMJA1AEAjNrPcgCEgagDABi1n+UACANRBwAwaj/LARAGog4AYNR+ll8BwB/DMak3s/ex8ap/Umyjw6SNfhEg+J597lAzVn1W50mqR4dJG/0i1Rh1snoze3kBPxwABPXT6gGj+qx+wJLq0WHSRr9INUadrN7M3h7QUh5qGNKwpSJA6BmrZqz6rM6TVI8Okzb6Raox6mT1ZvbyAvIbUP2qvqmb/uDUeZL00WHSRr+Il8j3jD+G6Xn2TzcA9sxUHxnVZ3WepHp0mLTxb1HkYFPjK5VH8gDAV0Ls+1sB8At/kh/JK+EJgAAY5R0AARAAz3Qg8sWdeeDFdkXySP6+ihx8MSjOlBvJAwDPjPi5dwEgvwGjhAIgAALgmQ5EvrgzD2TXsQP8Bjz2iIpFBwBw0VxGHzsAgMceUbHoAAAumsvoYwcA8NgjKhYdAMBFcxl97AAAHntExaIDALhoLqOPHQDAY4+oWHQgCeDiWYwuOBD5mykALCR10xYAvGmwVzkLAK+S1E11AuBNg73KWQB4laRuqhMAbxrsVc4CwKskdVOdAHjTYK9yFgBeJamb6rwNgOohN82xfVbqLwfU3Eb1jQ77bb16SDupmw7YyESxSs1tVN/oMABUcj6s2cjkcKnxz2GM6hsdBoBKzoc1G5kcLgVAxaLXqAHAZs7qb4nmmtu2A2AzWgDsGQiAPf/G/223ppzLtQNgMzJewJ6BANjzjxew6R8ANg1UX8CU0c3zyu348sC6DQgw+jGj+AKA5ddrohEAAXCCo/IMAATAMjwTjQAIgBMclWcAIACW4ZloBEAAnOCoPAMAAbAMz0QjAALgBEflGQAIgGV4JhqfHcCIPv4mZAItbUYkYE3aR1VEHwAaCTVLIwEbmiP6ANBIqFkaCdjQHNEHgEZCzdJIwIbmiD4ANBJqlkYCNjRH9AGgkVCzNBKwoTmiDwCNhJqlkYANzRF9AGgk1CyNBGxojugDQCOhZmkkYENzRB8AGgk1SyMBG5oj+gDQSKhZGgnY0BzRB4BGQs3SSMCG5og+ADQSapZGAjY0R/QBoJFQszQSsKE5og8AjYSapZGADc0RfQBoJNQsjQRsaI7oA0AjoWZpJGBDc0QfABoJNUsjARuaI/oA0EioWRoJ2NAc0QeARkLN0kjATc3r7QC4bvGfBQD4wGsABMDzHADAqNe8gAAIgFEHADBqPy8gAAJg1AEAjNrPCwiAABh1AACj9vMCAiAAfuNA5APhD6LPYzISsHFeRB8AGgk1SyMBG5oj+gDQSKhZGgnY0BzRB4BGQs3SSMCG5og+ADQSapZGAjY0R/QBoJFQszQSsKE5og8AjYSapZGADc0RfQBoJNQsjQRsaI7oA0AjoWZpJGBDc0QfABoJNUvVgJtr/mtXM1b1qfOkO0aH/d4YOUS6Nluk+jKtUs1Y1afOk+4YHQaA33quBiwFZxSpGav61HmSxNFhAAiAEnWfigDQdaxer74w9Q2PO9WMVX3qPOmO0WG8gLyAEnW8gK5NI/XqCzOyrJCxqm/00RodxgvIC+h+PQDoOlavV1+Y+gZ+A344oBq9Af90eJPzVF8md/6apfqs6lPnSXeMDjP/FywJfMGijUye1saNY9Uv6WlNCQvbyCR80tfrN44FwF7cG5n0FC12bxwLgL3ANjLpKVrs3jgWAHuBbWTSU7TYvXEsAPYC28ikp2ixe+NYAOwFtpFJT9Fi98axANgLbCOTnqLF7o1jAbAX2EYmPUWL3RvHAmAvsI1MeooWu1/q2EUfGV10AACLxtE24wAAzvjIlKIDAFg0jrYZBwBwxkemFB0AwKJxtM04AIAzPjKl6AAAFo2jbcYBAJzxkSlFBwCwaBxtMw4A4IyPTCk6AIBF42ibcQAAZ3xkStEBACwaR9uMAwA44yNTig4AYNE42mYcAMAZH5lSdAAAi8bRNuMAAM74yJSiAwBYNI62GQd+AgW/DrA0Vpa8AAAAAElFTkSuQmCC",
            type: ToolTypes.BUTTON,
            run: runUp
        }, {
            name: "Scale Down Tool",
            icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACgCAYAAACLz2ctAAAF7UlEQVR4Xu2c205bQRRD4f8/ulUleEFpZON94p0zq8+eGV8WCRJSPz/4RwPFBj6Lb/M0DXwAIBBUGwDAav08DoAwUG0AAKv18zgAwkC1AQCs1s/jAAgD1QYAsFo/jwMgDFQbAMBq/TwOgDBQbQAAq/XzOADCQLUBAKzWz+MACAPVBgCwWj+PAyAMVBsAwGr9PN4E8A/1v2UDo8yMXmbWCYBmYUvko8yMXmYWBIBmYUvko8yMXmYWBIBmYUvko8yMXmYWBIBmYUvko8yMXmYWBIBmYUvko8yMXmYWBIBmYUvko8yMXmYWBIBmYUvko8yMXmYWBIBmYUvko8yMXmYWBIBmYUvko8yMXmYWpALY9GhGemt5ZY/muJXAb43IteYrewDgtaO+0+0A+J+1mj8k7wRQ6hUAATBlKDoPgAAYAZQeBkAATBmKzgMgAEYApYcBEABThqLzAAiAEUDpYQAEwJSh6DwAAmAEUHoYAAEwZSg6D4AAGAGUHgZAAEwZis4DIABGAKWHARAAU4ai8wAIgBFA6WEABMCUoeg8AAJgBFB6GAABMGUoOg+AABgBlB4GQABMGYrOAyAARgClhwEQAFOGovMACIARQOlhAATAlKHoPAACYARQehgAATBlKDoPgAAYAZQeBsCwQfW/8FCLVu1Mv6vep/pTdWovo/5GL1OTfunUwOq1apbt76o51F5UndrLqL/Ry9SkAPi0qdYmAGgC/FOuDqcWrdqZfle9T/Wn6tReRv2NXqYm5ROQT8DvBgDQ/Kl5IFc7rHzCGPEq/tTyjByyVA2sXqhm2f6umkPtRdWpvYz6G71MTcpXMF/BfAWbPy1P5OoPceUTxohZ8aeWZ+SQpWpg9UI1y/Z31RxqL6pO7WXU3+hl5lfrFW+rZTd0lYGNoBV/V0BQCWIU3ZJu76XiDwBfh2NlYCNexR8AGguF0srAhueKPwA0FgqllYENzxV/AGgsFEorAxueK/4A0FgolFYGNjxX/AGgsVAorQxseK74A0BjoVBaGdjwXPEHgMZCobQysOG54g8AjYVuLgXAmw+8PR4Abl/o5v4A8OYDb48HgNsXurk/ALz5wNvjAeD2hW7uDwBvPvD2eAC4faGb+wPAmw+8PR4Abl8o9FcZ2PBc8cef4oyFQmllYMNzxR8AGguF0srAhueKPwA0FgqllYENzxV/AGgsFEorAxueK/4A0FgolFYGNjxX/AGgsVAorQxseK74A0BjoVBaGdjwXPEHgMZCobQysOG54g8AjYVCaWVgw3PF3xUAGpmPklYGNhqu+ANAY6FQWhnY8FzxB4DGQqG0MrDhueIPAI2FQmllYMNzxR8AGguF0srAhueKPwA0FgqllYENzxV/AGgsFEorAxueK/4A0FgolFYGNjxX/AGgsVAorQxseK74A0BjoVBaGdjwXPF3BYCVIEbRSB83UNkNAMHxuwEAhIVqAwBYrZ/HARAGqg0AYLV+HgdAGKg2AIDV+nkcAGGg2gAAVuvncQC8OQPqwNtrGP3jxehlX82pRV/x9ubx1F42Z/jnbXS30csA8Ck7APigHgB83ecNAALg62h78BIAAiAADjQw+q05ehm/A/I7oAs4ALqN/V7PVzBfwb+nZ+AkAALgAEa/vwIAXwSgOtH0IOqvE9vfVXOoPa/WNcNuB0EdTu1Qzavep/pbrWuGVQdRC1SzbH9XzaH2slrXDLsdBHU4tUM1r3qf6m+1rhlWHUQtUM2y/V01h9rLal0z7HYQ1OHUDtW86n2qv9W6Zlh1ELVANcv2d9Ucai+rdc2w20FQh1M7VPOq96n+VuuaYdVB1ALVLNvfVXOovazWNcNuB0EdTu1Qzavep/pbrTsq7OolDjUHgIcOvyU2AG5Z4lAfAHjo8FtiA+CWJQ71AYCHDr8lNgBuWeJQHwB46PBbYgPgliUO9QGAhw6/JTYAblniUB8AeOjwW2ID4JYlDvUBgIcOvyU2AG5Z4lAfAHjo8FtiA+CWJQ71AYCHDr8lNgBuWeJQHwB46PBbYv8FFooYsG/Ar4wAAAAASUVORK5CYII=",
            type: ToolTypes.BUTTON,
            run: runDown
        }]
    })
})(editor);