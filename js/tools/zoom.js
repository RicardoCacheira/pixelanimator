(function (editor) {
    function zoomIn() {
        var zoom = (editor.getZoom() * 100) | 0;
        if (zoom > 100) {
            zoom = ((zoom / 25) | 0) * 25 + 25;
        } else {
            zoom = ((zoom / 10) | 0) * 10 + 10;
        }
        editor.setZoom(zoom / 100);
    }

    function zoomOut() {
        var zoom = (editor.getZoom() * 100) | 0;
        if (zoom > 100) {
            if (zoom % 25) {
                zoom = ((zoom / 25) | 0) * 25
            } else {
                zoom = ((zoom / 25) | 0) * 25 - 25;
            }

        } else {
            if (zoom % 10) {
                zoom = ((zoom / 10) | 0) * 10
            } else {
                zoom = ((zoom / 10) | 0) * 10 - 10;
            }
        }
        editor.setZoom(zoom / 100);
    }

    editor.addTool({
        name: "Zoom In",
        icon: 
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABz0lEQVR4Xu2ZUW4DIQwFN/c/dCs1VVQlINkeYKGafPPAHp5NYB+XP0TggdSKLwFCEwhQgJAAlOtAAUICUK4DBQgJQLkOPBjgF4x9pLxspLJwQPQChBAFKMDr1ruwDkw4sAXrzv77Hno5vlVJlANMbBIZWo5PgE/sAiT2EyCktxvAcjlwDkNnCOUxoweGFh6a6pzJQnkIsA9fgNCYAhQgLC8BCvBFYPRhFepPOlAH6sBKFWxTXpXg/2hCeYzuTehlI5FwKLnEfK2hoTUECPusAAV470G1mwNXfGiK5nxkDxQged3taOFh+iHXgZCoAAUICcA2EV39yENkaHLRyTrjBChAeEsQoADvvWbpQB2oAytVEDr+KxNnX4tXrBG91mRiEWCGVmPsCoAwxJA8lMepDgwRgIMEKEBIAMp14H8C2MplRv+tMgu5bVUS0e8aAky8o63aPB1YJQB1lvCpAKP9DuY3XV7ux2Xhb0oChHsrQAH+EChXYlloCT8JZACScs2sA4tirTyTmAAbeyNAaFgBChASgPKeA+13QbACDILqDROgACEBKNeBkwC2pi0/OsIYt5bT/4EZ/dYgqsFlAOjACVe5zAZUN3lrXQaADmxs5TedRIxRjKTg6QAAAABJRU5ErkJggg==",
        type: ToolTypes.BUTTON,
        run: zoomIn,
    })
    editor.addTool({
        name: "Zoom Out",
        icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABpUlEQVR4Xu3ZwbqCIBRFYX3/h66BjQr6DmdxUGvdcRvhZ0s33Tf/kMCO0oY3AWEJBBQQCsC4DRQQCsC4DRQQCsC4Dbwx4APOfWY8XaR0cMLsBYSIAgq4nfpb2AYONLCFdeb5+z719PxWLSI9wYFNIh9Nz0/Ag11AUj8Bod7VANO3A3eYOkJoHRVnYOjCU5daM1hoHQL28QWExRRQQCgA4zZQQCgA4zZQQCgA4zZQQCgA4zZQQCgA4zbwHwFXvGiKPkC5ZQMFJE93O1l4J37EbSAUFVBAKLAofssvkUU2ocsIGGLynQhkElDAMgE4sGeggFAAxm2ggFAAxm2ggFAAxm2ggFAAxm3gLwG21hJ9sAkdQvFQ21YtIvpeQ8DO3goYKv3YY6BV7c9O3Vs4K/fKnQYYvV3h+srj6fM4Hfyyc+WrLbhA2iEdFPAQEBA6CLgQkHxh0I0qOPbmDDmyMAEb5gLCIgooIBSA8V4DPe+CsAIGoXofE1BAKADjNrAIsDVs+pkZnOOl4/T/wJH8pSGykxsBsIEFP+VGNiC7yZfOjQDYwMZWPgFkfHhR1YrrEwAAAABJRU5ErkJggg==",
        type: ToolTypes.BUTTON,
        run: zoomOut,
    })
})(editor);