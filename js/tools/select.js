(function (editor) {
    var lastPoint;
    var isTranslate;

    function selectRectangle(startX, startY, endX, endY) {
        for (var x = startX; x <= endX; x++) {
            for (var y = startY; y <= endY; y++) {
                editor.selectPixel(x, y);
            }
        }
    }

    function startRectangle(pixel) {
        isTranslate = editor.isSelectedPixel(pixel.x, pixel.y);
        if (!isTranslate) {
            editor.unselectAll();
        }
        lastPoint = pixel;
    }

    function moveRectangle(pixel) {
        if (isTranslate) {
            editor.translateSelection(pixel.x - lastPoint.x, pixel.y - lastPoint.y);
            lastPoint = pixel;
        } else {
            if (lastPoint.x < pixel.x) {
                var startX = lastPoint.x;
                var endX = pixel.x;
            } else {
                var startX = pixel.x;
                var endX = lastPoint.x;
            }
            if (lastPoint.y < pixel.y) {
                var startY = lastPoint.y;
                var endY = pixel.y;
            } else {
                var startY = pixel.y;
                var endY = lastPoint.y;
            }
            editor.unselectAll();
            selectRectangle(startX, startY, endX, endY);
        }
    }

    function endRectangle() {};

    function selectArea(pixel) {
        var isSelected = editor.isSelectedPixel(pixel.x, pixel.y);
        editor.unselectAll();
        if (!isSelected) {
            editor.selectArea(pixel.x, pixel.y);
        }
    }
    editor.addTool({
        name: "Select Tools",
        type: ToolTypes.TOOL_LIST,
        list: [{
            name: "Select Rectangle",
            icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAABkElEQVR4Xu3cQY7CMBBEUXL/Q4MEiA2O5K4fmsT6s07F7keNE7Fgu/mHBDaUNnwTEJZAQAGhAIzbQAGhAIzbQAGhAIzbwD8D3gfrjz6U0XWjrZ8pO0VLGyjgFPP+RQIK+BSI/xPj4BveBhYaOItVuOVpLo1nqzQwXuQ0TPws/7qDgC+SuBwCCnjIAWEDIaOAAkIBGLeBAkIBGG9pINzjmvHKe+CaAnAqAQWEAjBuAxsB4ycV3GNHPJ6t0sB4kQ4BuEY8m4B+GwO7J6CAhwjAm3gGCggFYNwGCggFYLylgXCPa8YrL9JrCsCpBBQQCsC4DWwEjJ9UcI8d8Xi2SgPjRToE4BrxbAL6bQzsnoACHiIAb+IZKCAUgHEbKCAUgPGWBsI9rhmvvEivKQCnElBAKADjNrARMH5SwT12xOPZKg2MF+kQgGvEswnotzGwewJeB3B2p8v+xMkIoHIGCjgQEHC2FjvXCSjgR4CcvTHjLxoYb+aKQQHhpyaggFAAxm2ggFAAxm2ggFAAxm2ggFAAxh/G2mlRhTiJSQAAAABJRU5ErkJggg==",
            type: ToolTypes.DRAG,
            start: startRectangle,
            move: moveRectangle,
            end: endRectangle,
            raw: false
        }, {
            name: "Select Area",
            icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACgCAYAAACLz2ctAAAFuUlEQVR4Xu2cS24dQQzEkvsfOlll56ApQJrS9NDrsj4U3Qb8AP/+5ZcEggR+B3vbWgK/FFAJogQUMIrf5gqoA1ECChjFb3MF1IEoAQWM4re5AupAlIACRvHbXAF1IEpAAaP4ba6AOhAloIBR/DZXQB2IElDAKH6bK6AORAkoYBS/zRVQB6IEFDCK3+YTAv4JYaW7bJ8vhC/Tlh6tMt32A2+fr8L69VkFfO6EE6yfm36o0wSU7S/M9vmGTr2zrAI+d5cJ1s9NP9RpAsr2F2b7fEOn3llWAZ+7ywTr56Yf6jQBZfsLs32+oVPvLKuAz91lgvVz0w91moCy/YXZPt/QqXeWnRBw56b5qaj4n7rJp5YNO6iAPxxAAZ+zUgEV8DnbfuikgAqogFECChjF7wuogAoYJaCAUfy+gAqogFECChjF7wv4kICC/tlzuSigL2CUgAJG8fsCKqACRgkoYBS/L6ACKmCUgAJG8fsCKqACRgkoYBS/L6ACKmCUgAJG8fsCKqACRgkoYBS/L6ACKmCUgAJG8fsCKqACRgkoYBS/L6ACKmCUgAJG8fsCKqACRgko4Db8zjPxz4n8VaNXmIACYlQGJwgo4ARVa2ICCohRGZwgoIATVK2JCSggRmVwgoACTlC1JiaggBiVwQkCCjhB1ZqYwISAuPnHgv6B/qGP4j7mFV5XARUQyzIRVEAFnPAK11RABcSyTAQVUAEnvMI1FVABsSwTQQVUwAmvcE0FVEAsy0RQARVwwitcUwEfEpCCxpeDwe2f6lAu2/eA52CxiWUpaDYhT03swrufk5TL9j3OmxYSE8tS0IUxUXRiF9QYhiiX7XvAdVlsYlkKmk3IUxO78O7nJOWyfY/zpoXExLIUdGFMFJ3YBTWGIcpl+x5wXRabWJaCZhPy1MQuvPs5Sbls3+O8aSExsSwFXRgTRSd2QY1hiHLZvgdcl8UmlqWg2YQ8NbEL735OUi7b9zhvWkhMLEtBF8ZE0YldUGMYoly27wHXZbGJZSloNiFPTezCu5+TlMv2Pc6bFhKfWrbAZSLaLSCt171LqzOtxbo3vaweFYbehNbrxkjnQ31bi6GO3w1RYehNaL1u4nQ+1Le1GOr43RAVht6E1usmTudDfVuLoY7fDVFh6E1ovW7idD7Ut7UY6vjdEBWG3oTW6yZO50N9W4uhjt8NUWHoTWi9buJ0PtS3tRjq+N0QFYbehNbrJk7nQ31bi6GO3w1RYehNaL1u4nQ+1Le1GOr43RAVht6E1usmTudDfVuLoY71EAX9hl3q21/+HW84mgJeLKECXnzcN6ymgG+40sUzKuDFx33Dagr4hitdPKMCXnzcN6ymgG+40sUzKuDFx33Dagr4hitdPKMCXnzc4mqRP/grYPFKF8cV8D/HjYC5WLT/rRbh7Av4QdM2/aAroAL+I+ALuOkn84NeKqACRrVXQAVUwCgBBYzi9wVUQAWMElDAKH5fQAVUwCgBBYzi9wVUQAWMElDAKH5fQAVUwCgBBYzi9wVUQAWMElDAKH5fQAVUwCgBm0cJ+AJG8dtcAXUgSkABo/htroA6ECWggFH8NldAHYgSUMAofpsroA5ECShgFL/NFfChj+Io6G4l6X+h2D5fKxcKpbVpsRg9CN2F1iuOeYzfMt9x0UqAQqnU7M5SYegutF73HrfM18qFQmltWixGhaG70HrFMY/xW+Y7LloJUCiVmt1ZKgzdhdbr3uOW+Vq5UCitTYvFqDB0F1qvOOYxfst8x0UrAQqlUrM7S4Whu9B63XvcMl8rFwqltWmxGBWG7kLrFcc8xm+Z77hoJUChVGp2Z6kwdBdar3uPW+Zr5UKhtDYtFqPC0F1oveKYx/gt8x0XrQQolEpNsxLABBQQozI4QUABJ6haExNQQIzK4AQBBZygak1MQAExKoMTBBRwgqo1MQEFxKgMThBQwAmq1sQEFBCjMjhB4C+j3NyhmMgo5QAAAABJRU5ErkJggg==",
            type: ToolTypes.CLICK,
            run: selectArea,
        }]
    })
})(editor);
