(function (editor) {
    var lastPoint;

    function start(pixel, raw) {
        lastPoint = raw;
    }

    function move(pixel, raw) {
        editor.moveCanvas(raw.x - lastPoint.x, raw.y - lastPoint.y);
        lastPoint = raw;
    }

    function end(pixel, raw) {

    }

    editor.addTool({
        name: "Move Layer",
        icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAACL0lEQVR4Xu2Z25ICMQhE4/9/9G5taemDk6RJk4vu8RkGODRkJt7KWb8fMZ2baDfd7JhEHpUC0Gw5AAFoEjDdUWAA4BUsdSc7voEU+6Zqwv0nxS0cCI5vPNOGBwBNnAAE4JPAFjGsCrpiZ62I8aZXAH7ICK9Qx4oYKNAUHAC/CWB2LVfPm77jpwcIXlNlQ51e3/QAAMzRhHrLkhPt9ZTpApkeAAVma6L9PFWpqxrfrf6YRIJKPSbvYxIBYFfskgEjLGGqG301wBXFfVyMyA78uOIqQk+tA4D1dSKxkYwWnpCp6kCBpjp2AlRvd1U79XDepcCrSZRqq42w5FxKUe0AWEoZ7pJKr9KQ7ItStemSHQq8t2dYHABMAChJ1TzRAlO8xVR9nXtj9ecIwOsRvuokACv6RoHm4AMQgCYB0x0FAtAkYLqjQACaBEx3S4HSC6PzvRgojuusCmiVIQABeNeK+n2s2qFAduD1vaF6+tRUueK2+OgYAKwvF4mNZPSIseuERIHqCbLxT6XhJqHAhSMcENKw6Yo1MZzcsExTI7YfBkATNgABaBIw3VFgBaAKxuT/5h55yxiKPT1A8CV8qIiG0/T6pgcAYI4mGGGTIwAnAMxeH9mXu1LJ2UXUgq4obkWM9cd84xDJbh4ApZmpGwEQgDECjmIc31iWHevsPRRJzoHg+EZy7NoCsIuobQBAAD4JbBHDlqCNpquffMfkfUwiwVubY/I+JhEAmssYgP8U4C/Ku7RHpDwVRwAAAABJRU5ErkJggg==",
        type: ToolTypes.DRAG,
        start: start,
        move: move,
        end: end,
        raw: true,
        showStartPos: false,
        defaultTool: true
    })
})(editor);

(function (editor) {
    var startPoint;
    var isSelection;

    function start(pixel) {
        startPoint = pixel
        isSelection = editor.isSelectedPixel(pixel.x, pixel.y);
    }

    function moveSelection(pixel) {
        if (isSelection) {
            editor.translateSelectionContent(pixel.x - startPoint.x, pixel.y - startPoint.y);
        } else {
            editor.translateLayer(pixel.x - startPoint.x, pixel.y - startPoint.y);
        }
    }

    editor.addTool({
        name: "Translate",
        icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAACL0lEQVR4Xu2Z25ICMQhE4/9/9G5taemDk6RJk4vu8RkGODRkJt7KWb8fMZ2baDfd7JhEHpUC0Gw5AAFoEjDdUWAA4BUsdSc7voEU+6Zqwv0nxS0cCI5vPNOGBwBNnAAE4JPAFjGsCrpiZ62I8aZXAH7ICK9Qx4oYKNAUHAC/CWB2LVfPm77jpwcIXlNlQ51e3/QAAMzRhHrLkhPt9ZTpApkeAAVma6L9PFWpqxrfrf6YRIJKPSbvYxIBYFfskgEjLGGqG301wBXFfVyMyA78uOIqQk+tA4D1dSKxkYwWnpCp6kCBpjp2AlRvd1U79XDepcCrSZRqq42w5FxKUe0AWEoZ7pJKr9KQ7ItStemSHQq8t2dYHABMAChJ1TzRAlO8xVR9nXtj9ecIwOsRvuokACv6RoHm4AMQgCYB0x0FAtAkYLqjQACaBEx3S4HSC6PzvRgojuusCmiVIQABeNeK+n2s2qFAduD1vaF6+tRUueK2+OgYAKwvF4mNZPSIseuERIHqCbLxT6XhJqHAhSMcENKw6Yo1MZzcsExTI7YfBkATNgABaBIw3VFgBaAKxuT/5h55yxiKPT1A8CV8qIiG0/T6pgcAYI4mGGGTIwAnAMxeH9mXu1LJ2UXUgq4obkWM9cd84xDJbh4ApZmpGwEQgDECjmIc31iWHevsPRRJzoHg+EZy7NoCsIuobQBAAD4JbBHDlqCNpquffMfkfUwiwVubY/I+JhEAmssYgP8U4C/Ku7RHpDwVRwAAAABJRU5ErkJggg==",
        type: ToolTypes.DRAG,
        start: start,
        move: moveSelection,
        end: moveSelection,
        preview: true,
        showPosDifference: true
    })
})(editor);