/**
 * Layer with a multiple frame
 * The frames in this layer are independent from each other.
 */

class PixelCanvasLayerMF {

    static fromJSON(json, colorList, frameCount) {
        var layer = new PixelCanvasLayerMF(frameCount, json.name, json.isActive);
        for (var i = 0; i < frameCount; i++) {
            var frame = json.frames[i];
            for (var x in frame) {
                var line = frame[x];
                for (var y in line) {
                    layer.setPixel(x, y, colorList[line[y]], i);
                }
            }
        }
        return layer;
    }

    constructor(frameNum, name = "Layer", isActive = true) {
        this.frames = [];
        this.name = name;
        this.isActive = isActive;
        for (var i = 0; i < frameNum; i++) {
            this.frames[i] = new PixelCanvas();
        }
    }

    getPixel(x, y, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            if (x < frame.x || y < frame.y) {
                return Colors.CLEAR
            }
            return frame.getPixel(x, y)
        }
        return Colors.CLEAR
    }

    setPixel(x, y, color, frame = 0) {
        if (this.isActive) {
            frame = this.frames[frame];
            if (frame) {
                frame.setPixel(x, y, color);
            }
        }
    }

    translate(x = 0, y = 0, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.translate(x, y)
        }
    }

    translateSelectionContent(x = 0, y = 0, selection = {}, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.translateSelectionContent(x, y, selection);
        }
    }

    invert(d, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.invert(d);
        }
    }

    invertSelectionContent(selection = {}, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.invertSelectionContent(selection);
        }
    }

    mirror(dimensions, isVertical, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.mirror(dimensions, isVertical);
        }
    }

    mirrorSelectionContent(selection = {}, isVertical, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.mirrorSelectionContent(selection, isVertical);
        }
    }

    rotate(dimensions, angle, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.rotate(dimensions, angle);
        }
    }

    rotateSelectionContent(selection = {}, dimensions, angle, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.rotateSelectionContent(selection, dimensions, angle);
        }
    }

    scaleUp(ratio, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.scaleUp(ratio);
        }
    }

    slaceUpSelectionContent(selection = {}, ratio, cx, cy, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.slaceUpSelectionContent(selection, ratio, cx, cy);
        }
    }

    scaleDown(ratio, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.scaleDown(ratio);
        }
    }

    slaceDownSelectionContent(selection = {}, ratio, cx, cy, frame = 0) {
        frame = this.frames[frame];
        if (frame) {
            frame.slaceDownSelectionContent(selection, ratio, cx, cy);
        }
    }

    addFrame(pos) {
        this.frames.splice(pos, 0, new PixelCanvas());
    }

    removeFrame(pos) {
        this.frames.splice(pos, 1);
    }

    moveFrame(currentPos, newPos) {
        this.frames.splice(newPos, 0, this.frames.splice(currentPos, 1)[0]);
    }

    clear(frame) {
        frame = this.frames[frame];
        if (frame) {
            frame.clear();
        }
    }

    startPreview(frame) {
        frame = this.frames[frame];
        if (frame) {
            return frame.startPreview();
        }
    }

    endPreview(frame) {
        frame = this.frames[frame];
        if (frame) {
            return frame.endPreview();
        }
    }

    setIsActive(status) {
        this.isActive = status;
    }

    normalizeColorPalette(palette) {
        for (var frame of this.frames) {
            frame.normalizeColorPalette(palette);
        }
    }

    setCanvas(canvas, frame) {
        this.frames[frame] = canvas;
    }

    getFrameCopy(frame) {
        return this.frames[frame].clone();
    }

    merge(layer) {
        if (layer instanceof PixelCanvasLayerMF) {
            for (var i = 0; i < this.frames.length; i++) {
                this.frames[i].merge(layer.frames[i]);
            }
        } else {
            for (var i = 0; i < this.frames.length; i++) {
                this.frames[i].merge(layer.canvas);
            }
        }
    }

    createSelectionCanvas(selection, frame) {
        frame = this.frames[frame];
        if (frame) {
            return frame.cloneAndClearSelection(selection);
        }
    }

    mergeSelectionCanvas(canvas, frame) {
        frame = this.frames[frame];
        if (frame) {
            //TODO Change this from merge with the selection canvas to replace every pixel from the selection canvas
            return frame.merge(canvas);
        }
    }

    toSF(frame) {
        var sf = new PixelCanvasLayerSF(this.name, this.isActive);
        sf.setCanvas(this.frames[frame].clone());
        return sf;
    }

    toJSON(colorList) {
        var frameList = [];
        for (var frame of this.frames) {
            frameList.push(frame.toJSON(colorList));
        }
        return {
            name: this.name,
            type: "mf",
            isActive: this.isActive,
            frames: frameList
        };
    }
}