/**
 * Layer with a single frame
 * The content of the layer is the same trought all the frames.
 */

class PixelCanvasLayerSF {
    static fromJSON(json, colorList) {
        var layer = new PixelCanvasLayerSF(json.name, json.isActive);
        var canvas = json.canvas;
        for (var x in canvas) {
            var line = canvas[x];
            for (var y in line) {
                layer.setPixel(x, y, colorList[line[y]]);
            }
        }
        return layer;
    }

    constructor(name = "Layer", isActive = true) {
        this.canvas = new PixelCanvas();
        this.name = name;
        this.isActive = isActive;
    }

    getPixel(x, y) {
        if (x < this.canvas.x || y < this.canvas.y) {
            return Colors.CLEAR
        }
        return this.canvas.getPixel(x, y)
    }

    setPixel(x, y, color) {
        if (this.isActive) {
            this.canvas.setPixel(x, y, color);
        }
    }

    translate(x = 0, y = 0) {
        this.canvas.translate(x, y)
    }

    translateSelectionContent(x = 0, y = 0, selection = {}) {
        this.canvas.translateSelectionContent(x, y, selection);
    }

    invert(d) {
        this.canvas.invert(d);
    }

    invertSelectionContent(selection = {}) {
        this.canvas.invertSelectionContent(selection);
    }

    mirror(dimensions, isVertical) {
        this.canvas.mirror(dimensions, isVertical);
    }

    mirrorSelectionContent(selection = {}, isVertical) {
        this.canvas.mirrorSelectionContent(selection, isVertical);
    }

    rotate(dimensions, angle) {
        this.canvas.rotate(dimensions, angle);

    }

    rotateSelectionContent(selection = {}, dimensions, angle) {
        this.canvas.rotateSelectionContent(selection, dimensions, angle);
    }

    scaleUp(ratio) {
        this.canvas.scaleUp(ratio);
    }

    slaceUpSelectionContent(selection = {}, ratio, cx, cy) {
        this.canvas.slaceUpSelectionContent(selection, ratio, cx, cy);
    }

    scaleDown(ratio) {
        this.canvas.scaleDown(ratio);
    }

    slaceDownSelectionContent(selection = {}, ratio, cx, cy) {
        this.canvas.slaceDownSelectionContent(selection, ratio, cx, cy);
    }

    addFrame() {}

    removeFrame() {}

    moveFrame() {}

    clear() {
        this.canvas.clear();
    }

    startPreview() {
        return this.canvas.startPreview();
    }

    endPreview() {
        return this.canvas.endPreview();
    }

    setIsActive(status) {
        this.isActive = status;
    }

    normalizeColorPalette(palette) {
        this.canvas.normalizeColorPalette(palette);
    }

    setCanvas(canvas) {
        this.canvas = canvas;
    }

    getFrameCopy() {
        return this.canvas.clone();
    }

    merge(layer, frame) {
        if (layer instanceof PixelCanvasLayerSF) {
            this.canvas.merge(layer.canvas);
        } else {
            this.canvas.merge(layer.frames[frame]);
        }
    }

    createSelectionCanvas(selection) {
        return this.canvas.cloneAndClearSelection(selection);
    }

    mergeSelectionCanvas(canvas) {
        return this.canvas.merge(canvas);
    }

    toMF(frame, frameNum) {
        var mf = new PixelCanvasLayerMF(frameNum, this.name, this.isActive);
        mf.setCanvas(this.getFrameCopy(), frame);
        return mf;
    }

    toJSON(colorList) {
        return {
            name: this.name,
            type: "sf",
            isActive: this.isActive,
            canvas: this.canvas.toJSON(colorList)
        };
    }
}