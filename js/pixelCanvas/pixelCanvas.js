/**
 * Canvas that contains the pixels of a frame in a layer
 */

class PixelCanvas {
    constructor() {
        this.canvas = {};
        this.onPreview = false;
        this.originalCanvas;
    }

    setPixel(x, y, color) {
        if (color.equals(Colors.CLEAR)) {
            if (this.canvas[x]) {
                if (this.canvas[x][y]) {
                    delete this.canvas[x][y];
                    this.canvas[x].length--;
                    if (this.canvas[x].length == 0) {
                        delete this.canvas[x];
                    }
                }
            }
        } else {
            if (!this.canvas[x]) {
                this.canvas[x] = {};
                Object.defineProperty(this.canvas[x], 'length', {
                    value: 0,
                    enumerable: false,
                    writable: true,
                    configurable: true
                });
            }
            if (!this.canvas[x][y]) {
                this.canvas[x].length++;
            }
            this.canvas[x][y] = color;
        }
    }

    getPixel(x, y) {
        var color = undefined;
        if (this.canvas[x]) {
            color = this.canvas[x][y];
        }
        if (!color) {
            color = Colors.CLEAR;
        }
        return color;
    }

    translate(x, y) {
        x = x | 0
        y = y | 0
        var newCanvas = {}
        var newLine;
        var line;
        for (var i in this.canvas) {
            newLine = {};
            i = Number.parseInt(i);
            line = this.canvas[i]
            for (var j in line) {
                j = Number.parseInt(j)
                newLine[j + y] = line[j];
            }
            Object.defineProperty(line, 'length', {
                value: this.canvas[i].length,
                enumerable: false,
                writable: true,
                configurable: true
            });
            newCanvas[i + x] = newLine;
        }
        this.canvas = newCanvas;
    }

    translateSelectionContent(x, y, selection) {
        var clone = this.clone();
        for (var ix in selection) {
            for (var iy of selection[ix]) {
                clone.setPixel(ix, iy, Colors.CLEAR);
            }
        }
        for (var ix in selection) {
            for (var iy of selection[ix]) {
                clone.setPixel(Number.parseInt(ix) + x, Number.parseInt(iy) + y, this.getPixel(ix, iy))
            }
        }
        this.canvas = clone.canvas;
    }

    invert(d) {
        var endX = d.end.x;
        var endY = d.end.y;
        for (var x = d.start.x; x <= endX; x++) {
            for (var y = d.start.y; y <= endY; y++) {
                this.setPixel(x, y, this.getPixel(x, y).inverse());
            }
        }
    }

    invertSelectionContent(selection) {
        for (var x in selection) {
            for (var y of selection[x]) {
                this.setPixel(x, y, this.getPixel(x, y).inverse());
            }
        }
    }

    mirror(dimensions, isVertical) {
        var oldCanvas = this.canvas;
        this.clear();
        if (isVertical) {
            var middle = dimensions.height / 2
            for (var x in oldCanvas) {
                var line = oldCanvas[x];
                for (var y in line) {
                    this.setPixel(x, middle + (middle - y) - 1, line[y]);
                }
            }
        } else {
            var middle = dimensions.width / 2
            for (var x in oldCanvas) {
                var line = oldCanvas[x];
                var nx = middle + (middle - x) - 1;
                for (var y in line) {
                    this.setPixel(nx, y, line[y]);
                }
            }
        }
    }

    mirrorSelectionContent(selection, isVertical) {
        var d = getSelectionLimits(selection);
        var aux = this.cloneAndClearSelection(selection);
        if (isVertical) {
            var middle = (d.end.y + d.start.y) / 2;
            for (var x in selection) {
                for (var y of selection[x]) {
                    this.setPixel(x, middle + (middle - y), aux.getPixel(x, y));
                }
            }
        } else {
            var middle = (d.end.x + d.start.x) / 2;
            for (var x in selection) {
                var nx = middle + (middle - x);
                for (var y of selection[x]) {
                    this.setPixel(nx, y, aux.getPixel(x, y));
                }
            }
        }
    }

    rotate(dimensions, angle) {
        if (angle != 0) {
            var oldCanvas = this.canvas;
            this.clear();
            dimensions = {
                start: dimensions.start,
                end: {
                    x: dimensions.end.x,
                    y: dimensions.end.y
                }
            }
            var xSize = (dimensions.end.x - dimensions.start.x + 1);
            var ySize = (dimensions.end.y - dimensions.start.y + 1);
            var xEven = xSize % 2 == 0;
            var yEven = ySize % 2 == 0;
            var offsetX = 0;
            var offsetY = 0;
            var center;
            var aux;
            if (xEven != yEven) { //width and height are even and odd
                if (xSize > ySize) {
                    switch (angle) {
                        case 180:
                            offsetX = 1;
                            break;
                        case 270:
                            offsetY = 1;
                            break;
                    }
                    dimensions.end.x--;
                    xEven = !xEven;
                } else {
                    switch (angle) {
                        case 90:
                            offsetX = 1;
                            break;
                        case 180:
                            offsetY = 1;
                            break;
                    }
                    dimensions.end.y--;
                    yEven = !yEven;
                }
            }
            if (xEven) { //width and height are even
                center = centerEven(dimensions.start.x, dimensions.start.y, dimensions.end.x, dimensions.end.y);
                for (var x in oldCanvas) {
                    var line = oldCanvas[x];
                    for (var y in line) {
                        aux = rotateEven(x, y, center.x, center.y, angle);
                        this.setPixel(aux.x + offsetX, aux.y + offsetY, line[y]);
                    }
                }
            } else { //width and height are odd
                center = centerOdd(dimensions.start.x, dimensions.start.y, dimensions.end.x, dimensions.end.y);
                for (var x in oldCanvas) {
                    var line = oldCanvas[x];
                    for (var y in line) {
                        aux = rotateOdd(x, y, center.x, center.y, angle);
                        this.setPixel(aux.x + offsetX, aux.y + offsetY, line[y]);
                    }
                }
            }
        }
    }

    rotateSelectionContent(selection = {}, dimensions, angle) {
        if (angle != 0) {
            var clone = this.cloneAndClearSelection(selection);
            dimensions = {
                start: dimensions.start,
                end: {
                    x: dimensions.end.x,
                    y: dimensions.end.y
                }
            }
            var xSize = (dimensions.end.x - dimensions.start.x + 1);
            var ySize = (dimensions.end.y - dimensions.start.y + 1);
            var xEven = xSize % 2 == 0;
            var yEven = ySize % 2 == 0;
            var offsetX = 0;
            var offsetY = 0;
            var center;
            var aux;
            if (xEven != yEven) { //width and height are even and odd
                if (xSize > ySize) {
                    switch (angle) {
                        case 180:
                            offsetX = 1;
                            break;
                        case 270:
                            offsetY = 1;
                            break;
                    }
                    dimensions.end.x--;
                    xEven = !xEven;
                } else {
                    switch (angle) {
                        case 90:
                            offsetX = 1;
                            break;
                        case 180:
                            offsetY = 1;
                            break;
                    }
                    dimensions.end.y--;
                    yEven = !yEven;
                }
            }
            if (xEven) { //width and height are even
                center = centerEven(dimensions.start.x, dimensions.start.y, dimensions.end.x, dimensions.end.y);
                for (var x in selection) {
                    var line = selection[x];
                    for (var y of line) {
                        aux = rotateEven(x, y, center.x, center.y, angle);
                        this.setPixel(aux.x + offsetX, aux.y + offsetY, clone.getPixel(x, y));
                    }
                }
            } else { //width and height are odd
                center = centerOdd(dimensions.start.x, dimensions.start.y, dimensions.end.x, dimensions.end.y);
                for (var x in selection) {
                    var line = selection[x];
                    for (var y of line) {
                        aux = rotateOdd(x, y, center.x, center.y, angle);
                        this.setPixel(aux.x + offsetX, aux.y + offsetY, clone.getPixel(x, y));
                    }
                }
            }
        }
    }



    scaleUp(ratio) {
        var oldCanvas = this.canvas;
        this.clear();
        var sx, sy, endy, endx, iy;
        for (var x in oldCanvas) {
            x = Number.parseInt(x);
            var line = oldCanvas[x];
            for (var y in line) {
                sx = x * ratio;
                sy = y * ratio;
                endy = sy + ratio;
                for (endx = sx + ratio; sx < endx; sx++) {
                    for (iy = sy; iy < endy; iy++) {
                        this.setPixel(sx, iy, line[y]);
                    }
                }
            }
        }
    }

    scaleUpSelectionContent(selection, ratio, cx, cy) {
        var clone = this.cloneAndClearSelection(selection);
        var sx, sy, endy, endx, iy, color;
        for (var x in selection) {
            for (var y of selection[x]) {
                sx = (x - cx) * ratio;
                sy = (y - cy) * ratio;
                endy = sy + ratio;
                color = clone.getPixel(x, y);
                for (endx = sx + ratio; sx < endx; sx++) {
                    for (iy = sy; iy < endy; iy++) {
                        this.setPixel(sx + cx, iy + cy, color);
                    }
                }
            }
        }
    }

    scaleDown(ratio, cx, cy) {
        var oldCanvas = this.canvas;
        this.clear();
        var list = {};
        var ix, iy;
        for (var x in oldCanvas) {
            x = Number.parseInt(x);
            var line = oldCanvas[x];
            for (var y in line) {
                ix = (x / ratio) | 0;
                iy = (y / ratio) | 0;
                if (list[ix]) {
                    if (list[ix][iy]) {
                        list[ix][iy].push(line[y]);
                    } else {
                        list[ix][iy] = [line[y]];
                    }
                } else {
                    list[ix] = {};
                    list[ix][iy] = [line[y]];
                }
            }
        }
        for (var x in list) {
            for (var y in list[x]) {
                this.setPixel(x, y, Color.average(list[x][y]));
            }
        }
    }

    scaleDownSelectionContent(selection, ratio, cx, cy) {
        var clone = this.cloneAndClearSelection(selection);
        var list = {};
        var ix, iy;
        for (var x in selection) {
            for (var y of selection[x]) {
                ix = ((x - cx) / ratio + cx) | 0;
                iy = ((y - cy) / ratio + cy) | 0;
                if (list[ix]) {
                    if (list[ix][iy]) {
                        list[ix][iy].push(clone.getPixel(x, y));
                    } else {
                        list[ix][iy] = [clone.getPixel(x, y)];
                    }
                } else {
                    list[ix] = {};
                    list[ix][iy] = [clone.getPixel(x, y)];
                }
            }
        }
        for (var x in list) {
            for (var y in list[x]) {
                this.setPixel(x, y, Color.average(list[x][y]));
            }
        }
    }

    clear() {
        this.canvas = {};
    }

    merge(canvas) {
        for (var x in canvas.canvas) {
            var line = canvas.canvas[x];
            for (var y in line) {
                this.setPixel(x, y, line[y].over(this.getPixel(x, y)));
            }
        }
    }

    startPreview() {
        var preview = undefined;
        if (this.onPreview) {
            preview = this.endPreview();
        }
        this.onPreview = true;
        this.originalCanvas = this.canvas;
        this.canvas = {};
        var newLine;
        var line;
        for (var x in this.originalCanvas) {
            var newLine = {};
            line = this.originalCanvas[x];
            for (var y in line) {
                newLine[y] = line[y];
            }
            Object.defineProperty(line, 'length', {
                value: this.originalCanvas[x].length,
                enumerable: false,
                writable: true,
                configurable: true
            });
            this.canvas[x] = newLine;
        }
        return preview;
    }

    endPreview() {
        if (this.onPreview) {
            var preview = this.canvas;
            this.canvas = this.originalCanvas;
            this.onPreview = false;
            return preview;
        }
    }

    normalizeColorPalette(palette) {
        for (var x in this.canvas) {
            var line = this.canvas[x];
            for (var y in line) {
                line[y] = palette.addColor(line[y]);
            }
        }
    }

    clone() {
        var clone = new PixelCanvas();
        for (var x in this.canvas) {
            var line = this.canvas[x];
            for (var y in line) {
                clone.setPixel(x, y, line[y]);
            }
        }
        return clone;
    }

    cloneAndClearSelection(selection) {
        var clone = new PixelCanvas();
        for (var x in selection) {
            for (var y of selection[x]) {
                clone.setPixel(x, y, this.getPixel(x, y));
                this.setPixel(x, y, Colors.CLEAR);
            }
        }
        return clone;
    }

    toJSON(colorList) {
        var json = {};
        for (var x in this.canvas) {
            var line = this.canvas[x];
            var lineJSON = {};
            for (var y in line) {
                lineJSON[y] = colorList.indexOf(line[y]);
            }
            json[x] = lineJSON;
        }
        return json;
    }
}