class SpriteLayerMF {
    constructor(sprite, x, y, ratio, frameNum) {
        this.sprite = sprite;
        this.frames = [];
        for (var i = 0; i < frameNum; i++) {
            this.frames = {
                x: x,
                y: y,
                ratio: ratio,
                show: true
            }

        }
        this.x = x;
        this.y = y;
        this.ratio = ratio;
    }

    getPos(frame) {
        frame = this.frames[frame];
        if (frame) {
            return {
                x: frame.x,
                y: frame.y
            };
        }
    }

    getPixel(x, y, frame) {
        frame = this.frames[frame];
        if (frame) {
            if (x < frame.x || y < frame.y) {
                return Colors.CLEAR
            }
            return this.sprite.getPixel(((x - frame.x) / this.ratio) | 0, ((y - frame.y) / this.ratio) | 0)
        }
        return Colors.CLEAR
    }

    setPixel(){
        console.log("Unable to set Pixel on sprite layer.")
    }

    setRatio(ratio = 1, frame) {
        frame = this.frames[frame];
        if (frame) {
            frame.ratio = ratio;
        }
    }

    translate(x = 0, y = 0, frame) {
        frame = this.frames[frame];
        if (frame) {
            frame.x = x;
            frame.y = y;
        }
    }

    addFrame(pos) {
        var frame;
        if (pos) {
            frame = this.frames[pos - 1];
        } else {
            frame = this.frames[pos + 1];
        }
        this.frames.splice(pos, 0, {
            x: frame.x,
            y: frame.y,
            ratio: frame.ratio,
            show: frame.show
        })
    }

    removeFrame(pos) {
        this.frames.splice(pos, 1);
    }

    moveFrame(currentPos, newPos) {
        this.frames.splice(newPos, 0, this.frames.splice(currentPos, 1));
    }
}