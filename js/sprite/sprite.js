class Sprite {
    constructor(width, height) {
        this.width = width | 0;
        this.height = height | 0;
        this.pixels = [];
        var aux;
        for (var i = 0; i < width; i++) {
            aux = [];
            for (var j = 0; j < height; j++) {
                aux[j] = Colors.CLEAR;
            }
            this.pixels[i] = aux;
        }
    }

    getPixel(x, y) {
        x = x | 0
        y = y | 0
        if (x < this.width && x >= 0 && y < this.height && y >= 0) {
            return this.pixels[x][y]
        } else {
            return Colors.CLEAR;
        }
    }

    setPixel(x, y, color) {
        x = x | 0
        y = y | 0
        if (x < this.width && x >= 0 && y < this.height && y >= 0) {
            this.pixels[x][y] = color;
        }
    }

    resize(newWidth, newHeight) {
        if (newWidth < this.width) {
            resizeArray(this.pixels, newWidth);
        } else if (newWidth > this.width) {
            var aux;
            for (var i = this.width; i < newWidth; i++) {
                aux = [];
                for (var j = 0; j < this.height; j++) {
                    aux[j] = Colors.CLEAR;
                }
                this.pixels[i] = aux;
            }
        }

        if (newHeight < this.height) {
            for (var i = 0; i < newWidth; i++) {
                resizeArray(this.pixels[i], newHeight);
            }

        } else if (newHeight > this.height) {
            var aux;
            for (var i = 0; i < newWidth; i++) {
                aux = this.pixels[i];
                for (var j = this.height; j < newHeight; j++) {
                    aux[j] = Colors.CLEAR;
                }
            }
        }

        this.width = newWidth;
        this.height = newHeight;
    }




}