class SpriteLayerSF {
    constructor(sprite, x, y, ratio) {
        this.sprite = sprite;
        this.x = x;
        this.y = y;
        this.ratio = ratio;
    }

    getPos() {
        return {
            x: this.x,
            y: this.y
        };
    }

    getPixel(x, y) {
        if (x < this.x || y < this.y) {
            return Colors.CLEAR
        }
        return this.sprite.getPixel(((x - this.x) / this.ratio) | 0, ((y - this.y) / this.ratio) | 0)
    }

    setPixel() {
        console.log("Unable to set Pixel on sprite layer.")
    }

    setRatio(ratio = 1) {
        this.ratio = ratio;
    }

    translate(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    addFrame() {}

    removeFrame() {}

    moveFrame() {}

}